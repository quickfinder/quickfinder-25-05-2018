<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Favourite extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Favourite_model');
        $this->load->model('Register_model');
        $this->load->model('Store_model');
    }
    public function addStore()
	{
		$storeid=$_POST['id'];
		$this->Favourite_model->addStore($storeid);
	}
	public function removeStore()
	{
		$storeid=$_POST['id'];
		$this->Favourite_model->removeStore($storeid);
	}
	public function myFavorite()
	{
		$data['storelist']=$this->Favourite_model->myFavorite();
		$this->load->view('product/store', $data);
	}
	public function userLogin()
	{
		
        if ($data = $this->Register_model->login()) 
			{
				if(!empty($data[0]['profilepic']))
				{
					$this->session->set_userdata('profilepic',$data[0]['profilepic']);
				}
				else{
					$this->session->set_userdata('profilepic','asset/img/avatar.jpg');
				}	
                $this->session->set_userdata(array('userid' => $data[0]['userid'],'username'=>$data[0]['username'], 'usertype' => $data[0]['usertype'], 'email' => $data[0]['email'], 'is_logged_in' => TRUE, 'is_logged' => 1));

                if ($num_store = $this->Store_model->getNumStore()) {
                    $this->session->set_userdata('num_store', $num_store);
                }
				$array['success']=1;
				echo json_encode($array);
			}
			else 
			{
				$array['error']='UserName and Password Missmatch!...';
				echo json_encode($array);
            }
    }
}