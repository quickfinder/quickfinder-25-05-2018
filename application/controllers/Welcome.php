<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Search_model');
		if($this->session->userdata('user')!=true)
		{
			if($this->Search_model->userCount())
			{	
				$this->session->set_userdata('user',true);
			}
		} 
	}	
	public function index()
	{
		if($data['caterories']=$this->Search_model->getCategories())
		{
			$data['banner']=$this->Search_model->getBanner();
			$this->load->view('home/index',$data);
		}
	}

	public function fetch_all_names()
    {
        $caterories = $this->Search_model->get_names_for_autofill();
        $datapassed['all_data'] = $caterories;
        $data=$this->load->view('autofill/data',$datapassed, TRUE);
        $this->output->set_output($data);
    }

	public function fetch_all_areas()
    {
        $areas = $this->Search_model->get_all_areas();
        $datapassed['all_data'] = $areas;
        $data=$this->load->view('autofill/areas',$datapassed, TRUE);
        $this->output->set_output($data);
    }

	public function Search()
	{
		$data['storelist'] = $this->Search_model->getSearch();
		//echo "<pre>storelist :\n"; print_r($data['storelist']); echo "</pre>";
        $this->load->view('product/store', $data);
	}
	public function locationSearch()
	{
		
		$storelist = $this->Search_model->getSearch();
        
		echo json_encode($data); 
	}
	public function top_Result()
	{
		$data['storelist'] = $this->Search_model->top_Result();
        $this->load->view('product/store', $data);
	}
	
}
