<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Keywords extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Keyword_model');
    }

    public function listKeyword() {

        $data['keywordlist'] = $this->Keyword_model->getKeywordData();
        $data['page'] = 'banner/keywordlist';
        $this->load->view('templates/content', $data);
        // $this->load->view('banner/js', $data);
    }

    public function addKeyword()
    {
        
        $this->form_validation->set_rules('keyword', 'Plz Enter The keyword', 'required');

         if ($this->form_validation->run() == FALSE)
        {   
            $data['page'] = 'banner/keywordlist';
        $this->load->view('templates/content', $data);
        }
        else
        {
            if($this->Keyword_model->insertkeyword())
            {
                // echo "succefully";
                // $this->session->set_userdata('msg', "Password change succefully you can do login!...");
                $data['page'] = 'banner/keywordlist';
        $this->load->view('templates/content', $data);
                // redirect('signin');
            }
        }
       
    }

}

?>
