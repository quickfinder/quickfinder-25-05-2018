<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validation extends CI_Controller {
	public function __construct()
	{
		parent::__construct();	
		$this->load->model('Validation_model');	
		$this->load->model('Store_model');	
	}	
	public function index()
	{
	}
	public function addStoreNext1()
	{
// echo "string";
		$this->form_validation->set_rules('name', '', 'required');
        $this->form_validation->set_rules('categoryid', '', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email|callback_useremail_check');
        $this->form_validation->set_rules('mobile', '', 'required|min_length[10]');
        $this->form_validation->set_rules('city', '', 'required');
        $this->form_validation->set_rules('area1', 'area', 'required');
        $this->form_validation->set_rules('description', '', 'required');
        $this->form_validation->set_rules('address', '', 'required');
        if ($this->form_validation->run() == FALSE){
			
			$array='';
			foreach($this->form_validation->error_array() as $key => $val)
			{
				$array[$key]=$val;
			}
			// print_r($array);
			echo json_encode($array);
			// die;
		  // $errors = validation_errors();
            //echo json_encode(['error'=>$errors]);
        }else{
        	
			if($this->Store_model->saveStoreDetails())
			{
				echo json_encode(['sucess'=>'1']);

			}
			else
			{
				
				print("Not save Store Data");	
			}
          
        }
	}
	public function adminNewStore()
	{
		
		
	$this->form_validation->set_rules('fname', 'First Name', 'trim|required|regex_match[/^[A-Z a-z]+$/]');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|regex_match[/^[A-Z a-z]+$/]');
        $this->form_validation->set_rules('useremail','','trim|required|callback_useremail_check');
        $this->form_validation->set_rules('username', '', 'trim|required|callback_username_check');
        $this->form_validation->set_rules('mobile', '', 'trim|required|min_length[10]|regex_match[/^[789][0-9]{9}+$/]');
        if ($this->form_validation->run() == FALSE){
			
			$array='';
			foreach($this->form_validation->error_array() as $key => $val)
			{
				$array[$key]=$val;
			}
			echo json_encode($array);
        }else
		{
			
           if($this->Store_model->saveUserDetails())
			{
				echo json_encode(['sucess'=>'1']);
				
			}
			else
			{
				
				print("Not save Store Data");	
			}
        }
	}
	public function store_images()
	{
		if($this->Store_model->save_images())
		{
			echo json_encode(['sucess'=>'1']);
			//echo json_encode(['storeid'=>$this->session->userdata('storeid')]);
			//echo json_encode(['newStoreID'=>$this->session->userdata('newStoreID')]);
				
		}
		else
		{
			print("Not save Store Data");	
		}
	}
	
	public function editStoreNext1()
	{
		
		$this->session->set_userdata('checkemail',$_POST['id']);
		$this->session->set_userdata('prevEmail',$_POST['prevEmail']);
		$this->form_validation->set_rules('name', '', 'required');
        $this->form_validation->set_rules('categoryid', '', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email|callback_editemail_check');
        $this->form_validation->set_rules('mobile', '', 'required');
        $this->form_validation->set_rules('city', '', 'required');
        $this->form_validation->set_rules('area1', 'area', 'required');
        $this->form_validation->set_rules('description', '', 'required');
        $this->form_validation->set_rules('address', '', 'required');
        if ($this->form_validation->run() == FALSE){
			
			$array='';
			foreach($this->form_validation->error_array() as $key => $val)
			{
				$array[$key]=$val;
			}
			//print_r($array);
			echo json_encode($array);
        }else{
           echo json_encode(['sucess'=>'1']);
        }
	}
	public function editemail_check($str)
	{
		if($this->Validation_model->editCheck_email($str))
			return true;
		else
		{
			$this->form_validation->set_message('editemail_check', 'The {field} allready exists ');
			return false;
		}
	}
	public function useremail_check($str)
	{
		
		if($this->Validation_model->check_email($str))
			return true;
		else
		{
			$this->form_validation->set_message('useremail_check', 'The {field} allready exists ');
			return false;
		}
	}
	public function username_check($str)
	{
		if($this->Validation_model->username_check($str))
			return true;
		else
		{
			$this->form_validation->set_message('username_check', 'The {field} allready exists ');
			return false;
		}
	}
	
}