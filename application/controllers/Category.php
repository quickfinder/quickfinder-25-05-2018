<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	public function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Category_model');
		$this->load->model('store_model');
	}	
	public function index()
	{
		$data['categorylist']=$this->Category_model->getCategory();	
		$data['page']='category/categorylist';
		
		$this->load->view('templates/content',$data);	
		$this->load->view('category/js',$data);	
			
	}
	public function storeProfile($id)
	{
		//echo "user id : " .$this->session->userdata('userid');die();

		$decode = base64_decode($id);
		//echo " Decoded id : ".$decode;die();

		$this->Category_model->userVisit($decode); 
		
		$data['storeprofile'] = $this->Category_model->getStoreProfile($decode);
		//echo "<pre>Store-profile : \n"; print_r($data['storeprofile']); echo "</pre>";die();
		
		$data['storerating'] = $this->Category_model->getStoreRating($decode);
		//echo "<pre>store rating : \n"; print_r($data['storerating']); echo "</pre>";die();
		
        $this->load->view('product/storeprofile',$data);
    }

    /**
    * show store location with marker on  click of map button
    * ---
    * localhost/quickfinder/Category/storeprofile/MTAw
    **/
    public function show_store_location()
    {
    	$data['s'] = '';
        $this->load->view('product/storelocation',$data);
    }
    

	public function subCategory($id)
	{
        if($data['subCategoryList'] = $this->Category_model->getListSubCategory($id))
		{
			$this->load->view('subcategories/subcategories', $data);
		}
		else
		{
			$data['subCategoryList']=array();
			$this->load->view('subcategories/subcategories',$data);
		}
    }
	public function getProduct($id)
	{
        $decode = base64_decode($id);
        $data['storelist'] = $this->Category_model->getProduct($decode);
		$this->load->view('product/store', $data);
    }
	public function addCategory()
	{
		$data['page']='category/addcategory';
		$this->load->view('templates/content',$data);	
	}
	public function viewSubCategory($id)
	{
		$data['subcategory']=$this->Category_model->getSubCategory($id);
		$data['page']='category/addsubcategory';
		$this->load->view('templates/content',$data);	
		$this->load->view('category/js',$data);	
	}
	// public function addnewSubcategory()
	// {

	// 	print_r($_POST);
	// 	die;
	// }
	public function addSubCategory()
	{
		// print_r($_POST);
		// die;
		$this->Category_model->addSubCategory();
		{
			redirect('Category');
		}
	}
	public function editCategory($id)
	{
		echo $id;
	}
	public function deactiveCategory($id)
	{
		
		if($this->Category_model->deactiveCategory($id))
		{
			redirect('Category');
		}
	}
	public function activeCategory($id)
	{
		if($this->Category_model->activeCategory($id))
		{
			redirect('Category');
		}
	}
	public function addNewCategory() {
        $config['upload_path'] = './images/icons/';
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_size'] = 100;
        // $config['max_width'] = 1024;
        // $config['max_height'] = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('input-file-preview')) {
            $error = array('error' => $this->upload->display_errors());
            $data['error'] = $error;
            $data['page'] = 'category/addcategory';
            $this->load->view('templates/content', $data);
        } else {
            $filedata = $this->upload->data();
            $filename = 'images/icons/' . $filedata['file_name'];
            if ($this->Category_model->addNewCategory($filename)) {
				$this->session->set_userdata('success',1);
                redirect('Category');
            }
        }
    }
	
    public function serch()
	{
		
		$city=$_POST['id'];
		$searchBox=$_POST['srchbx'];
		$area=$_POST['area'];
		
		$newstore = $this->store_model->getStoresInfoBySearchBox($city,$searchBox,$area);
		if(is_array($newstore))
					   {
						   $i=0;
						   foreach ($newstore as $row)
						   {
							 
						   ?>
						    <!--- card start------->
							<figure class="figure pull-left fadeInLeftShort animated ">
					<div class="col-md-12 n-margin-2">
						   <div class="col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url('about/'.$row['id'])?>"> <img src="<?php echo base_url();?><?php echo $row['firstimage']?>" class="box-v2-cover img-responsive n-img"></a>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12 ">
                            
                             <h3 class="vender-name"><?php echo $row['name'];?></h3>
                             <select class="example-css">
							  <option value="1">1</option>
							  <option value="2">2</option>
							  <option value="3">3</option>
							  <option value="4">4</option>
							  <option value="5">5</option>
							</select>
                             <p class="contact"><span class="fa-phone fa contact-icon"></span><?php echo $row['mobile'];?></p>
                             
                             <p class="address"><span class="fa-diamond fa contact-icon"></span><?php echo substr($row['description'],0,25);?>
							</p>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 price">
                        </div>
						 </div>
						</figure>
						<script src="<?php echo base_url();?>asset1/js/plugins/Minimal-jQuery-Rating-Widget-Plugin-Bar-Rating/examples.js"></script>
                        <?php
						}
					   }
					   else{
						   echo $newstore;
					   }
	}
	//display store by category id
	public function pages($id)
	{
		$newstore = $this->store_model->getStoresInfoByCategory($id);
					   if(is_array($newstore))
					   {
						   $i=0;
						   foreach ($newstore as $row)
						   {
							 
						   ?>
						    <!--- card start------->
							<figure class="figure pull-left fadeInLeftShort animated ">
              <div class="col-md-12 n-margin-2">
						   <div class="col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url('about/'.$row['id'])?>"> <img src="<?php echo base_url();?><?php echo $row['firstimage']?>" class="box-v2-cover img-responsive n-img"></a>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12 ">
                            
                             <h3 class="vender-name"><?php echo $row['name'];?></h3>
                             <select class="example-css">
							  <option value="1">1</option>
							  <option value="2">2</option>
							  <option value="3">3</option>
							  <option value="4">4</option>
							  <option value="5">5</option>
							</select>
                            
                             <p class="contact"><span class="fa-phone fa contact-icon"></span><?php echo  $row['mobile'];?></p>
                             
                             <p class="address"><span class="fa-diamond fa contact-icon"></span><?php echo  substr($row['description'],0,50);?>
							</p>
                        </div>
                      
                        <div class="col-md-2 col-sm-6 col-xs-12 price">

                        </div>
						 </div>
              <!--- card end-------> 
  </figure>
           <script src="<?php echo base_url();?>asset1/js/plugins/Minimal-jQuery-Rating-Widget-Plugin-Bar-Rating/examples.js"></script>			  
                        <?php
						}
					   }
					   else{
						   echo $newstore;
					   }

		 
	}
	public function backtosubcategories()
	{
		// $this->session->unset_userdata('success');
		redirect('categories');
	}
	public function categorydelete()
	{
		if($this->Category_model->categorydelete())
		{
			echo json_encode(['sucess'=>'1']);
		}
	}
	public function changeSubcategory()
	{
		if($this->Category_model->changeSubcategory())
		{
			echo json_encode(['sucess'=>'1']);
		}
		else{
			echo json_encode(['sucess'=>'2']);
		}
	}
	public function deleteSubcategory()
	{
		if($this->Category_model->deleteSubcategory())
		{
			echo json_encode(['sucess'=>'1']);
		}
	}
	public function addNewSubcategory()
	{
		if($this->Category_model->addNewSubcategory())
		{
			echo json_encode(['sucess'=>'1']);
		}
		else{
			echo json_encode(['sucess'=>'2']);
		}
	}
	public function editCategories($id)
	{
        $this->form_validation->set_rules('name', '', 'required');
        if ($this->form_validation->run() == FALSE)
		{
			$data['category']=$this->Category_model->getCategoryDetails($id);
			$data['page']='category/editcategory';
			$this->load->view('templates/content',$data);
		}
		else
		{
			$config['upload_path'] = './images/icons/';
			$config['allowed_types'] = 'gif|jpg|png';
			// $config['max_size'] = 100;
			// $config['max_width'] = 1024;
			// $config['max_height'] = 768;
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('input-file-preview')) 
			{
				$error = array('error' => $this->upload->display_errors());
				$data['error'] = $error;
				$data['page']='category/editcategory';
				$this->load->view('templates/content',$data);
			} 
			else
			{
				$filedata = $this->upload->data();
				$filename = 'images/icons/' . $filedata['file_name'];
				if ($this->Category_model->editCategories($filename,$id)) {
					$this->session->set_userdata('success',1);
					redirect('Category');
				}
			}
		}
	
	}
	public function backtoedicategory()
	{
		$this->session->unset_userdata('success');
		redirect('Category');
	}
}
?>