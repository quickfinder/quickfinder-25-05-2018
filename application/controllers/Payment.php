<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signIn');
		}
		$this->load->model('Payment_model');
	}
	public function index()
	{
		$data['payment']=$this->Payment_model->getPaymentData();
		$data['page']='payment/paymentlist';
		$this->load->view('templates/content',$data);
	}
	public function details($id)
	{
		$data['page']='payment/paymentdetails';
		$this->load->view('templates/content',$data);
	}
}	
?>