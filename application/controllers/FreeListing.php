<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class FreeListing extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('FreeListing_model');
		$this->load->model('Validation_model');
		$this->load->model('Register_model');
	}
	public function index()
    {
		if($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE) 
		 {
			 $this->load->view('user/signUp');
		 }
		 else
		 {
			$this->load->view('user/free-listing');
		 }
	}
	public function listingForm()	
	{
		$this->form_validation->set_rules('name', 'Company name', 'trim|required');
		//$this->form_validation->set_rules('uname', 'username','trim|required|callback_username_check');
		//$this->form_validation->set_rules('email', '', 'trim|required|valid_email|callback_editemail_check');
		if ($this->form_validation->run() == FALSE)
		{
				$this->load->view('user/free-listing');
		}
		else
		{
			
			if ($this->Register_model->vendorRegister()) 
			{   
				$this->session->set_userdata('msg', "you are successfully register as Vendors");
                redirect('Dashboard');
            }
		}
	}
	public function editemail_check($str)
	{
		if($this->Validation_model->editCheck_email($str))
			return true;
		else
		{
			$this->form_validation->set_message('editemail_check', 'The {field} allready exists ');
			return false;
		}
	}
	public function username_check($str)
	{
		if($this->Validation_model->username_email($str))
			return true;
		else
		{
			$this->form_validation->set_message('username_check', 'The {field} allready exists ');
			return false;
		}
	}
	
}
?>