<!DOCTYPE HTML>
<html>
<head>
<title>Colorful Pricing Tables Widget Flat Responsive Widget Template :: w3layouts</title>
<!-- Custom Theme files -->
<link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- for-mobile-apps -->

<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Monda:400,700' rel='stylesheet' type='text/css'>
<!--google fonts-->

<script src="<?php echo base_url();?>js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>

</head>
<div class="priceing-table w3l">
	<div class="wrap">
		  
		<div class="priceing-table-main">
			<div class="price-grid">
	    		   <div class="price-block agile">
		    			<div class="price-gd-top pric-clr1">
		    				<h4>Entry </h4>
		    				<h3>&#x20B1;99</h3>
		    				<h5>1 month</h5>
		    			</div>
		    			<div class="price-gd-bottom">
		    				<div class="price-list">
			    				<ul>
			    					<li>Full access</li>
			    					<li>Documentation</li>
			    					<li>Customers Support</li>
			    					<li>Free Updates</li>
			    					<li>Unlimited Domains</li>
			    				</ul>
		    				</div>
		    			</div>
		    		    <div class="price-selet pric-sclr1">		    			   
		    			   	  <a class="popup-with-zoom-anim" href="<?php echo site_url('EntryPlanP');?>">Subscribe</a>
		    			</div>
		    		</div>
	    		</div>
				<div class="price-grid" style="display:none">
	    		   <div class="price-block agile">
		    			<div class="price-gd-top1 pric-clr4">
		    				<h4>Silver </h4>
		    				<h3>&#x20B1;499</h3>
		    				<h5>1 month</h5>
		    			</div>
		    			<div class="price-gd-bottom">
		    				<div class="price-list">
			    				<ul>
			    					<li>Full access</li>
			    					<li>Documentation</li>
			    					<li>Customers Support</li>
			    					<li>Free Updates</li>
			    					<li>Unlimited Domains</li>
			    				</ul>
		    				</div>
		    			</div>
		    		    <div class="price-selet pric-sclr1" >		    			   
		    			   	  <a class="popup-with-zoom-anim" style="background:#1daa3f;" href="<?php echo site_url('SilverPlanP');?>">Subscribe</a>
		    			</div>
		    		</div>
	    		</div>
	    		<div class="price-grid" style="display:none">
	    			<div class="price-block agile">
		    			<div class="price-gd-top pric-clr2">
		    				<h4>Gold </h4>
		    				<h3>&#x20B1;799</h3>
		    				<h5>1 months</h5>
		    			</div>
		    			<div class="price-gd-bottom">
		    				<div class="price-list">
			    				<ul>
			    					<li>Full access</li>
			    					<li>Documentation</li>
			    					<li>Customers Support</li>
			    					<li>Free Updates</li>
			    					<li>Unlimited Domains</li>
			    				</ul>
		    				</div>
		    			</div>
		    		    <div class="price-selet pric-sclr2">
		    			   	 <a class="popup-with-zoom-anim" href="<?php echo site_url('GoldPlanP');?>">Subscribe</a>
		    			</div>
		    		</div>
	    		</div>
	    		<div class="price-grid wthree" style="display:none">
	    			<div class="price-block agile">
		    			<div class="price-gd-top pric-clr3">
		    				<h4>Platinum </h4>
		    				<h3>&#x20B1;999</h3>
		    				<h5>1 months</h5>
		    			</div>
		    			<div class="price-gd-bottom">
		    				<div class="price-list">
			    				<ul>
			    					<li>Full access</li>
			    					<li>Documentation</li>
			    					<li>Customers Support</li>
			    					<li>Free Updates</li>
			    					<li>Unlimited Domains</li>
			    				</ul>
		    				</div>
		    			</div>
		    		    <div class="price-selet pric-sclr3">
		    			   	<a class="popup-with-zoom-anim" href="<?php echo site_url('PlatinumPlanP');?>">Subscribe</a>
		    			</div>
		    		</div>
    	       </div>
			   
             <div class="clear"> </div>
		</div>
	</div>
</div>
<!--header end here-->
<!--pop-up-grid-->
				                 