<!-- start: Content -->
<style>
img {
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
    width: 150px;
}

img:hover {
    box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
</style>
<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Store</h3>
                <p class="animated fadeInDown">
                    Store <span class="fa-angle-right fa"></span> List Store
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
					<?php if($this->session->userdata('num_store')){?>
                            <!--<a href="<?php echo site_url('addstore');  ?>">
						
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a>-->
						<form action="<?php echo site_url('Search/Search');?>" method="post"/>
						<select class="form-text cities" name="city" id="city">
                                        <option  selected="selected">Select City</option>
                                        <option value="cebu">Cebu</option>
                                        <option value="manila">Manila</option>
                                        
                                    </select>
						<select class="form-text area" name="area" id="area">
                                        <option value="" selected="selected">Select Area</option>
                                        <option value="apas">Apas</option>
                                        <option value="banilad">Banilad</option>
                                        <option value="basak pardo">Basak Pardo</option>
                                        <option value="capitol site">Capitol Site</option>
                                       <option value="guadalupe">Guadalupe</option>
                                       <option value="kamputhaw">Kamputhaw</option>
                                       <option value="kasambagan">Kasambagan</option>
                                       <option value="labangon">Labangon</option>
                                       <option value="lahug">Lahug</option>
                                       <option value="mabolo">Mabolo</option>
                                        <option value="talamban">Talamban</option>
                                        <option value="mandaue">Mandaue</option>
                                        
                                    </select>
									<div class="col-md-3">
									<div class="form-group form-animate-text" >
                                    <input type="text" class="form-text" name="search" />   
                                    <label>Store Descripion</label>
                                </div>
								</div>
								<input type="submit" value="submit"/>
								</form>
					<?php }else{
						?>
						<a href="<?php echo site_url('pricing');  ?>">
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a> 
						<?php
					} ?>		</div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
									
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
								
								
								
                                foreach ($storelist as $row) {
                                    $i++;
                                    
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td>
                                            <img src="<?php echo base_url().$row['firstimage']; ?>" style="width: 202px;height: 140px">
                                        </td>
                                        <td><?php echo $row['storename']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td><?php echo $row['mobile']; ?></td>
                                    </tr>
                                <?php }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<!-- end: content -->