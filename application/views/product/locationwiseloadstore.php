<div class="deals card">
                <div class="center-sub">
                    <?php
                    if (!empty($storelist)) {

                        foreach ($storelist as $row) {
								
                            ?>
                            <div class="row card">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <a href="<?php echo site_url('storeprofile/') . base64_encode($row['row']['id']) ?>">
                                        <img src="<?php echo base_url(); ?>admin/<?php echo $row['row']['firstimage']; ?>"
                                             class="n-img" alt="store thumbnail">
                                    </a>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <h3 class="vender-name"><?php echo $row['row']['storename']; ?></h3>

                                    <p class="n-contact"><span
                                                class=" n-icon fa-phone fa contact-icon"></span>+91-<?php echo $row['row']['mobile']; ?>
                                    </p>
                                    <div id="rateYo_<?php echo $row['row']['id']; ?>"></div>
                                    <script type="text/javascript">

                                        $(function () {

                                            $("#rateYo_<?php echo $row['row']['id']; ?>").rateYo({
                                                precision: 0,
                                                rating:<?php echo $row['rating'];?>,
                                                starWidth: "15px",
                                                fullStar: true,
                                                readOnly: true,
                                            });
                                        });

										</script>
										
<?php if($this->session->userdata('userid'))
	{
	if($row['favourite']==0){ ?>
<div class="mainclick" id="<?php echo $row['row']['id']; ?>">		
<div class="click" >
	<span class="fa fa-star-o"></span>
	<div class="ring"></div>
	<div class="ring2"></div>
	<p class="info ">Added to favourites!</p>
</div>
</div>
<?php }else{ ?>
<div class="mainclick" id="<?php echo $row['row']['id']; ?>">		
<div class="click active fa-star1" >
	<span class="fa fa-star-o"></span>
	<div class="ring"></div>
	<div class="ring2"></div>
	<p class="info info-tog">Added to favourites!</p>
</div>
</div>	
<?php } } ?>

                                    <p class="address"><span class=" n-icon fa-diamond fa "></span>
                                        <?php echo $row['row']['address']; ?>
                                    </p>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>

                        <div class="row card">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <h5>Not Record Found!..</h5>
                            </div>
                        </div>
                    <?php } ?>
                </div>
           
    </div>