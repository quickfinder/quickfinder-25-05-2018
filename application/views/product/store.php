<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Custom Theme files -->
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!--- materialize css --->
    <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css"
          media="all"/>
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- menu style -->
    <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- banner slider -->
    <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
    <!-- carousel slider -->
	<link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/subcategories.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/n-card.css" rel="stylesheet">
	<style>
	@import url(//fonts.googleapis.com/css?family=Open+Sans:600,400&subset=latin,cyrillic);

h4 {
	text-align: center;
	font-family: 'Open Sans', sans-serif;
	font-weight: 400;
	opacity: 0.7;
}

.click {
	    font-size: 33px;
    color: rgba(0,0,0,.5);
    width: 34px;
    height: 25px;
    margin: 0 auto;
    margin-top: -24px;
    position: relative;
    cursor: pointer;
}

body {
	padding-top: 20px;
}

.click span {
	margin-left: 4px;
	margin-top: -2px;
	z-index: 999;
	position: absolute;
}

span:hover {
	opacity: 0.8;
}



.ring, .ring2 {
	opacity: 0;
	background: grey;
	width: 1px;
	height: 1px;
	position: absolute;
	top: 19px;
	left: 18px;
	border-radius: 50%;
	cursor: pointer;
}

.active span, .active-2 span {
	color: #F5CC27 !important;
}

.active-2 .ring {
	width: 58px !important;
	height: 58px !important;
	top: -10px !important;
	left: -10px !important;
	position: absolute;
	border-radius: 50%;
	opacity: 1 !important;
}

.active-2 .ring {
	background: #F5CC27 !important;
}

.active-2 .ring2 {
	background: #fff !important;
}

.active-3 .ring2 {
	width: 60px !important;
	height: 60px !important;
	top: -11px !important;
	left: -11px !important;
	position: absolute;
	border-radius: 50%;
	opacity: 1 !important;
}

.info {
	font-family: 'Open Sans', sans-serif;
	font-size: 12px;
	font-weight: 600;
	text-transform: uppercase;
	white-space: nowrap;
	color: grey;
	position: relative;
	top: 30px;
	left: -46px;
	opacity: 0;
	transition: all 0.3s ease;
}

.info-tog {
	color: #F5CC27;
    position: relative;
    top: 8px;
    opacity: 1;
    left: 42px;
}


.error{
	color:red;
	font-weight:bold;
	font-size:15px;
}
	</style>
	
    <!-- //font-awesome icons -->

    <!-- js -->
    <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <!-- //js -->
	<script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <!---- materialize js ---->
    <script src="<?php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
     $('.mainclick').click(function() {	    
		var id =$(this).attr('id');
	if ($(this).find('.click').hasClass("fa-star1")) 
	{	
			$.ajax({
	            url: "<?php echo site_url()?>"+'Favourite/removeStore',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
				}
		});
			$(this).find('.click').removeClass('active')
			$(this).find('.click').removeClass('fa-star1');
			$(this).find('.info').removeClass('info-tog')
		
	}else
	{
		$.ajax({
	            url: "<?php echo site_url()?>"+'Favourite/addStore',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
				}
		});
		
					$(this).find('.click').addClass('active');
					$(this).find('.click').addClass('fa-star1');
					$(this).find('.info').addClass('info-tog');
			
	}
})
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/move-top.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->

<link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/filter.css" rel="stylesheet">
<style>

</style>
</head>
<body>
<?php 
	$searchQuery=$this->session->userdata('search_query')!='' ? $this->session->userdata('search_query') : ' ';
	$searchCity=$this->session->userdata('search_city')!='' ? $this->session->userdata('search_city') : 'Cebu';
	$searchArea=$this->session->userdata('search_area')!='' ? $this->session->userdata('search_area') : 'All';;
?>

<datalist id="anything">
    <!---data loaded from database- using ajax-->
</datalist>

<datalist id="cities">
    <option value="Cebu">Cebu</option>
    <option value="Manila">Manila</option>
</datalist>

<datalist id="areas">
</datalist>


<div class="n-ele-center n-white">
    <!-- header -->

    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="header-logo">
                    <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                 src="<?php echo base_url(); ?>images/logo/1.png"
                                                                 alt="quickfinder logo"/></a></h1>
                </div>
                <div class="header-search">
                    <form action="<?php echo site_url('Search') ?>" method="post"/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer pd-0">
                            <input type='search'
                                   placeholder='Search anything in Philippines'
                                   class='productsearch'
                                   data-search-by-word='true'
                                   data-min-length='0'
                                   list='anything'
                                   name='search'
								   value="<?php echo $searchQuery; ?>"
								   />
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer pd-0">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   class='citysearch'
                                   data-min-length='0'
                                   list='cities'
                                   data-search-by-word='true'
                                   placeholder='Select city'
                                   name='flexdatalist-city'
                                   value="<?php echo $searchCity; ?>">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer pd-0">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   placeholder='Select Area'
                                   class='areasearch'
                                   data-search-by-word='true'
                                   data-min-length='0'
                                   list='areas'
                                   name='area'
                                   value="<?php echo $searchArea; ?>">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer searchbtn">
                            <button type="submit" class="btn btn-default" aria-label="Left Align">
                                <i class="fa fa-search" aria-hidden="true"> </i>
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-6">
                        <div class="social-icons">

                            <!-- Dropdown Structure -->
                            <?php if($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE){?>
                                <div class="container-fluid top" style="padding-top: 16px">
								<a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
								<a class="bold"  href="<?php echo site_url('signupform');?>">Register</a>
                                </div>
							<?php }else{?>
							<div class="container-fluid top">
							<ul class="nav navbar-nav">
                                       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?php echo base_url();?><?php echo $this->session->userdata('profilepic');?>"/> <?php echo $this->session->userdata('username');?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('User');?>">My Account</a></li>
                                                <li><a href="<?php echo site_url('changePassword');?>">change password</a></li>
                                                <li><a href="<?php echo site_url('profile');?>">profile</a></li>
												<?php if($this->session->userdata('usertype')==2){?> 
								<li><a href="<?php echo site_url('Dashboard');?>">Dashboard</a></li>
						<?php } ?>
												<li><a href="<?php echo site_url('logout');?>">logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
									</div>
							<?php }?>

                        </div>
                    </div>
            </div>
        </div><!-- //header-two -->
    </div>


    <!-- //header -->
    <!-- banner -->
    <div class="banner pad-tp">
        <div class="carousel carousel-slider center ">
            <a class="carousel-item" href="#one!">
                <img src="<?php echo base_url(); ?>images/subcategories/autocare/auto.png" alt="banner1">
            </a>

        </div>

        <script>
            $(document).ready(function () {

                $('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    indicators: false
                });

            });

        </script>
    </div>
    <!-- //banner -->
    <!-- deals -->
	<div class="bread_crumb">
				<div class="sortbyresult"><a id="l_sortbyresult" href="javascript:void(0);">SORT RESULTS BY</a><i class="left_arrow"></i></div>
				<div class="brdDiv">
				<ul class="brdUL">
					<li style="cursor:pointer;"><a class=" lng_srtfltr" href="#" >Top Results</a></li>
					<?php if($this->session->userdata('userid')){ ?>
					<li style="cursor:pointer;" class="act"><a class="act lng_srtfltr"  href="My-Favorite">My Favourite</a></li>
					<?php }else{?>
					
					<li style="cursor:pointer;" class="act"data-toggle="modal" data-target="#loginModel" ><a class="act lng_srtfltr">My Favourite</a></li>
					<?php }?>
					
					<li style="cursor:pointer;" id="distance">
							<a class="disttab " href="" onclick=""><span class="lng_srtfltr">Distance </span> <i class="res_downarrowic resultimg"></i></a></li>
					<li style="cursor:pointer;" id="rating">
					<a class=""  href=""><span class="lng_srtfltr">Ratings </span><i class="Rtng_ic"></i> </a></li>								
				</ul>
				</div>
	</div>
	<div id="default">
    <div class="deals card">
        <div> <!--class="container"> -->
            <div class="">
                <div class="center-sub">
                    <?php
                    if (!empty($storelist)) {

                        foreach ($storelist as $row) {
								
                            ?>
                            <div class="row card">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <a href="<?php echo site_url('Category/storeprofile/') . base64_encode($row['row']['id']) ?>">
                                        <img src="<?php echo base_url(); ?><?php echo $row['row']['firstimage']; ?>"
                                             class="n-img" alt="store thumbnail">
                                    </a>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <h3 class="vender-name"><?php echo $row['row']['storename']; ?></h3>

                                    <p class="n-contact"><span
                                                class=" n-icon fa-phone fa contact-icon"></span>+91-<?php echo $row['row']['mobile']; ?>
                                    </p>
                                    <div id="rateYo_<?php echo $row['row']['id']; ?>"></div>
                                    <script type="text/javascript">

                                        $(function () {

                                            $("#rateYo_<?php echo $row['row']['id']; ?>").rateYo({
                                                precision: 0,
                                                rating:<?php echo $row['rating'];?>,
                                                starWidth: "15px",
                                                fullStar: true,
                                                readOnly: true,
                                            });
                                        });

										</script>
										
<?php if($this->session->userdata('userid'))
	{
	if($row['favourite']==0){ ?>
<div class="mainclick" id="<?php echo $row['row']['id']; ?>">		
<div class="click" >
	<span class="fa fa-star-o"></span>
	<div class="ring"></div>
	<div class="ring2"></div>
	<p class="info ">Added to favourites!</p>
</div>
</div>
<?php }else{ ?>
<div class="mainclick" id="<?php echo $row['row']['id']; ?>">		
<div class="click active fa-star1" >
	<span class="fa fa-star-o"></span>
	<div class="ring"></div>
	<div class="ring2"></div>
	<p class="info info-tog">Added to favourites!</p>
</div>
</div>	
<?php } } ?>

                                    <p class="address"><span class=" n-icon fa-diamond fa "></span>
                                        <?php echo $row['row']['address']; ?>
                                    </p>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>

                        <div class="row card">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <h5>Not Record Found!..</h5>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
	</div>
	<div id="location" style="display:none">
	<?php echo $this->load->view('product/locationwiseloadstore',$data);?>
	</div>
    <!-- subscribe -->
    <div class="subscribe">
        <div class="">
            <div class="col-md-6 social-icons w3-agile-icons shareicon">
                <h4>Keep in touch</h4>
                <ul>
                    <li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
                    <li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
                    <li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
                    <li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
                    <li><a href="#" class="fa fa-rss icon rss"> </a></li>
                </ul>
                <ul class="apps">
                    <li><h4>Download Our app : </h4></li>
                    <li><a href="#" class="fa fa-apple"></a></li>
                    <li><a href="#" class="fa fa-windows"></a></li>
                    <li><a href="#" class="fa fa-android"></a></li>
                </ul>
            </div>
            <div class="col-md-6 subscribe-right">
                <h4>Sign up for email</h4>
                <form action="#" method="post">
                    <input type="text" name="email" placeholder="Enter your Email..." required="">
                    <input type="submit" value="Subscribe">
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //subscribe -->
    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
        </div>
    </div>

</div>

<div class="modal fade" id="loginModel"
                             style="background-color: transparent; box-shadow: 0px; -webkit-box-shadow: none;">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                                        <h4 class="modal-title">Login </h4>
										
                                    </div>

                <form id="login" method="post">
                                        <div class="modal-body">
										<p id="err" style="display:none" class='error'></p>
    <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="text" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
    <small id="emailHelp" class="form-text text-muted"></small>
  </div>
											
    <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password" required>
  </div>
                        <button type="submit" class="btn btn-success">Submit
                                            </button>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </form>
                                    
                                </div>

                            </div>
                        </div>
						
<div class="modal fade" id="locationModel"
                             style="background-color: transparent; box-shadow: 0px; -webkit-box-shadow: none;">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                                        <h4 class="modal-title">Sort By Location</h4>
											
                                    </div>

                <form id="search" method="post">
                                        <div class="modal-body">
										<p id="err" style="display:none" class='error'></p>
    <div class="form-group">
    <label for="exampleInputEmail1">Select City</label>
    <div class="searchContainer pd-0">
                            
                            <input type='search'
								   
                                   class='citysearch'
                                   data-min-length='0'
                                   list='cities'
                                   data-search-by-word='true'
                                   placeholder='Select city'
                                   name='flexdatalist-city'
                                   value="<?php echo $searchCity; ?>">
                        </div>
    <small id="emailHelp" class="form-text text-muted"></small>
  </div>
											
    <div class="form-group">
    <label for="exampleInputPassword1">Select Area</label>
    <div class="searchContainer pd-0" >
                            <input type='search'
                                   style="border:1px solid"
								   placeholder='Select Area'
                                   class='areasearch'
                                   data-search-by-word='true'
                                   data-min-length='0'
                                   list='areas'
                                   name='area'
                                   value="<?php echo $searchArea; ?>">
    </div>
<input type='hidden' name='flexdatalist-search' value="">	
  </div>
                        <button type="submit" class="btn btn-success">Submit
                                            </button>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </form>
                                    
                                </div>

                            </div>
                        </div>						
                    </div>
<!-- menu js aim -->
<script src="<?php echo base_url(); ?>js/jquery.menu-aim.js"></script>
<script src="<?php echo base_url(); ?>js/main.js"></script> <!-- Resource jQuery -->
<!-- //menu js aim -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<!---auto suggest---->
<link href="<?php echo base_url(); ?>plugins/jquery-flexdatalist/jquery.flexdatalist.min.css" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url(); ?>plugins/jquery-flexdatalist/jquery.flexdatalist.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/countrysearch.js" type="text/javascript"></script>


<!--auto adjust height of all cols----->
<script type="text/javascript" src="<?php echo base_url(); ?>plugins/trunk8/trunk8.js"></script>
<script>
    $(function () {
        $('.address').trunk8();
    });
</script>

<script>

    $(function () {
        $.ajax({
            url: "<?php echo site_url('fetch_data')?>",
            //data: {action: 'test'},
            type: 'post',
            success: function (response) {
                //alert(response);
                $('#anything').html(response);
            }
        });

        $.ajax({
            url: "<?php echo site_url('fetch_areas')?>",
            //data: {action: 'test'},
            type: 'post',
            success: function (response) {
                //alert(response);
                $('#areas').html(response);
            }
        });
	$("#login").submit(function(e) {
		e.preventDefault();
		$.ajax({
			   type: "POST",
			   url: "<?php echo site_url('My-Favorite/userLogin') ?>",
			   data: $("#login").serialize(),
			   dataType: "json",
			   success: function(data)
			   {
					if($.isEmptyObject(data.error))
					{
						$("#err").hide();
					}else
					{
						$("#err").show();
	                	$("#err").html(data.error);
					}
					if(data.success==1)
					{
						window.location.href = "http://34.235.93.56/testwebsites/quickfinder/My-Favorite";
					}
			   }
			 });		
		});
		$("#search").submit(function(e) {
		e.preventDefault();
		$.ajax({
			   type: "POST",
			   url: "<?php echo site_url('locationSearch'); ?>",
			   data: $("#search").serialize(),
			   dataType: "json",
			   success: function(data)
			   {
				   alert(data);
					$('#location').show();
					$('#location').html(data);
					$('#default').hide();	
			   }
			 });		
		});
    });
</script>


</body>
</html>