<!DOCTYPE html>
<html lang="en">

<head>
    <title>Quick Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Custom Theme files -->
    <!--<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>-->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <!--- materialize css --->
    <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all" />
    <!-- menu style -->
    <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all" />
    <!-- banner slider -->
    <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
    <!-- carousel slider -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--//Custom Theme files -->

    <link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">

    <!-- font-awesome icons -->
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/subcategories.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/n-card.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/store-rating.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!--slider photo grid-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/gallery-grid.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <!-- js -->
    <!-- <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
    <!-- //js -->
    <!---- materialize js ---->
    <script src="<?php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/jquery-scrolltofixed-min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function() {

            // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

            //$('.header-two').scrollToFixed();
            // previous summary up the page.
            /*
             var summaries = $('.summary');
             summaries.each(function(i) {
             var summary = $(summaries[i]);
             var next = summaries[i + 1];
             
             summary.scrollToFixed({
             marginTop: $('.header-two').outerHeight(true) + 10,
             zIndex: 999
             });
             });*/
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/move-top.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function() {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({
                easingType: 'easeOutQuart'
            });



        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->

    <style>
        .deals {
            background-color: #f7f7f7;
            padding-bottom: 0px;
        }
        .txt1 {
            color: white;
            font-size: 20px;
        }
        .banner {
            background-image: url('<?php echo base_url() . 'admin/' . $storeprofile[0]['firstimage']; ?>');
            background-repeat: no-repeat;
            height: 100%;
            color: white;
            -moz-background-size: 100% 100%;
            -webkit-background-size: 100% 100%;
            background-size: 100% 100%;
        }
        .checked {
            color: orange;
        }
        .ratingbadge {
            font-size: 15px;
        }
        .bottomdiv {
            width: 100%;
            position: absolute;
            top: auto;
            bottom: -10px;
            z-index: 2;
            filter: alpha(opacity=90);
            opacity: 0.9;
        }
        .block {
            border-left: 1px solid black;
            padding: 5px;
            width: 100%;
            height: 100%;
            color: white;
        }
        .block:hover {
            transform: scale(1.2);
        }
        .txt2 {
            color: white;
        }
        .footerdiv {
            background-color: #f2f2f2;
            padding-top: 10px;
        }
        .layer {
            background-color: rgba(8, 8, 8, 0.7);
        }
        .leftdiv {
            background-color: #f7f7f7;
        }
        .rightdiv {
            background-color: white;
            padding-top: 5px;
            padding-bottom: 15px;
        }
        .panel {
            padding-top: 0px;
            !important
        }
        #panelhead {
            padding-bottom: 0px;
            padding-left: 0px;
            padding-right: 0px;
            padding-top: 0px;
        }
        .spanhead {
            font-size: 18px;
        }
        .spancontent {
            font-size: 16px;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- location marker -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <style type="text/css" rel="stylesheet">
    html, body, #map_canvas 
    {
        height: 100%;
        width: 100%;
    }
    </style>
    <!-- /location marker -->

</head>

<body>
    <?php $searchQuery=$this->session->userdata('search_query')!='' ? $this->session->userdata('search_query') : ' '; $searchCity=$this->session->userdata('search_city')!='' ? $this->session->userdata('search_city') : 'Cebu'; $searchArea=$this->session->userdata('search_area')!='' ? $this->session->userdata('search_area') : 'All';; ?>
    <datalist id="anything">
    </datalist>

    <datalist id="cities">
        <option value="Cebu">Cebu</option>
        <option value="Manila">Manila</option>
    </datalist>

    <datalist id="areas">
    </datalist>


    <div class="n-ele-center n-white">
        <!-- header -->

        <div class="header">
            <div class="header-two">
                <!-- header-two -->
                <div class="container">
                    <div class="header-logo">
                        <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                         src="<?php echo base_url(); ?>images/logo/1.png"
                                                                         alt="quickfinder logo"/></a></h1>
                    </div>
                    <div class="header-search">
                        <form action="<?php echo site_url('Search') ?>" method="post" />
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pd-0">
                            <div class="searchContainer">
                                <input type='search' placeholder='Search anything in Philippines' class='productsearch' data-search-by-word='true' data-min-length='0' list='anything' name='search' value="<?php echo $searchQuery; ?>" />
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                            <div class="searchContainer">
                                <i class="fa fa-map-marker" aria-hidden="true"> </i>
                                <input type='search' class='citysearch' data-min-length='0' list='cities' placeholder='Select city' name='flexdatalist-city' data-search-by-word='true' value="<?php echo $searchCity; ?>">
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                            <div class="searchContainer">
                                <i class="fa fa-map-marker" aria-hidden="true"> </i>
                                <input type='search' placeholder='Select Area' class='areasearch' data-min-length='0' list='areas' name='area' value="<?php echo $searchArea; ?>">
                            </div>
                        </div>

                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pd-0 left">
                            <div class="searchContainer searchbtn">
                                <button type="submit" class="btn btn-default" aria-label="Left Align">
                                    <i class="fa fa-search" aria-hidden="true"> </i>
                                </button>
                            </div>
                        </div>
                        </form>

                    </div>
                    <div class="col-lg-3 col-md-2 col-sm-6 col-xs-6">
                        <div class="social-icons">

                            <!-- Dropdown Structure -->
                            <?php if($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE){?>
                            <div class="container-fluid top" style="padding-top: 16px">
                                <a class="bold" href="<?php echo site_url('signin'); ?>">Login</a> /
                                <a class="bold" href="<?php echo site_url('signupform');?>">Register</a>
                            </div>
                            <?php }else{?>
                            <div class="container-fluid top">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?php echo base_url();?><?php echo $this->session->userdata('profilepic');?>"/> <?php echo $this->session->userdata('email');?> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo site_url('User');?>">My Account</a>
                                            </li>
                                            <li><a href="<?php echo site_url('changePassword');?>">change password</a>
                                            </li>
                                            <li><a href="<?php echo site_url('profile');?>">profile</a>
                                            </li>
                                            <?php if($this->session->userdata('usertype')==2){?>
                                            <li><a href="<?php echo site_url('Dashboard');?>">Dashboard</a>
                                            </li>
                                            <?php } ?>
                                            <li><a href="<?php echo site_url('logout');?>">logout</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <?php }?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- //header-two -->
        </div>


        <!-- //header -->
        <!-- banner -->

        <!-- //banner -->

        <!-- location-->
        s
        <!-- /location-->


        <!-- deals -->
       
        <!-- //subscribe -->
        <div class="footerdiv">
            <div class="row">
         
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <center>
                        <a href="#"><img src="<?php echo base_url() ?>/images/google-play-badge.svg" height="30px"></a>
                        <a href="#"><img src="<?php echo base_url() ?>/images/app_store_badge.svg" height="30"></a>
                        <a href="#"><img src="<?php echo base_url() ?>/images/windows-store-badge.svg" height="30"></a>
                        <a href="#"><img src="<?php echo base_url() ?>/images/get-it-on-blackberry-world.svg" height="30"></a>
                    </center>
                </div>
                <div class="col-sm-4">
                    <ul class="list-inline text-center">
                        <li><a href="#"><img src="<?php echo base_url() ?>/images/facebook.svg" width="30"></a>
                        </li>
                        <li><a href="#"><img src="<?php echo base_url() ?>/images/youtube.svg" width="30"></a>
                        </li>
                        <li><a href="#"><img src="<?php echo base_url() ?>/images/google-plus.svg" width="30"></a>
                        </li>
                        <li><a href="#"><img src="<?php echo base_url() ?>/images/instagram.svg" width="30"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="copy-right">
                <div class="">
                    <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- menu js aim -->
    <script src="<?php echo base_url(); ?>js/jquery.menu-aim.js"></script>
    <script src="<?php echo base_url(); ?>js/main.js"></script>
    <!-- Resource jQuery -->
    <!-- //menu js aim -->
    <!-- Bootstrap core JavaScript
        ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->


    <!---auto suggest---->
    <link href="<?php echo base_url(); ?>plugins/jquery-flexdatalist/jquery.flexdatalist.min.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>plugins/jquery-flexdatalist/jquery.flexdatalist.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>js/countrysearch.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(function() {
            $("#rateYo").rateYo({
                precision: 0,
                //rating:,
                fullStar: true,
                onSet: function(rating, rateYoInstance) {
                    //           alert(rating);
                    $("#rateyo").val(rating);
                }
            });
        });
    </script>
    <!--auto adjust height of all cols -->
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/trunk8/trunk8.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    <script>
        $(function() {
            $('.address').trunk8();
        });
    </script>


    <script src="<?php echo base_url(); ?>plugins/Easy-Table-List-Pagination-Plugin/paginathing.js"></script>
    <script src="https://designshack.net/tutorialexamples/ScrollLinks/jquery.scrollTo.js"></script>
    <script src="https://designshack.net/tutorialexamples/ScrollLinks/jquery.localscroll.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.ul-selector').paginathing({
                // Limites your pagination number
                // false or number
                limitPagination: false,
                // Pagination controls
                prevNext: true,
                firstLast: true,
                prevText: '&laquo;',
                nextText: '&raquo;',
                firstText: 'First',
                lastText: 'Last',
                containerClass: 'pagination-container',
                ulClass: 'pagination',
                liClass: 'page',
                activeClass: 'active',
                disabledClass: 'disabled',
            })

        });
    </script>


    <script>
        $(function() {
            $.ajax({
                url: "<?php echo site_url('fetch_data') ?>",
                //data: {action: 'test'},
                type: 'post',
                success: function(response) {
                    //alert(response);
                    $('#anything').html(response);
                }
            });

            $.ajax({
                url: "<?php echo site_url('fetch_areas') ?>",
                //data: {action: 'test'},
                type: 'post',
                success: function(response) {
                    //alert(response);
                    $('#areas').html(response);
                }
            });

        });
    </script>

   
    <script>
      $(document).ready(function() {
        var geocoder;
        var map;
        var address = "Balewadi, IN";

        function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var myOptions = {
            zoom: 8,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },
            navigationControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        if (geocoder) {
            geocoder.geocode({
            'address': address
            }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                map.setCenter(results[0].geometry.location);

                var infowindow = new google.maps.InfoWindow({
                    content: '<b>' + address + '</b>',
                    size: new google.maps.Size(150, 50)
                });

                var marker = new google.maps.Marker({
                    position: results[0].geometry.location,
                    map: map,
                    title: address
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });

                } else {
                alert("No results found");
                }
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
            });
        }
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    });
    </script>

</body>

</html>