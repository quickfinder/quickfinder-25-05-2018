<?php
error_reporting(0);

# echo "<pre>";print_r($this->session->all_userdata());echo "</pre>";die();

if(!empty($storerating))
{
        $count = 0; //default initialization
		$count = count($storerating);
		$poor = $storerating[$count - 1]['poor'];
		$average = $storerating[$count - 1]['average'];
		$good = $storerating[$count - 1]['good'];
		$verygood = $storerating[$count - 1]['verygood'];
		$excellence = $storerating[$count - 1]['excellence'];
		$rating_value = $storerating[$count - 1]['value'];
        $ratevalue1 = ceil($rating_value/$count);
        
}
else
{
	$count=$poor=$average=$good=$verygood=$excellence=$rating_value=0;
}
//echo $rating_value;
//echo $ratevalue;

($address = ucwords($storeprofile[0]['area'] . ' ' . $storeprofile[0]['city'] . ' ' . $storeprofile[0]['state'] . ' ' . $storeprofile[0]['country']));

if (!empty($storeprofile[0]['storeid'])) {
    $storeid = $storeprofile[0]['storeid'];
} else {
    $storeid = $storeprofile[0]['id'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Quick Finder</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <!-- Custom Theme files -->
        <!--<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>-->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
        <!--- materialize css --->
        <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css"
              media="all"/>
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/><!-- menu style -->
        <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- banner slider -->
        <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
        <!-- carousel slider -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--//Custom Theme files -->
		
		<link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">
		
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/subcategories.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/n-card.css" rel="stylesheet">

		<link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/store-rating.css" rel="stylesheet">
        <!-- //font-awesome icons -->
        <!--slider photo grid-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/gallery-grid.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- store profile -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/store-profile.css">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
        <!-- js -->
        <!-- <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script> -->
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
        <!-- //js -->
        <!---- materialize js ---->
        <script src="<?php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
        
		<script>
            $(document).ready(function () {

                // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

                //$('.header-two').scrollToFixed();
                // previous summary up the page.
                /*
                 var summaries = $('.summary');
                 summaries.each(function(i) {
                 var summary = $(summaries[i]);
                 var next = summaries[i + 1];
                 
                 summary.scrollToFixed({
                 marginTop: $('.header-two').outerHeight(true) + 10,
                 zIndex: 999
                 });
                 });*/
            });
        </script>
        <!-- start-smooth-scrolling -->
        <script type="text/javascript" src="<?php echo base_url(); ?>js/move-top.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- //end-smooth-scrolling -->
        <!-- smooth-scrolling-of-move-up -->
        <script type="text/javascript">
            $(document).ready(function () {

                var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear'
                };

                $().UItoTop({easingType: 'easeOutQuart'});

                
                
            });
        </script>
        <!-- //smooth-scrolling-of-move-up -->

        <!---google charts---->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawAxisTickColors);

            function drawAxisTickColors() {
                var data = google.visualization.arrayToDataTable([
                    ["Element", "Rating", {role: "style"}],
                    ["Excellent",<?php echo $excellence; ?>, "#97daf6"],
                    ["Very Good",<?php echo $verygood; ?>, "#97daf6"],
                    ["Good", <?php echo $good; ?>, "#97daf6"],
                    ["Average", <?php echo $average; ?>, "#97daf6"],
                    ["Poor", <?php echo $poor; ?>, "#97daf6"]
                ]);

                var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                    {
                        calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation"
                    },
                    2]);


                var options = {
                    chartArea: {width: '60%'},
                    legend: {position: 'none'},
                    vAxis: {
                        minValue: 0,
                        maxValue: 100,
                        format: '#%'
                    },
                    hAxis: {
                        baselineColor: '#fff',
                        gridlineColor: '#fff',
                        textPosition: 'none'
                    },
                };
                var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                chart.draw(view, options);
            }
        </script>
        <style>
            
            .deals{
                background-color: #f7f7f7;
				padding-bottom:0px;
            }
          
            .txt1{
                color: white;
                font-size: 20px;
            }
            
        .banner
            {
                
                background-image: url('<?php echo base_url().$storeprofile['firstimage']; ?>');
                
                background-repeat:no-repeat;
                height: 100%;
                color: white;
                 -moz-background-size:100% 100%;
    -webkit-background-size:100% 100%;
    background-size:100% 100%;
                
            }
            .checked {
                color: orange;
            }
            
            .ratingbadge{
                font-size: 15px;
            }
            
            .bottomdiv{
                width: 100%;
                position: absolute;
                top: auto;
                bottom: -10px;
                z-index: 2;
                filter: alpha(opacity=90);
	           opacity: 0.9;
            }
            
            .block{
                border-left: 1px solid black;
                padding: 5px;
                width: 100%;
                height: 100%;
                color: white;
                
            }
            
            .block:hover{
                transform: scale(1.2);
            }
            .txt2{
                color: white;
            }
            .footerdiv{
                background-color: #f2f2f2;
                padding-top: 10px;
            }
            .layer {
                background-color: rgba(8,8,8, 0.7);
            }
            .leftdiv
            {
                background-color: 	#f7f7f7;
            }
            .rightdiv{
                background-color:white;
                padding-top: 5px;
                padding-bottom: 15px;
            }
			.panel{
				padding-top:0px; !important
			}
			#panelhead{
				padding-bottom:0px;
				padding-left:0px;
				padding-right:0px;
				padding-top:0px;
			}
			.spanhead{
				font-size:18px;
			}
			.spancontent{
				font-size:16px;
			}
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
<?php 
$searchQuery=$this->session->userdata('search_query')!='' ? $this->session->userdata('search_query') : ' ';
$searchCity=$this->session->userdata('search_city')!='' ? $this->session->userdata('search_city') : 'Cebu';
$searchArea=$this->session->userdata('search_area')!='' ? $this->session->userdata('search_area') : 'All';;

?>
        <datalist id="anything">
        </datalist>

        <datalist id="cities">
            <option value="Cebu">Cebu</option>
            <option value="Manila">Manila</option>
        </datalist>

        <datalist id="areas">
        </datalist>


        <div class="n-ele-center n-white">
            <!-- header -->

            <div class="header">
                <div class="header-two"><!-- header-two -->
                    <div class="container">
                        <div class="header-logo">
                            <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                         src="<?php echo base_url(); ?>images/logo/1.png"
                                                                         alt="quickfinder logo"/></a></h1>
                        </div>
                        <div class="header-search">
                            <form action="<?php echo site_url('Search') ?>" method="post"/>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pd-0">
                                <div class="searchContainer">
                                    <input type='search'
                                           placeholder='Search anything in Philippines'
                                           class='productsearch'
                                           data-search-by-word='true'
                                           data-min-length='0'
                                           list='anything'
                                           name='search'
										   value="<?php echo $searchQuery; ?>"
										   />
                                </div>
                            </div>


                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                                <div class="searchContainer">
                                    <i class="fa fa-map-marker" aria-hidden="true"> </i>
                                    <input type='search'
                                           class='citysearch'
                                           data-min-length='0'
                                           list='cities'
                                           placeholder='Select city'
                                           name='flexdatalist-city'
                                           data-search-by-word='true'
                                           value="<?php echo $searchCity; ?>">
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                                <div class="searchContainer">
                                    <i class="fa fa-map-marker" aria-hidden="true"> </i>
                                    <input type='search'
                                           placeholder='Select Area'
                                           class='areasearch'
                                           data-min-length='0'
                                           list='areas'
                                           name='area'
                                           value="<?php echo $searchArea; ?>">
                                </div>
                            </div>

                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pd-0 left">
                                <div class="searchContainer searchbtn">
                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <i class="fa fa-search" aria-hidden="true"> </i>
                                    </button>
                                </div>
                            </div>
                            </form>

                        </div>
                        <div class="col-lg-3 col-md-2 col-sm-6 col-xs-6">
                        <div class="social-icons">

                            <!-- Dropdown Structure -->
                            <?php if($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE){?>
                                <div class="container-fluid top" style="padding-top: 16px">
								<a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
								<a class="bold"  href="<?php echo site_url('signupform');?>">Register</a>
                                </div>
							<?php }else{?>
							<div class="container-fluid top">
							<ul class="nav navbar-nav">
                                       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?php echo base_url();?><?php echo $this->session->userdata('profilepic');?>"/> <?php echo $this->session->userdata('email');?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('User');?>">My Account</a></li>
                                                <li><a href="<?php echo site_url('changePassword');?>">change password</a></li>
                                                <li><a href="<?php echo site_url('profile');?>">profile</a></li>
												<?php if($this->session->userdata('usertype')==2){?> 
								<li><a href="<?php echo site_url('Dashboard');?>">Dashboard</a></li>
						<?php } ?>
												<li><a href="<?php echo site_url('logout');?>">logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
									</div>
							<?php }?>

                        </div>
                    </div>
                    </div>
                </div><!-- //header-two -->
            </div>


            <!-- //header -->
            <!-- banner -->
            <div class="banner pad-tp">
                <div class="layer">
                    <div class="carousel carousel-slider center ">

                        <!--<img src="<?php echo base_url(); ?>images/subcategories/autocare/auto.png" alt="banner1">-->
                        <div class="pull-left">
                          <div class="store-core-info">
                            <h1 class="store-name"><?php echo $storeprofile['name']; ?></h1>

                            <p class="text-left">
                               
                                <span class="fl-right" style="margin-top:-26px">
                                    <div id="storerating"></div>
                                        <script type="text/javascript">

                                            $(function () {
                                                $("#storerating").rateYo({
                                                    precision: 0,
                                                    rating: <?php echo $ratevalue1; ?>,
                                                    starWidth: "25px",
                                                    fullStar: true,
                                                    readOnly: true,
                                                });
                                            });
                                            </script>
                                 </span>
                                
                                <!--<span class="txt2"> 6 Votes</span>-->
                            <p class="text-left store-address"><span class="glyphicon glyphicon-map-marker txt1"></span><?php echo $storeprofile['address']; ?><i class="fa fa-map" ></i></p>
                            <p class="text-left  store-phone"><span class="glyphicon glyphicon-phone txt1"></span><?php echo $storeprofile['mobile']; ?></p>
                            </p>
                         </div>

                        <div class="bottomdiv">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class='block'>
                                        <figure><span class="glyphicon glyphicon-map-marker"></span>
                                            <figcaption><h4><a href="<?php echo site_url('store/location'); ?>">Map</a></h4></figcaption>
                                        </figure>            
                                
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class='block'>
                                        <figure><span class="glyphicon glyphicon-earphone text-center"></span>
                                            <figcaption><h4>SMS/Email</h4></figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class='block'>
                                        <figure><span class="fa fa-pencil-square-o"></span>
                                            <figcaption><h4>Write Review</h4></figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class='block'>
                                        <figure><div id="stars-existing" class="starrr" data-rating='4'></div>
                                            <figcaption><h4>Rating</h4></figcaption>
                                        </figure>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class='block'>
                                        <figure><span class="glyphicon glyphicon-thumbs-up"></span>
                                            <figcaption><h4>Verified</h4></figcaption>
                                        </figure>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
                </div>
                

                <script>
                    $(document).ready(function () {

                        $('.carousel.carousel-slider').carousel({
                            fullWidth: true,
                            indicators: false
                        });

                    });

                </script>
                
            </div>
            <!-- //banner -->
            <!-- deals -->
            <div class="deals">
                <div class="row">
                    <div class="col-md-3">
                        <div class="leftdiv">
                            
                            <div class="tz-gallery">
                                <div class="">
                                    <?php
                                    if (!empty($storeprofile['images'])) {
                                        foreach ($storeprofile['images'] as $row) {
                                   print_r($row);
                                            ?>
                                            <div class="col-md-6 gridleft">
                                                <a class="lightbox"
                                                   href="<?php echo base_url(). $row['images']; ?>">
                                                    <img src="<?php echo base_url(). $row['images']; ?>" alt="Park">
                                                </a>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="n-addr">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <p class="n-addrtxt"><span class=""><a role="tab" data-toggle="tab" aria-expanded="false" id='tgla' href="#tabs-demo4-area2">Click here to view your friends rating</a></span></p>
                                </div>
                                <div class="n-mob">
                                    <i class="fa fa-phone"></i><span class="mob-no"><?php echo $storeprofile[0]['mobile']; ?></span>
                                </div>
                                <div class="n-addr">
                                    <p class="pcon"><i class="fa fa-location-arrow"></i></p>
                                    <p class="n-addrtxt"><span class=""><?php echo $address; ?></span></p>
                                </div>
                                <div class="n-addr">
                                    <span class="glyphicon glyphicon-th-list"></span>
                                    <p class="n-addrtxt"><span class="">IT,CMS,SEO,..more</span></p>
                                </div>
                                <div class="n-addr">
                                    <span class="fa fa-money"></span>
                                    <p class="n-addrtxt"><span class="">Moderate(15000Rs-500000)</span></p>
                                </div>
                             
                                <div class="n-addr">
                                    <span class="glyphicon glyphicon-envelope"></span>
                                    <p class="n-addrtxt"><span class=""><a href="#">Send Enquiry Email</a></span></p>
                                </div>
								<div class="n-addr">
                                    <span class="glyphicon glyphicon-globe"></span>
                                    <p class="n-addrtxt"><span class=""><a href="#">www.Trigensoft.com</a></span></p>
                                </div>
                                
                                
                                <hr>
                            <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Today</a></li>
    <li><a data-toggle="tab" href="#menu1">View All</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <h5>
            <span><strong>Open</strong> 24hrs</span>
            <span class="text-right"><strong>Current</strong>:Open</span>
        </h5>
    </div>
    <div id="menu1" class="tab-pane fade">
      <table class="table table-light">
          <thead>
              <tr>
                  <th>Days</th>
                  <th>Open In</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td>Monday</td>
                  <td>Open 24Hours</td>
              </tr>
              <tr>
                  <td>Tuesday</td>
                  <td>Open 24Hours</td>
              </tr>
              <tr>
                  <td>Wednsday</td>
                  <td>Open 24Hours</td>
              </tr>
              <tr>
                  <td>Thursday</td>
                  <td>Open 24Hours</td>
              </tr>
              <tr>
                  <td>Friday</td>
                  <td>Open 24Hours</td>
              </tr>
              <tr>
                  <td>Saturday</td>
                  <td>Open 24Hours</td>
              </tr>
              <tr>
                  <td>Sunday</td>
                  <td>Open 24Hours</td>
              </tr>
          </tbody>
        </table>
    </div>
  </div>
    
                            <hr>
                            <hr>
                                <h4>Other Location</h4>
                                <div class="list-group">
                                    <a href="#" class="list-group-item text-center">Pune</a>
                                    <a href="#" class="list-group-item text-center">Phillipeans</a>
                                </div>

                                
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <!-- <div class="col-md-8"> -->
                        <!-- <div class="container"> -->
                        <!-- <h2>Basic Modal Example</h2> -->


                        <!-- Modal -->
                        <div class="modal fade" id="myModal"
                             style="background-color: transparent; box-shadow: 0px; -webkit-box-shadow: none;">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">

                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Reviews & Rating</h4>
                                    </div>

                                    <form id="ratingstore" action="<?php echo site_url('rating/') . base64_encode($storeprofile['storeid']); ?>"
                                          method="post">
                                        <input type="hidden" name="rateyo" id="rateyo">
                                        <div class="modal-body">
                                            <div id="rateYo" required="">
                                            </div>
                                            <div>
                                                <textarea name="review" id="review" rows="4" cols="50" required="" Placeholder="Write your review"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-success" >Submit
                                            </button>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <!--   -->
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--Login Modal -->
                        <div class="modal fade" id="loginModel"
                             style="background-color: transparent; box-shadow: 0px; -webkit-box-shadow: none;">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">

                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Login </h4>
                                    </div>

                                    <form id="login_rate_store"
                                         method="post">
                                        <div class="modal-body">
                                            <input type="text" name="email" 
											pattern ="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
											
											placeholder="Enter your Email..."
											required=""/>
											
                                            <input type="password" name="pwd" placeholder="Enter your password..."
											required=""/>
                                            <button type="submit" class="btn btn-success">Submit
                                            </button>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <p class="errtitle"></p>
                                        <p class="errmsg"></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-9 pull-right" id='rating'>
                        <div class="col-md-12 tabs-area rightdiv" >
                            <ul id="tabs-demo4" class="nav nav-tabs nav-tabs-v3" role="tablist">
                                <li role="presentation" class="active" id="tglc1"  >
                                    <a href="#tabs-demo4-area1" id="tabs-demo4-1" role="tab" data-toggle="tab"
                                       aria-expanded="true">All Ratings</a>
                                </li>
                                <li role="presentation" id='tgl' class="">
                                    <a href="#tabs-demo4-area3" role="tab" id="tabs-demo4-3" data-toggle="tab"
                                       aria-expanded="false">Friends Rating</a>
                                </li>
                                <li role="presentation" id="tglc2">
                                    <a href="#tabs-demo4-area2" role="tab" id="tabs-demo4-2" data-toggle="tab"
                                       aria-expanded="false">My Ratings</a>
                                </li>

                            </ul>
                            <div id="tabsDemo4Content" class="tab-content tab-content-v3">
                                <div role="tabpanel" class="tab-pane fade active in" id="tabs-demo4-area1"
                                     aria-labelledby="tabs-demo4-area1" id='div1'>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <p style="color:red">Overall Rating (<?php echo $count; ?>) </p>
                                        <span class="fl-right" style="margin-top:-26px">
                                            <div id="rateYoall"></div>
                                            <script type="text/javascript">

                                                $(function () {
                                                    $("#rateYoall").rateYo({
                                                        precision: 0,
                                                        rating: <?php echo $ratevalue1; ?>,
                                                        starWidth: "25px",
                                                        fullStar: true,
                                                        readOnly: true,
                                                    });
                                                });
                                            </script>
                                        </span>
                                        <div id="chart_div"></div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                        <ul class="ul-selector collection">
                                            <?php
                                            if ($storerating != '') {
                                                foreach ($storerating as $rating) {
//                                                    print_r($rating);
                                                    ?>
                                                    <li class="collection-item avatar">

                                                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
                                                            <img class="n-c-icon" src="<?php echo base_url(); ?>asset/img/avatar.jpg"/>
                                                        </div>

                                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pd-0">
                                                            <span class="title"><?php echo $rating['user_details'][0]['first_name'] . ' ' . $rating['user_details'][0]['last_name'] ?></span>
                                                            <span class="fl-right ">
                                                                <div id="rateYo_<?php echo $rating['userid']; ?>"></div>
                                                                <script type="text/javascript">

                                                                    $(function () {

                                                                        $("#rateYo_<?php echo $rating['userid']; ?>").rateYo({
                                                                            precision: 0,
                                                                            rating:<?php echo $rating['rating']; ?>,
                                                                            starWidth: "15px",
                                                                            fullStar: true,
                                                                            readOnly: true,
                                                                        });
                                                                    });
                                                                </script>
                                                            </span>
                                                            <p class="n-mail"><?php echo $rating['user_details'][0]['email']; ?></p>
                                                            <p class="ngrey"><?php echo $rating['review']; ?></p>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>


                                        </ul>


                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area2"
                                     aria-labelledby="tabs-demo4-area2" id='div2'>

                                    <!-- Trigger the modal with a button -->
                                    <?php if ($this->session->userdata('userid')) { ?>
                                        <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal">Reviews &
                                            Rating
                                        </button>
                                    <?php } else { ?>
                                        <button type="button" class="btn btn-info btn-rating" data-toggle="modal" data-target="#loginModel">Reviews &
                                            Rating
                                        </button>
                                    <?php } ?>


                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area3"
                                     aria-labelledby="tabs-demo4-area3" div3>

                                    <!-- Trigger the modal with a button -->
                                    <?php if ($this->session->userdata('userid')) { ?>
                                    <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal">Reviews &
                                        Rating
                                    </button>
                                    <?php } else { ?>
                                    <button type="button" class="btn btn-info btn-rating" data-toggle="modal" data-target="#loginModel">Reviews &
                                        Rating
                                    </button>
                                    <?php } ?>


                                </div>
                            </div>
                            
                            
                            
                        </div>
                        

                    </div>
                    <div class="col-md-9">
					<div class="panel panel-primary">
                        <div class="panel-heading" id='panelhead'><span class='spanhead'>Business Information</span></div>
                        <div class="panel-body">
                            <span class=='spancontent'><strong>Ambience and Service:</strong></span><br>
							<span>Tim Luck Luck in Rahatani comes with an overly informal set-up creating the stage for casual dining. The environment is quintessentially rustic and the décor typical of a Punjabi dhaba. The whole place is adorned with hurricane lighting arrangements and beautiful cane lights that offer a lot of brightness to the eatery. There is a miniature truck and a hand-pulled rickshaw on the premises which are a piece of amusement to the visitors who love to click pictures with them in the backdrop. The boundary walls are covered with graffiti and pictures of trucks. Beautiful trees planted all around impart a lovely look and feel to this venue. The open-air dining area features traditionally decorated and colourful wooden tables and chairs, all neatly laid out. There are big cots as well to provide the diners with a snug and cosy dining experience. The rooftop dining space features tall cane chairs and broad tables where guests can take-in the cool breezy wind while enjoying the lip-smacking food. There are also a few small dining rooms at one of the sides of this venue that feature low-seating arrangements with cushions. The restaurant also has a dedicated home-delivery service for customers who prefer to enjoy food while sitting at home. This dine-in remains open from 11:00 am - 03:00 pm and again from 07:00 pm - 11:45 pm for lunch and dinner respectively. For ordering food online, a click on the 'Order Online' tab will offer a quick access to the restaurant's huge menu.</span>
                            
                        </div>
                    </div>
					</div>
					

                </div>
            </div>
            <!-- //subscribe -->
       <div class="footerdiv">
    <div class="row">
          <ul class="list-inline text-center">
    <li><a href="#">Customer Care</a></li>
    <li><a href="#">Free Listing</a></li>
    <li><a href="#">Advertise</a></li>
    <li><a href="#">About Us</a></li>
    <li><a href="#">Media</a></li>
    <li><a href="#">Testimonial</a></li>
    <li><a href="#">Feedback</a></li>
    <li><a href="#">Business Badge</a></li>
  </ul>
    </div>
  
    <div class="row">
        <div class="col-sm-8">
        <center>
            <a href="#"><img src="<?php echo base_url() ?>/images/google-play-badge.svg" height="30px"></a>
            <a href="#"><img src="<?php echo base_url() ?>/images/app_store_badge.svg" height="30"></a>
            <a href="#"><img src="<?php echo base_url() ?>/images/windows-store-badge.svg" height="30"></a>
            <a href="#"><img src="<?php echo base_url() ?>/images/get-it-on-blackberry-world.svg" height="30"></a>
            </center>
            </div>
        <div class="col-sm-4">
            <ul class="list-inline text-center">
                <li><a href="#"><img src="<?php echo base_url() ?>/images/facebook.svg" width="30"></a></li>
                <li><a href="#"><img src="<?php echo base_url() ?>/images/youtube.svg" width="30"></a></li>
                <li><a href="#"><img src="<?php echo base_url() ?>/images/google-plus.svg" width="30"></a></li>
                <li><a href="#"><img src="<?php echo base_url() ?>/images/instagram.svg" width="30"></a></li>
            </ul>
        </div>
    </div>
            <div class="copy-right">
                <div class="">
                    <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
                </div>
            </div>
</div>
        </div>


        <!-- menu js aim -->
        <script src="<?php echo base_url(); ?>js/jquery.menu-aim.js"></script>
        <script src="<?php echo base_url(); ?>js/main.js"></script> <!-- Resource jQuery -->
        <!-- //menu js aim -->
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->


        <!---auto suggest---->
        <link href="<?php echo base_url(); ?>plugins/jquery-flexdatalist/jquery.flexdatalist.min.css" rel="stylesheet"
              type="text/css"/>
        <script src="<?php echo base_url(); ?>plugins/jquery-flexdatalist/jquery.flexdatalist.js"
        type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>js/countrysearch.js" type="text/javascript"></script>


        <script type="text/javascript">

                                                            $(function () {
                                                                $("#rateYo").rateYo({
                                                                    precision: 0,
                                                                    //rating:,
                                                                    fullStar: true,
                                                                    onSet: function (rating, rateYoInstance) {
                                                                        //           alert(rating);
                                                                        $("#rateyo").val(rating);
                                                                    }
                                                                });
                                                            });
        </script>
        <!--auto adjust height of all cols----->
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/trunk8/trunk8.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
        <script>
                                                            $(function () {
                                                                $('.address').trunk8();
                                                            });
        </script>

        <script>
            baguetteBox.run('.tz-gallery');
        </script>

        <script src="<?php echo base_url(); ?>plugins/Easy-Table-List-Pagination-Plugin/paginathing.js"></script>
<script src="https://designshack.net/tutorialexamples/ScrollLinks/jquery.scrollTo.js"></script>
<script src="https://designshack.net/tutorialexamples/ScrollLinks/jquery.localscroll.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('.ul-selector').paginathing({
                    // Limites your pagination number
                    // false or number
                    limitPagination: false,
                    // Pagination controls
                    prevNext: true,
                    firstLast: true,
                    prevText: '&laquo;',
                    nextText: '&raquo;',
                    firstText: 'First',
                    lastText: 'Last',
                    containerClass: 'pagination-container',
                    ulClass: 'pagination',
                    liClass: 'page',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                })

            });
        </script>


        <script>

            $(function () {
                $.ajax({
                    url: "<?php echo site_url('fetch_data') ?>",
                    //data: {action: 'test'},
                    type: 'post',
                    success: function (response) {
                        //alert(response);
                        $('#anything').html(response);
                    }
                });

                $.ajax({
                    url: "<?php echo site_url('fetch_areas') ?>",
                    //data: {action: 'test'},
                    type: 'post',
                    success: function (response) {
                        //alert(response);
                        $('#areas').html(response);
                    }
                });

            });
        </script>
		
		<script>
		$(document).ready(function (e) {
			
			 /* login model form submit event--to rate store ***/
			 $("#login_rate_store").submit(function(e){
					//alert("Submitted");
				 
				 var serialized_data = $(this).serialize();
				 //alert("form serialize :" +serialized_data);
				 
				 /*
				  ajax request
				  */
				   $.ajax({
					  url: "<?php echo site_url('rating-login/'. base64_encode($storeid)) ?>",
					  type: "POST",
					  //dataType:'json',
					  data : serialized_data
				   })
				   .done(function(response) 
				   {
					  //alert(response);
					  response =$.parseJSON(response);
					  //alert(response);
					  //alert(response['success']);
					  
					  if((response['success']) == 1)
					  {
						  //alert(response['redirect_loc']);
						  
						  window.location.href = response['redirect_loc'];//open new page in same window
					  }
					  else{
						  //set error msg
					    $('.errtitle').text(response['title']);
					    $('.errmsg').text(response['msg']);
					  }
				   });
					
				 e.preventDefault(); //to prevent default
			 });
		});
		</script>
    </body>
<script>
// Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function($, window) {
  var Starrr;

  Starrr = (function() {
    Starrr.prototype.defaults = {
      rating: void 0,
      numStars: 5,
      change: function(e, value) {}
    };

    function Starrr($el, options) {
      var i, _, _ref,
        _this = this;

      this.options = $.extend({}, this.defaults, options);
      this.$el = $el;
      _ref = this.defaults;
      for (i in _ref) {
        _ = _ref[i];
        if (this.$el.data(i) != null) {
          this.options[i] = this.$el.data(i);
        }
      }
      this.createStars();
      this.syncRating();
      this.$el.on('mouseover.starrr', 'span', function(e) {
        return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('mouseout.starrr', function() {
        return _this.syncRating();
      });
      this.$el.on('click.starrr', 'span', function(e) {
        return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('starrr:change', this.options.change);
    }

    Starrr.prototype.createStars = function() {
      var _i, _ref, _results;

      _results = [];
      for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
        _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
      }
      return _results;
    };

    Starrr.prototype.setRating = function(rating) {
      if (this.options.rating === rating) {
        rating = void 0;
      }
      this.options.rating = rating;
      this.syncRating();
      return this.$el.trigger('starrr:change', rating);
    };

    Starrr.prototype.syncRating = function(rating) {
      var i, _i, _j, _ref;

      rating || (rating = this.options.rating);
      if (rating) {
        for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }
      }
      if (rating && rating < 5) {
        for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
      }
      if (!rating) {
        return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
      }
    };

    return Starrr;

  })();
  return $.fn.extend({
    starrr: function() {
      var args, option;

      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      return this.each(function() {
        var data;

        data = $(this).data('star-rating');
        if (!data) {
          $(this).data('star-rating', (data = new Starrr($(this), option)));
        }
        if (typeof option === 'string') {
          return data[option].apply(data, args);
        }
      });
    }
  });
})(window.jQuery, window);

$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {
      
  $('#stars').on('starrr:change', function(e, value){
    $('#count').html(value);
  });
  
  $('#stars-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
});
    
    
    


   /* $(document).ready(function(){
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
*/
    $("#tgla").click(function(){
        $("#tgl").addClass("active");
        $(this).tab('show');
        //$("#div1").addClass('active');
        //$("#div2").removeClass('active');
        //$("#div3").removeClass('active');
        $("#tglc1").removeClass("active");
        $("#tglc2").removeClass("active");
    });
    
    $("#hrefscroll").click(function(){
        $(this).tab('show');
        $("#tgl").removeClass("active");
        $("#tglc1").addClass("active");
        //$("#div1").tab('show');
        //$("#div2").removeClass('active');
        //$("#div3").removeClass('active');
        $("#tglc2").removeClass("active");
        //$('#tabs-demo4-area1').show();
    });
    
</script>
</html>