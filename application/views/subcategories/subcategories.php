<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <!-- Custom Theme files -->
    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>

    <!--- materialize css --->
    <link href="<?php echo base_url();?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="<?php echo base_url();?>css/menu.css" rel="stylesheet" type="text/css" media="all"/> <!-- menu style -->

    <link href="<?php echo base_url();?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/> <!-- banner slider -->
    <link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url();?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"> <!-- carousel slider --><link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo base_url();?>css/subcategories.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="<?php echo base_url();?>js/jquery-3.2.1.min.js"></script>
    <!-- //js -->
<script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <!---- materialize js ---->
    <script src="<?php echo base_url();?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url();?>js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

            //$('.header-two').scrollToFixed();
            // previous summary up the page.
            /*
                    var summaries = $('.summary');
                    summaries.each(function(i) {
                        var summary = $(summaries[i]);
                        var next = summaries[i + 1];

                        summary.scrollToFixed({
                            marginTop: $('.header-two').outerHeight(true) + 10,
                            zIndex: 999
                        });
                    });*/
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->
<link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">

</head>
<body>
<?php 
$searchQuery=$this->session->userdata('search_query')!='' ? $this->session->userdata('search_query') : ' ';
$searchCity=$this->session->userdata('search_city')!='' ? $this->session->userdata('search_city') : 'Cebu';
$searchArea=$this->session->userdata('search_area')!='' ? $this->session->userdata('search_area') : 'All';;
?>
<datalist id="anything">
  </datalist>

<datalist id="cities">
    <option value="Cebu">Cebu</option>
    <option value="Manila">Manila</option>
</datalist>

<datalist id="areas">
 </datalist>



<div class="n-ele-center n-white">
    <!-- header -->
    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="header-logo">
                    <h1><a href="<?php echo site_url();?>"><img class="logo" src="<?php echo base_url();?>images/logo/1.png" alt="quickfinder logo"/></a></h1>
                </div>
                <div class="header-search">
					<form action="<?php echo site_url('Search')?>" method="post"/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <input type='search'
                                   placeholder='Search anything in Philippines'
                                   class='productsearch'
                                   data-min-length='0'
                                   data-search-by-word='true'
                                   list='anything'
                                   name='search'
								   value="<?php echo $searchQuery; ?>"	
								   />
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   class='citysearch'
                                   data-min-length='0'
                                   list='cities'
                                   placeholder='Select city'
                                   name='flexdatalist-city'
                                   data-search-by-word='true'
                                   value="<?php echo $searchCity; ?>">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   placeholder='Select Area'
                                   class='areasearch'
                                   data-min-length='0'
                                   list='areas'
                                   data-search-by-word='true'
                                   name='area'
                                   value="<?php echo $searchArea; ?>">
                        </div>
                    </div>
				
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer searchbtn">
                            <button type="submit" class="btn btn-default" aria-label="Left Align">
                                <i class="fa fa-search" aria-hidden="true"> </i>
                            </button>
                        </div>
					</div>
				</form>

                </div>
				<div class="col-lg-3 col-md-2 col-sm-6 col-xs-6">
                        <div class="social-icons">
						<!-- Dropdown Structure -->
                            <?php if($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE){?>
                                <div class="container-fluid top" style="padding-top: 16px">
								<a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
								<a class="bold"  href="<?php echo site_url('signupform');?>">Register</a>
                                </div>
							<?php }else{?>
							<div class="container-fluid top">
							<ul class="nav navbar-nav">
                                       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?php echo base_url();?><?php echo $this->session->userdata('profilepic');?>"/> <?php echo $this->session->userdata('email');?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('User');?>">My Account</a></li>
                                                <li><a href="<?php echo site_url('changePassword');?>">change password</a></li>
                                                <li><a href="<?php echo site_url('profile');?>">profile</a></li>
												<?php if($this->session->userdata('usertype')==2){?> 
								<li><a href="<?php echo site_url('Dashboard');?>">Dashboard</a></li>
						<?php } ?>
												<li><a href="<?php echo site_url('logout');?>">logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
									</div>
							<?php }?>

                        </div>
                    </div>
            </div>
        </div><!-- //header-two -->
    </div>
    <!-- //header -->
    <!-- banner -->
    <div class="banner pad-tp">
        <div class="carousel carousel-slider center ">
            <a class="carousel-item" href="#one!">
                <img src="<?php echo base_url();?>/images/subcategories/autocare/auto.png" alt="banner1">
            </a>

        </div>
        <script>
            $(document).ready(function () {

                $('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    indicators: false
                });

            });

        </script>
    </div>
    <!-- //banner -->
    <!-- deals -->
    <div class="deals card">
        <div> <!--class="container"> -->
            <div class="">
                <div class="center-sub">
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for <?php echo $this->uri->Segment(2);?> Subcategories.." title="Type in a name">

                    <ul id="myUL">
						<?php 
							if(is_array($subCategoryList))
							{
								foreach($subCategoryList as $row){ 
								$encode = base64_encode($row['id']);
								$cat=$this->uri->segment(2);
								$decode = base64_decode($encode);
						?>
                        <li><a href="<?php echo site_url($cat.'/'.$encode)?>"><?php echo $row['name']?></a></li>
                        <?php }
							}
							else{
						?>
						<li><a href="#">No Record Found!...</a></li>
							<?php }?>
						<li><a href="<?php echo site_url();?>">Back To Category</a></li>		
                    </ul>

                </div>
            </div>

        </div>
    </div>
    <!-- //deals -->


    <!-- subscribe -->
    <div class="subscribe">
        <div class="">
            <div class="col-md-6 social-icons w3-agile-icons shareicon">
                <h4>Keep in touch</h4>
                <ul>
                    <li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
                    <li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
                    <li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
                    <li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
                    <li><a href="#" class="fa fa-rss icon rss"> </a></li>
                </ul>
                <ul class="apps">
                    <li><h4>Download Our app : </h4></li>
                    <li><a href="#" class="fa fa-apple"></a></li>
                    <li><a href="#" class="fa fa-windows"></a></li>
                    <li><a href="#" class="fa fa-android"></a></li>
                </ul>
            </div>
            <div class="col-md-6 subscribe-right">
                <h4>Sign up for email</h4>
                <form action="#" method="post">
                    <input type="text" name="email" placeholder="Enter your Email..." required="">
                    <input type="submit" value="Subscribe">
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //subscribe -->
    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
        </div>
    </div>

</div>


<!-- menu js aim -->
<script src="js/jquery.menu-aim.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<!-- //menu js aim -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<!---auto suggest---->
<link href="<?php echo base_url();?>plugins/jquery-flexdatalist/jquery.flexdatalist.min.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url();?>plugins/jquery-flexdatalist/jquery.flexdatalist.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/countrysearch.js" type="text/javascript"></script>


<!--auto adjust height of all cols----->
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-match-height/jquery.matchHeight.js"></script>
<script>
    $(function() {
        $('.deals').matchHeight();
    });
</script>

<script>
    function myFunction() {
        var input, filter, ul, li, a, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";

            }
        }
    }
</script>

<script>

    $(function() {
        $.ajax({
            url: "<?php echo site_url('fetch_data')?>",
            //data: {action: 'test'},
            type: 'post',
            success: function(response)
            {
                //alert(response);
                $('#anything').html(response);
            }
        });

        $.ajax({
            url: "<?php echo site_url('fetch_areas')?>",
            //data: {action: 'test'},
            type: 'post',
            success: function(response)
            {
                //alert(response);
                $('#areas').html(response);
            }
        });

    });
</script>


</body>
</html>