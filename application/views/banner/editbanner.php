
 <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Banner </a></li>
                            <li class="active">Edit Banner
 

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">
<div class="form-element">
        <div class="col-md-12">
            <div class="col-md-12 panel">
                <div class="col-md-12 panel-heading">
                    <h4>Edit Banner Form</h4>
                </div>
                <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                    <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" enctype="multipart/form-data" action="<?php echo site_url('editbanner/'.$this->uri->segment(2)); ?>">
                            <div class="col-md-6">
                                <div class="form-group">

                                  <span class="image-preview-input-title">Browse</span>
                                                <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                    <div>
                                    <span class="error"><?php
                                        if (!empty($error)) {
                                            echo $error['error'];
                                        }
                                        ?></span>
                                        </div>
                                </div>
                               <!--  <div class="form-group">

                                <span class="image-preview-input-title">Browse</span>
                                                <input type="file" accept="image/png, image/jpeg, image/gif" name="file_name" required />
                                        
                                </div> -->
                                <div class="form-group">

<?php 
 // $banner->startdate.'-'.$banner->enddate;

$startdate = date("d/m/Y", strtotime($banner->startdate));
$enddate = date("d/m/Y", strtotime($banner->enddate));
// echo $startdate .'-'.$enddate;

?>
                                <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                            <input type="text" class="form-control daterange-basic" value="<?php echo $startdate .'-'.$enddate;?>" name="startenddate" value="Enter Date"> 
                                        </div>
                                        </div>

<!-- 
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text date" value="" name="startdate" required>
                        <span class="bar"></span>
                        <label>Start Date </label>
                      </div>
                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text date" value="" name="enddate" required>
                        <span class="bar"></span>
                        <label>End Date </label>
                      </div>
 -->



                            </div>                   
                            <div class="col-md-12">
                                <input class="submit btn btn-danger" type="submit" value="Submit">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>






</div>