
 <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Banner </a></li>
                            <li class="active">Banner List
 

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">


    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
               



                    <div class="panel-heading">
                        
                            <div class="heading-elements">
     <!-- <a href="<?php echo site_url('adduser');  ?>"> -->

                                <ul class="icons-list">
                                <a href="<?php echo site_url('add-banner');  ?>">
                            <button class="btn ripple btn-gradient btn-primary" style="width:150px">
                                <span>Add Banner</span>
                            </button>
                        </a> 
                                    
                                </ul>
                            </div>
                        </div>
                <div class="panel-body">

                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-bordered table-hover datatable-highlight" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Image</th>
									<th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Operations </th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--<a href="<?php echo site_url('addSubCategory/' . $row['id']); ?>"></a>-->
                                <?php
                                if (is_array($bannerlist)) {
                                    $i = 0;
                                    foreach ($bannerlist as $row) {

                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><img class="icon-img" width="250px" hight="100px" src="<?php echo base_url() ?>admin/<?php echo $row['images'] ?>"></td>
                                            <td><?php echo $row['startdate']; ?></td>
											<td><?php echo $row['enddate']; ?></td>
<td>  
                                                
                                                <?php if ($row['IsActive'] == 1) { ?>
                                                 <a href="<?php echo site_url('deactivebanner/' . $row['id']); ?>" > <span class="label label-success" title="Active Banner">Active</span></a>
                                                   
                                                <?php } else { ?>

                                                 <a href="<?php echo site_url('activebanner/' . $row['id']); ?>"> <span class="label label-danger" title="Unactive Banner">Inactive</span></a>


                                                   
                                                <?php }
                                                ?></td>


                                            <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <!-- <li><a href="<?php echo site_url('viewstore/'.$row['id']);  ?>"><i class=" icon-pencil4"></i> Search</a></li> -->
                                                    <li><a href="<?php echo site_url('edit-banner/'. $row['id']); ?>"><i class="icon-pencil3"></i>Edit </a></li>
                                                    <li><a href="#" id="<?php echo $row['id']; ?>" class="deletecategories "><i class="icon-box"></i> Delete</a></li>
                                                     
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>



											
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td>NO Record Found !...</td>

                                    </tr>
                                    <?php
                                }
                                ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<!-- end: content -->
