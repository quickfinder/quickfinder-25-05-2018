

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h1><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Keywords</span> 

</h1>
                        </div>

                       
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Keyword</a></li>
                            <li class="active">List Keywords

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">


                    <!-- Highlighting rows and columns -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">

                          <!--   <h5 class="panel-title"><?php if($this->session->userdata('usertype')==3){?>
                        <a href="<?php echo site_url('adminaddnewstores');  ?>">
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a>
                    <?php }else{ ?>
                    
                    <a href="<?php echo site_url('Users');  ?>">
                            <button class=" btn btn-gradient btn-primary" >
                                <span>Back</span>
                            </button>
                        </a>
                    <?php   
                    }
                    
                    ?>      </h5> -->
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="reload"></a></li>
                                </ul>
                            </div>
                        </div>


                        <form action="<?php echo site_url('addKeyword');?>" class="form-signin" method="post">
          <div class="panel periodic-login" style="">
              <!-- <span class="atomic-number">28</span> -->
              <div class="panel-body text-center">

  
                  <!-- <h1 class="atomic-symbol">Mi</h1>
                  <p class="atomic-mass">14.072110</p>
                  <p class="element-name">Miminium</p> -->

                 
                 
                  
                  <div class="form-group form-animate-text" style="margin-top:30px !important;">
                    <input type="text" class="form-text" id="keyword" value="" name="keyword" required>
                    <span class="bar" id="result"><?php echo form_error('keyword', '<div class="error">', '</div>'); ?></span>
                    <label> Enter Keyword</label>
                  </div>
                 
                  <input type="submit" class="btn btn-primary col-md-12" value="submit"/>
              </div>
                
          </div>
        </form>
                    </div>
                    <!-- /highlighting rows and columns -->

                    </div>
                   