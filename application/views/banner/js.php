
<script type="text/javascript">
$(document).ready(function(){
	  $('.deletebanner').click(function(){
	
		  var id=$(this).attr('id');
		 
		  var btn = this;
		  var table = $('#datatables-example').DataTable();
		  swal({
					  title: "Are you sure?",
					  text: "Once deleted, you will not be able to recover !",
					  icon: "warning",
					  buttons: true,
					  dangerMode: true,
				})
				.then((willDelete) => {
				if (willDelete) 
				{
				$.ajax({
	            url: "<?php echo site_url()?>"+'Banner/bannerdelete',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						swal("Poof! Your Record has been deleted!", {
						icon: "success",
						});
						table.row($(btn).parents('tr')).remove().draw(false);
					}
				}
					});	
				} 
				else {
					swal("Your data is safe!");
					}
				});
	  });
	  
	 
});
</script>