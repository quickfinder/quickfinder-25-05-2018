<!-- start: Content -->
 <style>
 .tags input, li.addedTag {
    border: 1px solid transparent;
    border-radius: 2px;
    box-shadow: none;
    display: block;
    padding: 0.5em;
}
@-moz-document url-prefix() {

  .tags {
    display: inline;
    padding:0em;
    width: 90%;
	}
	ul{
	list-style-type: disc;
    margin-before: 1em;
    margin-after: 1em;
    margin-start: 0px;
    margin-end: 0px;
    padding-start: 40px;
	}
}
@media screen and (-webkit-min-device-pixel-ratio:0) {
.tags{
	display:inline;
    padding:20em;
    width:100%;
}
}
	
.tags li.tagAdd, .tags li.addedTag {
    float: left;
    margin-left: 0.25em;
    margin-right: 0.25em;
}
.tags li.addedTag {
    background: none repeat scroll 0 0 #4FC3F7;
    border-radius: 2px;
    color: #fff;
    padding: 0.25em;
	margin-bottom: 2px;
}
</style>
  



    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    
                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Category</a></li>
                            <li class="active">Category List

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">


                    <!-- Highlighting rows and columns -->
                    <div class="panel panel-flat">


                    <div class="panel-heading">
                        
                            <div class="heading-elements">


                                                    <!-- <a href="<?php echo site_url('adduser');  ?>"> -->

                                <ul class="icons-list">
                                <a href="<?php echo site_url('addCategory'); ?>">
                            <button class="btn btn-primary ripple btn-gradient" style="width:150px">
                                <span>Add Category</span>
                            </button>
                            
                        </a>  

                                </ul>
                            </div>
                        </div>
                    


                        <table id="datatables-example" class="table table-bordered  datatable-highlight">
                            <thead>
                                <tr>
                                   <th>Sr . No</th>
                                    <th>Category Name</th>
                                    <th class="td">Icon</th>
                                    <th>Subcategories</th>
                                    <th>Status</th>
                                    <th>Operations </th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php
                                
                                if (is_array($categorylist)) {
                                    $i = 0;
                                    foreach ($categorylist as $row) {
                                        
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $row['category']['name']; ?></td>
                                            <td><img class="icon-img" width="50px" hight="50px" src="<?php echo base_url()?><?php echo $row['category']['icons']?>"></td>
                                            
                                            <td ><?php
                                            if ($row['subcategories'] == '') {
                                                 echo "<ul class='tags'>";
                                                 echo "<li ></li>";
                                            
                                            echo "</ul>";
                                                // echo "<td></td>";                               
                                                             }else{
                                            $sub_cats = Array();
                                            echo "<ul class='tags'>";
                                            foreach($row['subcategories'] as $u) 
                                            {
                                                echo "<li class='addedTag'>".$u['name']."</li>";
                                            }
                                            echo "</ul>";
                                        }
                                            //$str = implode(",",$sub_cats);
                                            //echo $str;
                                            ?>
                                            
                                            </td>
                                            <td>
                                                <?php if($row['category']['IsActive']==1){?>
                            <a href="<?php echo site_url('deactiveCategory/'.$row['category']['id']); ?>" > <span class="label label-success">Active</span></a>
                            <?php }else{ ?>
                            <a href="<?php echo site_url('activeCategory/'.$row['category']['id']); ?>"> <span class="label label-danger">Inactive</span></a>
                            <!-- <a href="<?php echo site_url('activeCategory/'.$row['category']['id']); ?>" ><i class=" icon-cross3" >Inactive</i></a> -->
                            <?php   
                            } ?>
                                            </td>


                                            <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <!-- <li><a href="<?php echo site_url('viewstore/'.$row['id']);  ?>"><i class=" icon-pencil4"></i> Search</a></li> -->
                                                    <li><a href="<?php echo site_url('addSubCategory/'.$row['category']['id']); ?>"><i class="icon-pencil3"></i>Edit Subcategories</a></li>
                                                    <li><a href="#" id="<?php echo $row['category']['id'];?>" class="deletecategories "><i class="icon-box"></i> Delete</a></li>
                                                     <li><a href="<?php echo site_url('editcategories/'.$row['category']['id']); ?>" id="<?php echo $row['category']['id'];?>" ><i class="icon-pencil4"></i> Edit categories</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                            
                                        
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td>NO Record Found !...</td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /highlighting rows and columns -->

                    </div>




