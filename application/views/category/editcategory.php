<?php ?>







    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    
                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Category</a></li>
                            <li class="active">Edit Category 

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">





    <div class="form-element">
        <div class="col-md-12">
            <div class="col-md-12 panel">
                <div class="col-md-12 panel-heading">
                    <h4>Edit Category Form</h4>
                </div>
				
                <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                    <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" enctype="multipart/form-data" action="<?php echo site_url('editcategories/'.$this->uri->segment(2)); ?>">
                            <div class="col-md-6">
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" value="<?php echo $category[0]->name;?>" id="validate_firstname" name="name" required>
                                    <span class="bar"></span>
                                    <label>Category Name</label>
                                </div>
                                <div class="form-group">

                                 <span class="image-preview-input-title">Browse</span>
                                                <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                   
                                    <span class="error"><?php
                                        if (!empty($error)) {
                                            echo $error['error'];
                                        }
                                        ?></span>
                                </div>

                            </div>                   
                            <div class="col-md-12">
							<a href="<?php echo site_url('backtoedicategory');?>"> <input class="submit btn btn-success" type="button" value="Back"></a>
                                <input class="submit btn btn-danger" type="submit" value="Submit">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>