<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Quickfinder login page</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/css/colors.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- Core JS files -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/pace.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->


  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
  <!-- /theme JS files -->

</head>

<body class="login-container">

  <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">

          <!-- Simple login form -->
      <form action="<?php echo site_url('login');?>" class="form-signin" method="post">
            <div class="panel panel-body login-form">
              <div class="text-center">
                            <div >
                              
                             <a href="<?php echo site_url('');?>"><img class="sign_in_logo" src="<?php echo base_url();?>images/logo/1.png" alt="quickfinder logo" height="100"/></a></div>
                <!-- <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div> -->
                  <?php 
                 
          echo $this->session->userdata('msg');
         $this->session->unset_userdata('msg');
          ?></p>
          <p id="errmsg" style="color:red;
    font-size: 15px;
    font-weight: bold">
          <?php 
           echo $this->session->userdata('errmsg');
           $this->session->unset_userdata('errmsg');
          
          ?></p>
                <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
              </div>

              <div class="form-group has-feedback has-feedback-left">

                    
                <input type="text" class="form-control" name="username" required placeholder="Username">
                <div class="form-control-feedback">
                <span class="bar"><?php echo form_error('username', '<div class="error">', '</div>'); ?></span>
                  <i class="icon-user text-muted"></i>
                </div>
              </div>

              <div class="form-group has-feedback has-feedback-left">
                <input type="password" class="form-control"  name="password" required placeholder="Password">
                <div class="form-control-feedback">
                    <span class="bar"><?php echo form_error('password', '<div class="error">', '</div>'); ?></span>
                  <i class="icon-lock2 text-muted"></i>
                </div>
              </div>

              <div class="form-group">

                <button type="submit" class="btn bg-pink-400 btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
              </div>

              <div class="text-center">

<a href="<?php echo site_url('forgotpassword');?>">Forgot Password |</a>
           <a href="<?php echo site_url('signupform');?>">Create New Acoount</a>
              </div>
            </div>
          </form>
          <!-- /simple login form -->


          <!-- Footer -->
        <!--   <div class="footer text-muted text-center">
            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
          </div> -->
          <!-- /footer -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->

</body>
</html>




























