<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Custom Theme files -->
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!--- materialize css --->
    <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css"
          media="all"/>
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- menu style -->
    <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- banner slider -->
    <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
    <!-- carousel slider --><link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo base_url(); ?>css/circularwaves.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
    <!-- //js -->

    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>

    <!---- materialize js ----
    <script src="<php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
  -->
    
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
		
    </script>
    <!-- //smooth-scrolling-of-move-up -->


    <link href="<?php echo base_url(); ?>asset/css/fade_effect_slider.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">
	
<link href="<?php echo base_url(); ?>css/passwordstatus.css" rel="stylesheet">
<style>
.left-group
{
      margin: auto;
    float: none;

}
.top{
	padding-top:20px;
}
.bold{
	color:#2d383e;font-weight:bold;
	padding-top:17px;
}
.navbar-nav > li > a {
    padding-top: 21px;
    padding-bottom: 15px;
}
.error{
	color:red !important;
}
</style>
</head>
<body>



<div class="n-ele-center n-white">
    <!-- header -->

    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="row">


                    <div class="col-lg-1 col-md-2 col-sm-6 col-xs-6">
                        <div class="header-logo">
                            <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                         src="<?php echo base_url(); ?>images/logo/1.png"
                                                                         alt="quickfinder logo"/></a></h1>
                        </div>
                    </div>


                    <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6 my-acc">
                         <div class="row">
                             <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 acc-heading">
                                 <h4>Register</h4>
                             </div>
                             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                 
                             </div>
                             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <a href="<?php echo site_url('freelisting');?>"> <button class="btn waves-effect waves-light">
                                     <span>Free Listing</span>
                                 </button>
								 </a>
                             </div>
                         </div>

                    </div>


                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                        <div class="social-icons">

                            <!-- Dropdown Structure -->
                            <nav class="navbar">
                                <div class="container-fluid top">
								<a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
								<a class="bold"  href="<?php echo site_url('signupform');?>">Register</a>
                                </div>
                            </nav>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div><!-- //header-two -->
    </div>


    <!-- //header -->


    <div class="deals">
        <div> <!--class="container"> -->
            <div class="row">
                
					
                <div class="col-lg-4 col-md-8 col-sm-4 col-xs-4 left-group">
                    
					<p class="frm-header">Enter details below to sign up<span>*</span></p>
					
					<form method="post" action="<?php echo site_url('signupform');?>">
							<div class="form-group">
							  <label for="usr">First Name:</label>
							  <input type="text" class="form-control" value="<?php echo set_value('fname'); ?>" id="fname" name="fname" placeholder="Please enter first name" required>
							   <?php echo form_error('fname', '<p class="error">', '</p>'); ?></p>
							</div>
							<div class="form-group">
							  <label for="usr">Last Name:</label>
							  <input type="text" class="form-control" value="<?php echo set_value('lname'); ?>" id="lname" name="lname" placeholder="Please enter last name"  required>
							  <?php echo form_error('lname', '<p class="error">', '</p>'); ?>	
							</div>
							<div class="form-group">
							  <label for="usr">Email:</label>
							  <input type="email" class="form-control" value="<?php echo set_value('email'); ?>" id="email" name="email" placeholder="Please enter your email" required>
							  <?php echo form_error('email', '<p class="error">', '</p>'); ?>
							</div>
							<div class="form-group">
							  <label for="usr">Mobile:</label>
							  <input type="text" class="form-control" pattern="[789][0-9]{9}" value="<?php echo set_value('mobile'); ?>" id="mobile" name="mobile" placeholder="Please enter mobile number" required>
							  <?php echo form_error('mobile', '<p class="error">', '</p>'); ?>	
							</div>
							
							<div class="form-group">
							  <label for="usr">User Name:</label>
							  <input type="text" class="form-control" value="<?php echo set_value('username'); ?>" id="lname" name="username" placeholder="Please enter user name" required>
							  <?php echo form_error('username', '<p class="error">', '</p>'); ?>	
							</div>
							
							<div class="form-group">
							  <label for="usr">Create Password:</label>
							  <input type="hidden" name="password_strength" id="password_strength"/>
							  <input type="Password" class="form-control" placeholder="Enter password" id="password" name="password" required>
							  <p style="color:red !important;"><?php echo form_error('password', '<p class="error">', '</p>'); ?></p>	
							  <span id="result"></span>	
							</div>
							<button type="submit" class="btn waves-effect waves-light">
                                     <span>Submit</span>
                                 </button>
</form>
                </div>
            </div>
        </div>
    </div>


    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
        </div>
    </div>

</div>

<script>
   $(document).ready(function () {
     /*
	  * password_strength
	  *
	  */
    $( "#password" ).keyup(function() {
	  //alert( "Handler for .keyup() called." );
	  var class_name = $('#result').attr('class');
	  $('#password_strength').val(class_name);
	});   
	
   });
</script>


</body>
</html>