<?php
//error_reporting(0);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Quick Finder</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <!-- Custom Theme files -->
        <style>
            //Reusing bootstrap 3 panel CSS

            .panel {
                background-color: #FFFFFF;
                border: 1px solid rgba(0, 0, 0, 0);
                border-radius: 4px 4px 4px 4px;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
                margin-bottom: 20px;
            }   

            .panel-primary {
                border-color: #428BCA;
            }   

            .panel-primary > .panel-heading {
                background-color: #428BCA;
                border-color: #428BCA;
                color: #FFFFFF;
            }   

            .panel-heading {
                border-bottom: 1px solid rgba(0, 0, 0, 0);
                border-top-left-radius: 3px;
                border-top-right-radius: 3px;
                padding: 10px 15px;
            }   

            .panel-title {
                font-size: 16px;
                margin-bottom: 0;
                margin-top: 0;
            }   

            .panel-body:before, .panel-body:after {
                content: " ";
                display: table;
            }   

            .panel-body:before, .panel-body:after {
                content: " ";
                display: table;
            }   

            .panel-body:after {
                clear: both;
            }   

            .panel-body {
                padding: 15px;
            }   

            .panel-footer {
                background-color: #F5F5F5;
                border-bottom-left-radius: 3px;
                border-bottom-right-radius: 3px;
                border-top: 1px solid #DDDDDD;
                padding: 10px 15px;
            }

            //CSS from v3 snipp
            .user-row {
                margin-bottom: 14px;
            }

            .user-row:last-child {
                margin-bottom: 0;
            }

            .dropdown-user {
                margin: 13px 0;
                padding: 5px;
                height: 100%;
            }

            .dropdown-user:hover {
                cursor: pointer;
            }

            .table-user-information > tbody > tr {
                border-top: 1px solid rgb(221, 221, 221);
            }

            .table-user-information > tbody > tr:first-child {
                border-top: 0;
            }


            .table-user-information > tbody > tr > td {
                border-top: 0;
            }

        </style>
        <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
        <!--- materialize css --->
        <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css"
              media="all"/>
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- menu style -->
        <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- banner slider -->
        <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
        <!-- carousel slider --><link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- //Custom Theme files -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">


        <link href="<?php echo base_url(); ?>css/circularwaves.css" rel="stylesheet">
        <!-- //font-awesome icons -->
        <!-- js -->
        <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
        <!-- //js -->

        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>

        <!---- materialize js ----
        <script src="<php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
        -->
        <script>
            $(document).ready(function () {


            });
        </script>
        <!-- start-smooth-scrolling -->
        <script type="text/javascript" src="js/move-top.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- //end-smooth-scrolling -->
        <!-- smooth-scrolling-of-move-up -->
        <script type="text/javascript">
            $(document).ready(function () {

                var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear'
                };

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <!-- //smooth-scrolling-of-move-up -->


        <link href="<?php echo base_url(); ?>asset/css/fade_effect_slider.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">

    </head>
    <body>



        <div class="n-ele-center n-white">
            <!-- header -->

            <div class="header">
                <div class="header-two"><!-- header-two -->
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-lg-1 ">
                                <div class="header-logo">
                                    <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                                 src="<?php echo base_url(); ?>images/logo/1.png"
                                                                                 alt="quickfinder logo"/></a></h1>
                                </div>
                            </div>


                            <div class="col-lg-7 my-acc">
                                <div class="row">
                                    <div class="col-lg-6  acc-heading">
                                        <h4>My Account</h4>
                                    </div>
                                    <!--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <p><?php echo $this->session->userdata('email'); ?></p>-->
                                    <!--</div>-->
                                    <div class="col-lg-6 ">
                                        <?php if ($this->session->userdata('usertype') == 1) { ?>
                                            <a href="<?php echo site_url('freelisting'); ?>"> <button class="btn pull-right  waves-effect waves-light">
                                                    <span>List Your Business</span>
                                                </button>
                                            <?php } ?>
                                        </a>
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-3 " >
                                <div class="social-icons">

                                    <!-- Dropdown Structure -->
                                    <nav class="navbar">
                                        
                                        <?php if ($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE) { ?>
                                            <div class="container-fluid top">
                                                <a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
                                                <a class="bold"  href="<?php echo site_url('signupform'); ?>">Register</a>
                                            </div>
                                        <?php } else { ?>
                                        
                                            <ul class="nav navbar-nav pull-right">
                                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <img src="<?php echo base_url(); ?><?php echo $this->session->userdata('profilepic'); ?>" alt="userpic"/>
                                                     <?php echo $this->session->userdata('username'); ?> 
                                                    <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="<?php echo site_url('User'); ?>">My Account</a></li>
                                                        <li><a href="<?php echo site_url('changePassword'); ?>">Change Password</a></li>

                                                        <li><a href="<?php echo site_url('my-profile'); ?>">Profile</a></li>
                                                        
                                                        <?php if ($this->session->userdata('usertype') == 2) { ?> 
                                                            <li><a href="<?php echo site_url('Dashboard'); ?>">Dashboard</a></li>
                                                        <?php } ?>
                                                        <li><a href="<?php echo site_url('logout'); ?>">Logout</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        <?php } ?>
                                    </nav>

                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                    <!--</div>-->
                </div><!-- //header-two -->
            </div>


            <!-- //header -->

            <div class="container deals" style="width:80%">
                <div class=" span8 offset2">
                    <!-- <div class="row-fluid user-row">
                        <div class="span1">
                            <img class="img-circle"
                                 src="<?php echo base_url() .$this->session->userdata('profilepic');  ?>"
                                 alt="User Pic" height="100px" width="100px"/>
                        </div>
                        <div class="">
                            <h5><strong><?php echo $userinfo[0]['first_name'] . ' ' . $userinfo[0]['last_name']; ?></strong></h5>
                            <span class="text-muted">User level: Administrator</span>
                        </div>
                        
                    </div> --><br>
                    <div class="row-fluid center user-infos cyruxx">
                        <div class=" offset1">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><b>User Profile</b></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row-fluid">
                                        <div class="span3 center">
                                            <img class="img-circle"
                                                 src="<?php echo base_url() .$this->session->userdata('profilepic');  ?>"
                                                 alt="User Pic" height="100px" width="100px">
                                        </div>
                                        <div class="span6 center">
                                            <strong><h6><b><?php echo $userinfo[0]['first_name'] . ' ' . $userinfo[0]['last_name']; ?></b></h6></strong><br>

                                            <!-- <table class="table table-condensed table-responsive table-user-information">
                                                <tbody>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td><?php echo $userinfo[0]['first_name'] . ' ' . $userinfo[0]['last_name']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td><?php echo $userinfo[0]['email'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Username</td>
                                                        <td><?php echo $userinfo[0]['username'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contact</td>
                                                        <td><?php echo $userinfo[0]['mobile'] ?></td>
                                                    </tr>
                                                </tbody>
                                            </table> -->
                                            <div class="row">
                                               <div class=" col-md-2"></div>
                                                <div class="col-md-8">
                                             <table class=" table bordered table-condensed table-responsive ">
                                                <tr class="text-center ">
                                                        <td><b>Email</b></td>
                                                        <td><?php echo $userinfo[0]['email'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Username</b></td>
                                                        <td><?php echo $userinfo[0]['username'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Contact</b></td>
                                                        <td><?php echo $userinfo[0]['mobile'] ?></td>
                                                    </tr>
                                             </table>
                                         </div>
                                         <div class=" col-md-2"></div>       


                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="<?php echo site_url('profile'); ?>" class="btn  btn-info " type="button"  data-toggle="tooltip" title="Edit">Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="copy-right">
                <div class="">
                    <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
                </div>
            </div>

        </div>



    </body>
</html>