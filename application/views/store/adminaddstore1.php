    <style>
        .ap{
            padding:50px;
            border:1px solid #000;
        }
        .addbtn{
            padding: 5px 18px !important;
            margin: 0px !important;
        }
        .btnset
        {
            height: 160px !important;
        }
        .bar
        {
            color:red;
        }

        .panel-heading {
    /*line-height: 1.42857143 !important;*/
     color: #FFF !important; 
}
        </style>
         <link href='//fonts.googleapis.com/css?family=Monda:400,700' rel='stylesheet' type='text/css'>

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h1><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add Store</span> 

</h1>
                        </div>

                       
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Stores</a></li>
                            <li class="active">Add Store

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">
                    <!-- Wizard with validation -->
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title">Add Store</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <!-- <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li> -->
                                </ul>
                            </div>
                        </div>
<div id="show"></div>
                        <form class="steps-validation form_validation storeform" method="post" onsubmit="myfrm(this)" action="<?php echo site_url('adminStoreAdd');?>" enctype="multipart/form-data" >


                            <h6>Personal Info</h6>


                                <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">First Name: <span class="text-danger">*</span></label>
                                            
                                            <input type ="text" name="store_name" id="store_name" class="form-control " pattern=""  />
                                        </div>
                                    </div>

                                   <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Last Name: <span class="text-danger">*</span></label>
                                            
                                            <input type ="text" name="store_name" class="form-control " pattern=""  />
                                        </div>
                                    </div>
                                </div>

                               
                               
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Mobile:</label><br><br>
                                            <input type="text" name="mobile" class="form-control  " placeholder="+99-99999-99999"  data-mask="+99-99999-99999">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Email address: <span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control" placeholder="your@email.com" >
                                        </div>                              
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">User Name:<span class="text-danger">*</span></label>
                                            <input type="text" name="address" class="form-control" >
                                        </div>
                                    </div>
                                    

                                </div>
                            </fieldset>



                            <h6>New Store</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Store Name: <span class="text-danger">*</span></label>
                                            
                                            <input type ="text" name="store_name" class="form-control " pattern=""  />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Store Description: <span class="text-danger">*</span></label>
                                            <input type="text" name="store_description" class="form-control " >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Select Category: <span class="text-danger">*</span></label>
                                            <select name="category" data-placeholder="Select Category" class="select  " >
                                                <option></option>
                                                    <option value="1">PROFESSIONAL</option>
                                                    <option value="2">TRAVEL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Select City: <span class="text-danger">*</span></label>
                                            <select name="city" class="select   " data-placeholder="Select City" >
                                                <option></option>
                                                    <option value="1">Cebu</option>
                                                    <option value="2">Manila</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <!--    -->
                                        <div class="form-group">
                                            <label class="text-semibold">Select Sub-Category: <span class="text-danger">*</span></label>
                                            <select name="sub_category" data-placeholder="Select Sub-Category" class="select  " >
                                                <option></option>
                                                    <option value="1">Local</option>
                                                    <option value="2">Local&Outstaion</option>
                                                    <option value="3">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                        <label class="text-semibold">Select Area:<span class="text-danger">*</span></label>
                                        <select  name="area" class="select  " data-placeholder="Select Area" >
                                            <option></option>
                                            <option value="1">Apans</option>
                                            <option value="2">Banilad</option>
                                            <option value="3">Mabolo</option>
                                            <option value="4">Bali</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Email address: <span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control  " placeholder="your@email.com" >
                                        </div>                              
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Office Landline #:</label>
                                            <input type="text" name="office_landline" class="form-control  " placeholder="99-99-9999" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Enter Address:<span class="text-danger">*</span></label>
                                            <textarea name="address" class="form-control  " ></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Mobile:</label><br><br>
                                            <input type="text" name="mobile" class="form-control  " placeholder="+99-999-999-9999"  data-mask="+99-999-999-9999">
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                             <h6>Additional info</h6>
                            <fieldset>
                                <div class="panel ">
                                    <div class="panel-heading bg-info-700"><label class="text-semibold">Select Keywords</label></div>
                                        <div class="panel-body">
                                            <div class="well well-lg">
                                                    <select multiple="multiple" data-placeholder="Enter Keywords" class="select">
                                                        <optgroup label="Mountain Time Zone">
                                                            <option value="AZ" >Arizona</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="WY">Wyoming</option>
                                                        </optgroup>
                                                        <optgroup label="Central Time Zone">
                                                            <option value="AL">Alabama</option>
                                                            <option value="IA" >Iowa</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                        </optgroup>
                                                        <optgroup label="Eastern Time Zone">
                                                            <option value="CT">Connecticut</option>
                                                            <option value="FL" >Florida</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="WV">West Virginia</option>
                                                        </optgroup>
                                                    </select>
                                                </div><br><br>
                                        </div>
                                    </div>
                                    
                            </fieldset> 
                            <!--- Add store Ends-->
                            <!--Store pics-->
                            <h6>Store Pictures</h6>
                            <fieldset>


                            <!-- Modal -->
                            <div id="img_preview" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Preview of Your Image</h4>
                                  </div>
                                  <div class="modal-body ">
                                    <center>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 ">
                                        <div class="cvrimgprediv">
                                        <img id="modal_img" alt="Cover image" class="cvrimg" >
                                        </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                    <br>
                                    <div class="container-fluid well well-lg">
                                    <h5 >Your page content will be here!!</h5>
                                    </div>
                                    </center>
                                  </div>
                                  <div class="modal-footer">
                                    Your image will look like this 
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>

                              </div>
                            </div>


                            <div class="panel ">
                            <div class="panel-heading bg-info-700"><label class="text-semibold ">Upload Cover Photo</label>
                            <a style="color:white;" href="#" data-toggle="tooltip" title="Upload COver Pic of your page"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                            </div>
                            <div class="panel-body">
                                <!--<div class="row">
                                    <div class="col-md-8">
                                        <span class=" text-center">Upload Cover Picture</span><br>
                                        <input type="file" name="cover_picture" class="file-styled">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="" class="">
                                    </div>

                                </div>-->

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4"><label class="control-label text-semibold">Select Images:</label>

                                            </div>
                                            <div class=" col-md-6"> 
                                                        <div class=" ">     
                                                        <div  id="pre-card" class=" cont well well-sm hide-file-img">
                                                            <img id="file_img" class="thmbpreimg" src="#" alt="cover image"/><br><br>
                                                            <div class="top-border" style="border-bottom:1px solid lightgrey;"></div>
                                                            
                                                            <a href="#" class="btn-icon " data-toggle="modal" data-target="#img_preview" onclick=""><i class="glyphicon  glyphicon-eye-open"></i></a>
                                                            
                                                            <a href="#" class="btn-icon" onclick="remimg()"><i class="glyphicon  glyphicon-trash"></i></a>
                                                            
                                                            <br>
                                                        
                                                        </div>
                                                        </div>
                                                        
                                            </div>
                                                    <div class="col-md-2"></div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                        <div class="col-lg-2"></div>
                                            <div class="col-lg-8" > 
                                            
                                                <input type="text" value="No File Selected" readonly id="filetb" class="form-control col-md-10 file-tb" required>
                                                    
                                                </div>
                                                <div class="col-sm-2">
                                                <button type="button" class=" btn  btn-primary" 
                                            onclick="document.getElementById('fileInput').click();"> Choose Files!
                                            </button>
                                            <input id="fileInput" class="hide-file-img " onchange="readURL(this);" type="file" /></div>
                                            </div>
                                        </div>              
                                        </div>
                                        </div>
                                    </div>
                                <br><br>
                            


                            <div id="multiple-img" class="panel ">
                                <div class="panel-heading bg-info-700">
                                    <div class="panel-title"><span class="text-semibold">Upload Photo Images</span><a style="color:white;" href="#" data-toggle="tooltip" title="Upload more Pictures"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                                    </div>
                                    <div class="heading-elements"></div>
                                </div>
                            
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label text-semibold">Select Images:</label>
                                        <div class="col-lg-10">

                                            <div class=" well well-sm">
                                                <p>Select Number of images to upload</p>
                                                <div class="">
                                                <label><input class="" id="singleup" type="radio" onclick="show(this)" name="selector">Single image</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input class="" type="radio" onclick="show(this)" id="mulup" name="selector">Multiple image</label>
                                                    
                                                </div>
                                            </div>
                                            <br>
                                            <div class="upload-single ">
                                                
                                            <input type="file" id="" name="store_picture"  class="file-input" fileTypeSettings="image"  >
                                            </div>
                                            <br>
                                            <div class="upload-multiple ">
                                                
                                                <input type="file" class="file-input-ajax" multiple="multiple">
                                            </div>
                                            <!--<input type="file" id="" name="store_picture"  class="file-input-ajax" fileTypeSettings="image" multiple="multiple">-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                            </fieldset>

                            <h6>Store Timing</h6>
                           <fieldset>
                                <div class="panel ">

                                    <div class="panel-heading bg-info-700 text-semibold">Enter Your Store Timings</div>

                                    <div class="panel-body" id="days">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label  class="text-semibold text-center"><h6>Monday:</h6></label>
                                            </div>

                                            <div  id="dot" class="col-md-3">
                                                <select class=" load-days" id="otime" name="mon-open"  onchange="validtime(this)" >
                                                </select>

                                            </div>
                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div  id="dct" class="col-md-3">

                                                <select class=" load-days" id="ctime"  name="mon-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed"   onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row ">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Tuesday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="tue-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="tue-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Wednesday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="wed-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="wed-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Thrusday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="thu-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class="load-days" id="ctime" name="thu-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Friday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="fri-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="fri-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Saturday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="sat-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="sat-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Sunday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="sun-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="sun-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="invalid-time text-danger-400 pull-right">
                                            <strong>*Invalid Time!</strong> Opening Time must be greater than Closing time.                     
                                        </div>

                                    </div><!--panel-->

                                
                            </fieldset>


 <h6>Payment </h6>
                            <fieldset>
                                <div class="panel ">

                                    <div class="panel-heading bg-info-700 text-semibold">Payment List</div>

                                    <div class="panel-body">
                                        <div>

<div class="col-md-12">
                    <div class="col-sm-6 col-md-3 price padding-0">
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Free</b>
                        </div>
                         <div class="badges-ribbon">
                          <div class="badges-ribbon-content badge-success">Free</div>
                        </div>
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>99<sub>/month</sub>
                              </span>
                        </div>
                         <ul class="list-group text-center">
                            <li class="list-group-item">
                              Listing Free
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                              Full Access
                              <i class="fa fa-check text-success"></i>
                            </li>
                              <li class="list-group-item">
                              Documentation
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                               Free Update
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item" style="padding:20px;">
                            <center>
                            
                            
                              <input type="button" id="EntryPlan" class="btn box-shadow-none text-white bg-indigo" value="Subscribe"/>
                             
                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3 price ">
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Platinum</b>
                        </div>
                         
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>999<sub>/month</sub>
                              </span>
                        </div>
                         <ul class="list-group text-center">
                            <li class="list-group-item">
                             Banner Listing Free 
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item">
                              Listing Free
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                              Full Access
                              <i class="fa fa-check text-success"></i>
                            </li>
                              <li class="list-group-item">
                              Documentation
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                               Free Update
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item" style="padding:20px;">
                            <center>
                            
                             <input type="button" id="PlatinumPlan" class="btn box-shadow-none text-white bg-indigo" value="Subscribe"/>
                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>

                    <div class="col-sm-6 col-md-3 price "  style="display: none;">
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Silver</b>
                        </div>
                         
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>399<sub>/month</sub>
                              </span>
                        </div>
                         <ul class="list-group text-center">
                            <li class="list-group-item">
                             Banner Listing Free 
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item">
                              Listing Free
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                              Full Access
                              <i class="fa fa-check text-success"></i>
                            </li>
                              <!-- <li class="list-group-item">
                              Documentation
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                               Free Update
                              <i class="fa fa-check text-success"></i>
                            </li> -->
                            <li class="list-group-item" style="padding:20px;">
                            <center>
                            
                             <input type="button" id="silverPlan" class="btn box-shadow-none text-white bg-indigo" value="Subscribe"/>
                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>


                    <div class="col-sm-6 col-md-3 price " style="display: none;">
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Gold</b>
                        </div>
                         
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>799<sub>/month</sub>
                              </span>
                        </div>
                         <ul class="list-group text-center">
                            <li class="list-group-item">
                             Banner Listing Free 
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item">
                              Listing Free
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                              Full Access
                              <i class="fa fa-check text-success"></i>
                            </li>
                              <li class="list-group-item">
                              Documentation
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <!-- <li class="list-group-item">
                               Free Update
                              <i class="fa fa-check text-success"></i>
                            </li> -->
                            <li class="list-group-item" style="padding:20px;">
                            <center>
                            
                             <input type="button" id="goldPlan" class="btn box-shadow-none text-white bg-indigo" value="Subscribe"/>
                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>
                   
                    </div>
                                            </div>
                                            
                                            

                                        </div>
                                    </div><!--panel-->

                                
                            </fieldset>






 <h6>Calender </h6>
                            <fieldset>
                                <div class="panel ">
                         <!--         <?php
                   
                   $total_banner=$count[0]->total_banner;
                   echo "string";
                   echo $total_banner;  
                 ?> -->
                            <div class="col-md-12">

                           <div class="content-group-lg">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                                            <input type="text" class="form-control pickadate-selectors" placeholder="Next Date of Follow up" required>
                                        </div>
                                    </div>


                    <!--  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text dateAnimate" name="date" required>
                        <span class="bar"></span>
                        <label>Next Date of Follow up</label>
                      </div> -->
                         <div class="row">
                        <!--  <?php 
                        
                         
                         foreach($cal as $row)
                         {
                                $hight=140/$total_banner;
                                if($row['count']==0)
                                {
                                    $hight='0px';
                                }
                                else
                                {
                                    $hight=($hight*$row['count']).'px';
                                }
                                
                                
                         ?> -->
              <div class="col-sm-6 col-md-3">
              <div class="btnset">
                <div class="panel bg-primary box-shadow-none" style="height:50px">
                  <div class="panel-body ap">
                    <center><h4 class="text-white" style="color:#000 !important;"><?php echo $row['name']; ?></h4></center>
                  </div>
                </div>
                </div>
               <!--  <?php 
                if($total_banner != $row['count'])
                                {?> -->

                <input type="button" class="btn btn-success addbanner" data="<?php echo $row['monthID']; ?>" datayear='<?php echo $row['year']; ?>' value="Add" />
                                <?php }?>
                <span class="label label-primary">Available :</span><span class="label label-danger">
                 <!-- <?php echo $total_banner-$row['count'];?> -->
                    
5
                </span>
              </div>
                         <!-- <?php }?> -->
             
                   </div>
                  </div>
                  </div>
                  </fieldset>
                           

 <h6>Banner Image </h6>
                            <fieldset>
                              <div id="multiple-img" class="panel " style="display: none;">
                                <div class="panel-heading bg-info-700">
                                    <div class="panel-title"><span class="text-semibold">Upload Photo Images</span><a style="color:white;" href="#" data-toggle="tooltip" title="Upload more Pictures"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                                    </div>
                                    <div class="heading-elements"></div>
                                </div>
                            
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label text-semibold">Select Images:</label>
                                        <div class="col-lg-10">

                                          
                                            <div class="">
                                                
                                            <input type="file" id="" name="store_picture"  class="file-input" fileTypeSettings="image"  >
                                            </div>
                                           
                                            <!--<input type="file" id="" name="store_picture"  class="file-input-ajax" fileTypeSettings="image" multiple="multiple">-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div> 
                            </fieldset>

                        </form>
                    </div>
                  

                </div>
                <!-- /content area -->
             <!--    <link href="<?php echo base_url();?>js/style.css" rel="stylesheet" type="text/css" media="all"/> -->
<script type="text/javascript">
    

    $(document).ready(function() {
                        $('.popup-with-zoom-anim').magnificPopup({
                            type: 'inline',
                            fixedContentPos: false,
                            fixedBgPos: true,
                            overflowY: 'auto',
                            closeBtnInside: true,
                            preloader: false,
                            midClick: true,
                            removalDelay: 300,
                            mainClass: 'my-mfp-zoom-in'
                        });





// $(".storeform").submit(function(){
// alert("hii");
// });
                                                                                        
                        });

</script>
<!--  -->
