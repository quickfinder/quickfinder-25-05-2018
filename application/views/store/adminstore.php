

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    <!-- <div class="page-header-content">
                        <div class="page-title">
                            <h1><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Store</span> 

</h1>
                        </div>

                       
                    </div> -->

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Admin Store</a></li>
                            <li class="active">List Store

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">


                    <!-- Highlighting rows and columns -->
                    <div class="panel panel-flat">

                    <div class="panel-heading">

 <div class="heading-elements">
     <!-- <a href="<?php echo site_url('adduser');  ?>"> -->

                                <ul class="icons-list">
                               <h5 class="panel-title"><?php if($this->session->userdata('usertype')==3 ){?>
                        <a href="<?php echo site_url('adminaddnewstores');  ?>">
                            <button class="btn ripple btn-gradient btn-primary" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a>
                    <?php }else{ ?>
                    
                    <a href="<?php echo site_url('Users');  ?>">
                            <button class=" btn btn-gradient btn-primary" >
                                <span>Back</span>
                            </button>
                        </a>
                    <?php   
                    }
                    
                    ?>      </h5>
                                    
                                </ul>
                            </div>

</div>
                        

                        <table class="table table-bordered table-hover datatable-highlight">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Payment Type</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php
                                $i = 0;
                                foreach ($storelist as $row) {
                                    $i++;
                                    // print_r($row);
                                    //print_r($row['data']['storedata']);
                                    //echo $row['data']['storedata']['id'];
                                    ?>
                                     <tr>
                                        <!--<td><?php echo $i; ?></td>-->
                                        <td>
                                            <img src="<?php echo base_url().$row['firstimage']; ?>" style="width: 202px;height: 140px">
                                        </td>
                                        <td><?php echo $row['storename']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td><?php echo $row['mobile']; ?></td>
                                        <td>
                                        <span class="label label-primary"><?php echo $row['payment_type'];?></span></td>
                                        <td>
                                        <?php if($row['IsActive']==1){?>
                                        <input type="button" data-toggle="tooltip" data-placement="top" title="Active Store" class="btn btn-success" value="Active"/>
                                        <?php }else{?>
                                        <a href="<?php echo site_url('pricingp/'.$row['id'])?>"><input type="button"  data-toggle="tooltip" data-placement="top" title="Unactive Store"class="btn btn-gradient btn-danger" value="Inactive"></a>
                                        <?php }?>
                                        <!-- <a href="<?php echo site_url('addstore/'.$row['userid'])?>"><input type="button" data-toggle="tooltip" data-placement="top" title="Add New Store" class="btn  btn-gradient btn-success" value="AddStore"></a> -->
                                        </td>


                        <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                                    <li><a href="<?php echo site_url('addstore/'.$row['userid'])?>" data-placement="top" title="Add New Store" value="AddStore"><i class="icon-pencil3"></i>Add Store</a></li>
                                                     <li><a href="<?php echo site_url('editstore/'.$row['id'])?>" ><i class="icon-pencil4"></i> Edit </a></li>
                                                   
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                    </tr>
                                <?php }
                                ?>


                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /highlighting rows and columns -->

                    </div>
                   