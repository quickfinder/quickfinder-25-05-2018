<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">VendorStore</span> </h4>
						</div>

						
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> VendorStore</a></li>
							<li class="active">List Store</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->



<!-- Content area -->
				<div class="content">

				

					<!-- DOM positioning -->
					<div class="panel panel-flat">
						<div class="panel-heading">
					    <a href="<?php echo site_url('addstore/'.$this->uri->segment(2));  ?>">
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a>
						<a href="<?php echo site_url('backtovendorlist');  ?>">
                            <button class="btn ripple btn-gradient btn-info " style="width:80px">
                                <span>Back</span>
                            </button>
                        </a>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<!-- <li><a data-action="collapse"></a></li> -->
			                		<li><a data-action="reload"></a></li>
			                		<!-- <li><a data-action="close"></a></li> -->
			                	</ul>
		                	</div>
						</div>

						
						<table class="table table-bordered table-hover datatable-highlight">
							<thead>
								<tr>
 									<th>Image</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
									<th>Payment Type</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
							
								 <?php
                                $i = 0;
                                foreach ($storelist as $row) {
                                    $i++;
                                    // print_r($row);
                                    //print_r($row['data']['storedata']);
                                    //echo $row['data']['storedata']['id'];
                                    ?>
                                    <tr>
                                        <!-- <td><?php echo $i; ?></td> -->
                                        <td>
                                            <img src="<?php echo base_url().$row['firstimage']; ?>" style="width: 202px;height: 140px">
                                        </td>
                                        <td><?php echo $row['storename']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td><?php echo $row['mobile']; ?></td>
                                        <td>
										<span class="label label-primary"><?php echo $row['payment_type'];?></span></td>
										<td>
										<?php if($row['IsActive']==1){?>
										<input type="button" data-toggle="tooltip" data-placement="top" title="Active Store" class="btn btn-success" value="Active"/>
				
										<?php }else{?>
										<a href="<?php echo site_url('pricingp/'.$row['id'])?>"><input type="button"  data-toggle="tooltip" data-placement="top" title="Unactive Store" class="btn btn-danger" value="Inactive"></a>
										<?php }?>
										
										</td>
                                    </tr>
                                <?php }
                                ?>
							</tbody>
						</table>
					</div>
					<!-- /DOM positioned -->




			