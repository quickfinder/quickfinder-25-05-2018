    <style>
        .ap{
            padding:50px;
            border:1px solid #000;
        }
        .addbtn{
            padding: 5px 18px !important;
            margin: 0px !important;
        }
        .btnset
        {
            height: 160px !important;
        }
        .bar
        {
            color:red;
        }

        .panel-heading {
    /*line-height: 1.42857143 !important;*/
     color: #FFF !important; 
}
        </style>
         <link href='//fonts.googleapis.com/css?family=Monda:400,700' rel='stylesheet' type='text/css'>

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Stores</a></li>
                            <li class="active">Add Store</li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">
                    <!-- Wizard with validation -->
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title">Add Store</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <!-- <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li> -->
                                </ul>
                            </div>
                        </div>
<div id="show"></div>
                        <form class="steps-validation form_validation " id="storeform" method="post" action="<?php echo site_url('adminStoreAdd');?>" enctype="multipart/form-data" >

                            <h6>Personal Info</h6>


                                <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">First Name: <span class="text-danger">*</span></label>
                                            
                                            <input type ="text" name="first_name" id="first_name" class="form-control " pattern=""  />
                                        </div>
                                    </div>

                                   <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Last Name: <span class="text-danger">*</span></label>
                                            
                                            <input type ="text" name="last_name" id="last_name" class="form-control " pattern=""  />
                                        </div>
                                    </div>
                                </div>

                               
                               
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Mobile:</label><br><br>
                                            <input type="text" name="p_mobile" id="p_mobile" class="form-control  " placeholder="+99-99999-99999"  data-mask="+99-99999-99999">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Email address: <span class="text-danger">*</span></label>
                                            <input type="email" name="p_email" id="p_email" class="form-control" placeholder="your@email.com" >
                                        </div>                              
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">User Name:<span class="text-danger">*</span></label>
                                            <input type="text" name="user_name" id="user_name" class="form-control" >
                                        </div>
                                    </div>
                                    

                                </div>
                            </fieldset>


                            <h6>New Store</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Store Name: <span class="text-danger">*</span></label>
                                            
                                            <input type ="text" name="store_name" class="form-control " pattern=""  />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Store Description: <span class="text-danger">*</span></label>
                                            <input type="text" name="store_desc" id="store_desc" class="form-control " >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Select Category: <span class="text-danger">*</span></label>
                                            <select id="categoryid" name="categoryid" data-placeholder="Select Category" class="select" >
                                                <option value="0">Select category</option>
                                        <?php
                                      
                                        foreach ($categorylist as $row) {
                                            ?>
                                            <option id="<?php echo $row['category']['id']; ?>" value="<?php echo $row['category']['id']; ?>"><?php echo ucwords($row['category']['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Select City: <span class="text-danger">*</span></label>
                                            <select name="city" class="select   " data-placeholder="Select City" >
                                                <option></option>
                                                    <option value="cebu">Cebu</option>
                                                    <option value="manila">Manila</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <!--    -->
                                        <div class="form-group">
                                            <label class="text-semibold">Select Sub-Category: <span class="text-danger">*</span></label>
                                            <select name="subcategoryid" id="subcategoryid" data-placeholder="Select Sub-Category" class="select  " >
                                                <option value="0" >Select Subcategory</option>
                                            </select>
											
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                        <label class="text-semibold">Select Area:<span class="text-danger">*</span></label>
                                        <select  name="area" class="select  " data-placeholder="Select Area" >
                                            <option></option>
                                            <option value="Apans">Apans</option>
                                            <option value="Banilad">Banilad</option>
                                            <option value="Mabolo">Mabolo</option>
                                            <option value="Bali">Bali</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Email address: <span class="text-danger">*</span></label>
                                            <input type="email" name="store_email" class="form-control  " placeholder="your@email.com" >
                                        </div>                              
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Office Landline #:</label>
                                            <input type="text" name="office_landline" class="form-control  " placeholder="99-99-9999" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Enter Address:<span class="text-danger">*</span></label>
                                            <textarea name="store_address" class="form-control  " ></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Mobile:</label><br><br>
                                            <input type="text" name="store_mobile" class="form-control  " placeholder="+99-999-999-9999"  data-mask="+99-999-999-9999">
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                             <h6>Additional info</h6>
                            <fieldset>
                                <div class="panel ">
                                    <div class="panel-heading bg-info-700"><label class="text-semibold">Select Keywords</label></div>
                                        <div class="panel-body">
                                            <div class="well well-lg">
                                                    <select multiple="multiple" data-placeholder="Enter Keywords" name="keywords[]" class="select">
                                                         <optgroup label="Restaurant">
                                                            <option value="Fast food" >Fast food</option>
                                                            <option value="Fine Art">Fine Art</option>
                                                            <option value="American Food Catering">American Food Catering</option>
                                                            <option value="American Restaurant">American Restaurant</option>
                                                            </optgroup>
                                                        <optgroup label="Hotel">
                                                            <option value="hotels near me">hotels near me</option>
                                                            <option value="cheapest hotel" >cheapest hotel</option>
                                                            <option value="choice hotel">choice hotel</option>
                                                            <option value="hotel trivago">hotel trivago</option>
                                                        </optgroup>
                                                        <optgroup label="Flights">
                                                            <option value="travelocity">travelocity</option>
                                                            <option value="airline tickets" >airline tickets</option>
                                                            <option value="trip">trip</option>
                                                            <option value="travel agency">travel agency </option>
                                                        </optgroup>
                                                    </select>
                                                </div><br><br>
                                        </div>
                                    </div>
                                    
                            </fieldset> 
                            <!--- Add store Ends-->
                            <!--Store pics-->
                            <h6>Store Pictures</h6>
                            <fieldset>


                            <!-- Modal -->
                            <div id="img_preview" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Preview of Your Image</h4>
                                  </div>
                                  <div class="modal-body ">
                                    <center>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 ">
                                        <div class="cvrimgprediv">
                                        <img id="modal_img" alt="Cover image" class="cvrimg" >
                                        </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                    <br>
                                    <div class="container-fluid well well-lg">
                                    <h5 >Your page content will be here!!</h5>
                                    </div>
                                    </center>
                                  </div>
                                  <div class="modal-footer">
                                    Your image will look like this 
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>

                              </div>
                            </div>


                            <div class="panel ">
                            <div class="panel-heading bg-info-700"><label class="text-semibold ">Upload Cover Photo</label>
                            <a style="color:white;" href="#" data-toggle="tooltip" title="Upload COver Pic of your page"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                            </div>
                            <div class="panel-body">
                                <!--<div class="row">
                                    <div class="col-md-8">
                                        <span class=" text-center">Upload Cover Picture</span><br>
                                        <input type="file" name="cover_picture" class="file-styled">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="" class="">
                                    </div>

                                </div>-->

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4"><label class="control-label text-semibold">Select Images:</label>

                                            </div>
                                            <div class=" col-md-6"> 
                                                        <div class=" ">     
                                                        <div  id="pre-card" class=" cont well well-sm hide-file-img">
                                                            <img id="file_img" class="thmbpreimg" src="#" alt="cover image"/><br><br>
                                                            <div class="top-border" style="border-bottom:1px solid lightgrey;"></div>
                                                            
                                                            <a href="#" class="btn-icon " data-toggle="modal" data-target="#img_preview" onclick=""><i class="glyphicon  glyphicon-eye-open"></i></a>
                                                            
                                                            <a href="#" class="btn-icon" onclick="remimg()"><i class="glyphicon  glyphicon-trash"></i></a>
                                                            
                                                            <br>
                                                        
                                                        </div>
                                                        </div>
                                                        
                                            </div>
                                                    <div class="col-md-2"></div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                        <div class="col-lg-2"></div>
                                            <div class="col-lg-8" > 
                                            
                                                <input type="text" value="No File Selected" readonly id="filetb" class="form-control col-md-10 file-tb" required>
                                                    
                                                </div>
                                                <div class="col-sm-2">
                                                <button type="button" class=" btn  btn-primary" 
                                            onclick="document.getElementById('fileInput').click();"> Choose Files!
                                            </button>
                                            <input id="fileInput" name="cover_img" class="hide-file-img " onchange="readURL(this);" type="file" /></div>
                                            </div>
                                        </div>              
                                        </div>
                                        </div>
                                    </div>
                                <br><br>
                            


                            <div id="multiple-img" class="panel ">
                                <div class="panel-heading bg-info-700">
                                    <div class="panel-title"><span class="text-semibold">Upload Photo Images</span><a style="color:white;" href="#" data-toggle="tooltip" title="Upload more Pictures"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                                    </div>
                                    <div class="heading-elements"></div>
                                </div>
                            
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label text-semibold">Select Images:</label>
                                        <div class="col-lg-10">

                                            <div class=" well well-sm">
                                                <p>Select Number of images to upload</p>
                                                <div class="">
                                                <label><input class="" id="singleup" type="radio" value="single" onclick="show(this)" name="selector">Single image</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input class="" type="radio" onclick="show(this)" id="mulup" value="multi" name="selector">Multiple image</label>
                                                    
                                                </div>
                                            </div>
                                            <br>
                                            <div class="upload-single ">
                                                
                                            <input type="file"  id="" name="store_spicture"  class="file-input" fileTypeSettings="image"  >
                                            </div>
                                            <br>
                                            <div class="upload-multiple ">
                                                
                                                <input type="file" name="store_mpicture" class="file-input-ajax" multiple="multiple">
                                            </div>
                                            <!--<input type="file" id="" name="store_picture"  class="file-input-ajax" fileTypeSettings="image" multiple="multiple">-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                            </fieldset>

                            <h6>Store Timing</h6>
                           <fieldset>
                                <div class="panel ">

                                    <div class="panel-heading bg-info-700 text-semibold">Enter Your Store Timings</div>

                                    <div class="panel-body" id="days">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label  class="text-semibold text-center"><h6>Monday:</h6></label>
                                            </div>

                                            <div  id="dot" class="col-md-3">
                                                <select class=" load-days" id="otime" name="mon-open"  onchange="validtime(this)" >
                                                </select>

                                            </div>
                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div  id="dct" class="col-md-3">

                                                <select class=" load-days" id="ctime"  name="mon-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed"   onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row ">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Tuesday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="tue-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="tue-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Wednesday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="wed-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="wed-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Thrusday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="thu-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class="load-days" id="ctime" name="thu-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Friday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="fri-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="fri-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Saturday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="sat-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="sat-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Sunday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="sun-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="sun-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="invalid-time text-danger-400 pull-right">
                                            <strong>*Invalid Time!</strong> Opening Time must be greater than Closing time.                     
                                        </div>

                                    </div><!--panel-->

                                
                            </fieldset>


						<h6>Payment </h6>
                            <fieldset>
                                <div class="panel">

                                    <div class="panel-heading bg-info-700 text-semibold">Payment List</div>
                                    <div class="panel-body">
                                        <div>
					<div class="col-md-12">
                    <div class="col-sm-6 col-md-3 price padding-0">
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Basic Package</b>
                        </div>
                         <!-- <div class="badges-ribbon">
                          <div class="badges-ribbon-content badge-success">Free</div>
                        </div> -->
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>99
                              </span>
                        </div>
                         <ul class="list-group text-center">
                            <li class="list-group-item">
                             1 Month Validity
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                             Free Analytics
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item"></li>
                              <!-- <li class="list-group-item">
                              Documentation
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                               Free Update
                              <i class="fa fa-check text-success"></i>
                            </li> -->
                            <li class="list-group-item" style="padding:20px;">
                            <center>

                                                        <h5><input type="radio"  value="1" name="price" value="Subscribe"/>&nbsp;&nbsp;<span class="text-indigo "> Subscribe</span></h5>
<!-- 
                            <button  class="btn box-shadow-none text-white bg-indigo" >
                            <input type="radio"  value="1" name="price" value="Subscribe"/>&nbsp;&nbsp;Subscribe</button> -->
                              <!-- <input type="radio" value="1" name="price" />Subscribe -->
                             
                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3 price ">
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Sliver Package (Promo)</b>
                        </div>
                         
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>495
                              </span>
                        </div>
                         <ul class="list-group text-center">
                            <li class="list-group-item">
                             6 Month Validity
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                             1 Month Free Subscription
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item">
                             Free Analytics
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item" style="padding:20px;">
                            <center>
                            
  <h5><input type="radio"  value="2" name="price" value="Subscribe"/>&nbsp;&nbsp;<span class="text-indigo "> Subscribe</span></h5>                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>

                    <div class="col-sm-6 col-md-3 price "  >
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Gold Package(Promo)</b>
                        </div>
                         
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>990
                              </span>
                        </div>
                         <ul class="list-group text-center">
                           <li class="list-group-item">
                             12 Month Validity
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                             2 Month Free Subscription
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item">
                             Free Analytics
                              <i class="fa fa-check text-success"></i>
                            </li>
                              <!-- <li class="list-group-item">
                              Documentation
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                               Free Update
                              <i class="fa fa-check text-success"></i>
                            </li> -->
                            <li class="list-group-item" style="padding:20px;">
                            <center>
                            
                               <h5><input type="radio"  value="3" name="price" value="Subscribe"/>&nbsp;&nbsp;<span class="text-indigo "> Subscribe</span></h5>
                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>


                    <div class="col-sm-6 col-md-3 price " >
                      <div class="panel">
                      <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>VIP Package(promo) </b>
                        </div>
                         
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>1386
                              </span>
                        </div>
                         <ul class="list-group text-center">
                          <li class="list-group-item">
                             18 Month Validity
                              <i class="fa fa-check text-success"></i>
                            </li>
                             <li class="list-group-item">
                             4 Month Free Subscription
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item">
                             Free Analytics
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item" style="padding:20px;">
                            <center>
                            
                              <h5><input type="radio"  value="4" name="price" value="Subscribe"/>&nbsp;&nbsp;<span class="text-indigo "> Subscribe</span></h5>
                              </center>
                            </li>
                          </ul>
                      </div>
                    </div>
                   
                    </div>
                                            </div>
                                            
                                            

                                        </div>
                                    </div><!--panel-->

                                
                            </fieldset>






					<h6>Calender </h6>
                            <fieldset>
                                <div class="panel ">
                                <?php
                   
                   $total_banner=$count[0]->total_banner;
                  
                 ?> 
                            <div class="col-md-12">

                           <div class="content-group-lg">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                                            <input type="text" class="form-control pickadate-selectors" placeholder="Next Date of Follow up" >
                                        </div>
                                    </div>


                   
                         <div class="row">
                          <?php 
                        
                         
                         foreach($cal as $row)
                         {
                                $hight=140/$total_banner;
                                if($row['count']==0)
                                {
                                    $hight='0px';
                                }
                                else
                                {
                                    $hight=($hight*$row['count'])+10 .'px';
                                }
                                
                                
                         ?>
              <div class="col-sm-6 col-md-3">
              <div class="btnset">
                <div class="panel bg-primary box-shadow-none" style="height:<?php echo $hight; ?>">
                  <div class="panel-body ap">
                    <center><h4 class="text-white" style="color:#000 !important;"><?php echo $row['name']; ?></h4></center>
                  </div>
                </div>
                </div>
               <!--  <?php 
                if($total_banner != $row['count'])
                                {?> -->
				<input type="radio" name="cal" value="<?php echo $row['monthID'].'-'.$row['year'];; ?>"	
                <input type="button" class="btn btn-success addbanner" data="<?php echo $row['monthID']; ?>" datayear='<?php echo $row['year']; ?>' value="Add" />
                               <?php }?>
                <span class="label label-primary">Available : <?php echo $total_banner-$row['count'];?></span>
				<span class="label label-danger">
                 <!-- <?php ?> -->
                    

                </span>
              </div>
                          <?php }?>
             
                   </div>
                  </div>
                  </div>
                  </fieldset>
                           

					<h6>Banner Image </h6>
                            <fieldset>
                              <div id="multiple-img" class="panel">
                                <div class="panel-heading bg-info-700">
                                    <div class="panel-title"><span class="text-semibold">Upload Photo Images</span><a style="color:white;" href="#" data-toggle="tooltip" title="Upload more Pictures"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                                    </div>
                                    <div class="heading-elements"></div>
                                </div>
                            
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label text-semibold">Select Images:</label>
                                        <div class="col-lg-10">

                                          
                                            <div class="">
                                                
                                            <input type="file" id="" name="store_picture"  class="file-input" fileTypeSettings="image"  >
                                            </div>
                                           
                                            <!--<input type="file" id="" name="store_picture"  class="file-input-ajax" fileTypeSettings="image" multiple="multiple">-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div> 
                            </fieldset>

                        </form>
                    </div>
                  

                </div>
                <!-- /content area -->
             <!--    <link href="<?php echo base_url();?>js/style.css" rel="stylesheet" type="text/css" media="all"/> -->

<!--  -->