
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Stores</a></li>
                            <li class="active">Edit Admin Store

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content"> 
                                    <div class="panel panel-white">
  
                <div id="show"></div>
                        <form class="steps-validation form_validation " id="storeform" method="post" action="<?php echo site_url('');?>" enctype="multipart/form-data" >

                            
                            <h6>New Store</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Store Name: <span class="text-danger">*</span></label>
                                            
                                            <input type ="text" name="store_name" class="form-control " pattern=""  value="<?php echo $storeData[0]['name'];?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Store Description: <span class="text-danger">*</span></label>
                                            <input type="text" name="store_desc" id="store_desc" class="form-control " value="<?php echo $storeData[0]['description'];?>"  >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Select Category: <span class="text-danger">*</span></label>
                                            <select id="categoryid" name="categoryid" data-placeholder="Select Category" class="select">
                                                <!-- <option value="0">Select category</option> -->
                                        <?php
                                      
                                        foreach ($categorylist as $row) {
                                            ?>
                                            <option id="<?php echo $storeData[0]['categoryid'];?>" value="<?php echo $storeData[0]['categoryid'];?>"><?php echo ucwords($row['category']['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Select City: <span class="text-danger">*</span></label>
                                            <select name="city" class="select   " data-placeholder="Select City" >
                                            <option value="<?php echo $storeData[0]['city'];?>"><?php echo $storeData[0]['city'];?></option>
                                                    <option value="cebu">Cebu</option>
                                                    <option value="manila">Manila</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <!--    -->
                                        <div class="form-group">
                                            <label class="text-semibold">Select Sub-Category: <span class="text-danger">*</span></label>
                                            <select name="subcategoryid" id="subcategoryid" data-placeholder="Select Sub-Category" class="select  " >
                                                <option value="0" >Select Subcategory</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                        <label class="text-semibold">Select Area:<span class="text-danger">*</span></label>
                                        <select  name="area" class="select  " data-placeholder="Select Area" >
<option value="<?php echo $storeData[0]['area'];?>"><?php echo $storeData[0]['area'];?></option>                                            <option value="Apans">Apans</option>
                                            <option value="Banilad">Banilad</option>
                                            <option value="Mabolo">Mabolo</option>
                                            <option value="Bali">Bali</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Email address: <span class="text-danger">*</span></label>
                                            <input type="email" name="store_email" class="form-control  " placeholder="your@email.com" value="<?php echo $storeData[0]['email'];?>">
                                        </div>                              
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Office Landline #:</label>
                                            <input type="text" name="office_landline" class="form-control  " placeholder="99-99-9999"  value="<?php echo $storeData[0]['landline'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Enter Address:<span class="text-danger">*</span></label>
                                            <textarea name="store_address" class="form-control"  value="<?php echo $storeData[0]['address'];?>" ></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-semibold">Mobile:</label><br><br>
                                            <input type="text" name="store_mobile" class="form-control  " placeholder="+99-999-999-9999"  data-mask="+99-999-999-9999"  value="<?php echo $storeData[0]['mobile'];?>">
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                             <h6>Additional info</h6>
                            <fieldset>
                                <div class="panel ">
                                    <div class="panel-heading bg-info-700"><label class="text-semibold">Select Keywords</label></div>
                                        <div class="panel-body">
                                            <div class="well well-lg">
                                                    <select multiple="multiple" data-placeholder="<?php echo $storeData[0]['tags'];?>" name="keywords[]" class="select" >
                                                    <option  value="<?php echo $storeData[0]['tags'];?>"><?php echo $storeData[0]['tags'];?></option>
                                                         <optgroup label="Restaurant">
                                                            <option value="Fast food" >Fast food</option>
                                                            <option value="Fine Art">Fine Art</option>
                                                            <option value="American Food Catering">American Food Catering</option>
                                                            <option value="American Restaurant">American Restaurant</option>
                                                            </optgroup>
                                                        <optgroup label="Hotel">
                                                            <option value="hotels near me">hotels near me</option>
                                                            <option value="cheapest hotel" >cheapest hotel</option>
                                                            <option value="choice hotel">choice hotel</option>
                                                            <option value="hotel trivago">hotel trivago</option>
                                                        </optgroup>
                                                        <optgroup label="Flights">
                                                            <option value="travelocity">travelocity</option>
                                                            <option value="airline tickets" >airline tickets</option>
                                                            <option value="trip">trip</option>
                                                            <option value="travel agency">travel agency </option>
                                                        </optgroup>
                                                    </select>
                                                </div><br><br>
                                        </div>
                                    </div>
                                    
                            </fieldset> 
                            <!--- Add store Ends-->
                            <!--Store pics-->
                            <h6>Store Pictures</h6>
                            <fieldset>


                            <!-- Modal -->
                            <div id="img_preview" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Preview of Your Image</h4>
                                  </div>
                                  <div class="modal-body ">
                                    <center>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 ">
                                        <div class="cvrimgprediv">
                                        <img id="modal_img" alt="Cover image" class="cvrimg" >
                                        </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                    <br>
                                    <div class="container-fluid well well-lg">
                                    <h5 >Your page content will be here!!</h5>
                                    </div>
                                    </center>
                                  </div>
                                  <div class="modal-footer">
                                    Your image will look like this 
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>

                              </div>
                            </div>


                            <div class="panel ">
                            <div class="panel-heading bg-info-700"><label class="text-semibold ">Upload Cover Photo</label>
                            <a style="color:white;" href="#" data-toggle="tooltip" title="Upload COver Pic of your page"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                            </div>
                            <div class="panel-body">
                                <!--<div class="row">
                                    <div class="col-md-8">
                                        <span class=" text-center">Upload Cover Picture</span><br>
                                        <input type="file" name="cover_picture" class="file-styled">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="" class="">
                                    </div>

                                </div>-->

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="row">
                                        <!-- <?php echo $storeData[0]['firstimage'];?> -->
                                       
                                            <div class="col-lg-4"><label class="control-label text-semibold">Select Images:</label>

                                            </div>
                                             <img src="<?php echo  base_url().$storeData[0]['firstimage'];?>" height=335 width=900 style="margin-left: 30px;">
                                            <div class=" col-md-6"> 

                                                        <div class=" ">     
                                                        <div  id="pre-card" class=" cont well well-sm hide-file-img">
                                                            <img id="file_img" class="thmbpreimg" src="#" alt="cover image"/><br><br>
                                                            <div class="top-border" style="border-bottom:1px solid lightgrey;"></div>
                                                            
                                                            <a href="#" class="btn-icon " data-toggle="modal" data-target="#img_preview" onclick=""><i class="glyphicon  glyphicon-eye-open"></i></a>
                                                            
                                                            <a href="#" class="btn-icon" onclick="remimg()"><i class="glyphicon  glyphicon-trash"></i></a>
                                                            
                                                            <br>
                                                        
                                                        </div>
                                                        </div>
                                                        
                                            </div>
                                                    <div class="col-md-2"></div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                        <div class="col-lg-2"></div>
                                            <div class="col-lg-8" > 
                                            
                                                <input type="text" value="No File Selected" readonly id="filetb" class="form-control col-md-10 file-tb" required>
                                                    
                                                </div>
                                                <div class="col-sm-2">
                                                <button type="button" class=" btn  btn-primary" 
                                            onclick="document.getElementById('fileInput').click();"> Choose Files!
                                            </button>
                                            <input id="fileInput" name="cover_img" class="hide-file-img " onchange="readURL(this);" value="<?php echo $storeData[0]['firstimage'];?>" type="file" /></div>
                                            </div>
                                        </div>              
                                        </div>
                                        </div>
                                    </div>
                                <br><br>
                           
                            <div id="multiple-img" class="panel ">
                                <div class="panel-heading bg-info-700">
                                    <div class="panel-title"><span class="text-semibold">Upload Photo Images</span><a style="color:white;" href="#" data-toggle="tooltip" title="Upload more Pictures"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                                    </div>
                                    <div class="heading-elements"></div>
                                </div>
                            
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label text-semibold">Select Images:</label>
                                        <div class="col-lg-10">

                                            <div class=" well well-sm">
                                                <p>Select Number of images to upload</p>
                                                <div class="">
                                                <label><input class="" id="singleup" type="radio" value="single" onclick="show(this)" name="selector">Single image</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input class="" type="radio" onclick="show(this)" id="mulup" value="multi" name="selector">Multiple image</label>
                                                    
                                                </div>
                                            </div>
                                            <br>
                                            <div class="upload-single ">
                                                
                                            <input type="file"  id="" name="store_spicture"  class="file-input" fileTypeSettings="image"  >
                                            </div>
                                            <br>
                                            <div class="upload-multiple ">
                                                
                                                <input type="file" name="store_mpicture" class="file-input-ajax" multiple="multiple">
                                            </div>
                                            <!--<input type="file" id="" name="store_picture"  class="file-input-ajax" fileTypeSettings="image" multiple="multiple">-->
                                        </div>
                                    </div>

                                     <img src="<?php echo  base_url().$storeData[0]['images'];?>" height=100 width=200 style="margin-left: 30px;">

                                </div>
                            </div>

                            </div>
                            </fieldset>

                            <h6>Store Timing</h6>
                           <fieldset>
                                <div class="panel ">

                                    <div class="panel-heading bg-info-700 text-semibold">Enter Your Store Timings</div>

                                    <div class="panel-body" id="days">
                                        <div class="row">
                                        <?php echo $alltime= $storeData[0]['store_timing'];
                                        // foreach ($alltime as $value) {

                                        //     echo $value['openTime'];
                                        //     # code...
                                        // }
                                                // $json = json_encode($storeData[0]['store_timing'], true);
                                                // for ($i=0; $i <count($json)  ; $i++) { 
                                                //     echo $i;
                                                // }

                                        ?>
                                            <div class="col-md-3">
                                                <label  class="text-semibold text-center"><h6>Monday:</h6></label>
                                            </div>

                                            <div  id="dot" class="col-md-3">
                                                <select class=" load-days" id="otime" name="mon-open"  onchange="validtime(this)" >
                                                </select>

                                            </div>
                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div  id="dct" class="col-md-3">

                                                <select class=" load-days" id="ctime"  name="mon-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed"   onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row ">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Tuesday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="tue-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="tue-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Wednesday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="wed-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="wed-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Thrusday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="thu-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class="load-days" id="ctime" name="thu-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Friday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="fri-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="fri-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Saturday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="sat-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="sat-close"  onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="text-semibold text-center"><h6>Sunday:</h6></label>
                                            </div>

                                            <div id="dot" class="col-md-3">
                                                <select class=" load-days"  id="otime" name="sun-open"  onchange="validtime(this)">
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-1 text-center text-semibold">TO</div>

                                            <div id="dct" class="col-md-3">
                                                
                                                <select class=" load-days" id="ctime" name="sun-close" onchange="validtime(this)" >
                                                    
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                <label><input type="checkbox" id="chckclose" value="closed" onchange="validtime(this)" class="styled">Closed</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="invalid-time text-danger-400 pull-right">
                                            <strong>*Invalid Time!</strong> Opening Time must be greater than Closing time.                     
                                        </div>

                                    </div><!--panel-->

                                
                            </fieldset>


                        

                    <h6>Banner Image </h6>
                            <fieldset>
                              <div id="multiple-img" class="panel">
                                <div class="panel-heading bg-info-700">
                                    <div class="panel-title"><span class="text-semibold">Upload Photo Images</span><a style="color:white;" href="#" data-toggle="tooltip" title="Upload more Pictures"><i class="glyphicon pull-right glyphicon-question-sign "></i></a>
                                    </div>
                                    <div class="heading-elements"></div>
                                </div>
                            
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label text-semibold">Select Images:</label>
                                        <div class="col-lg-10">

                                          
                                            <div class="">
                                                
                                            <input type="file" id="" name="store_picture"  class="file-input" fileTypeSettings="image"  >
                                            </div>
                                           
                                            <!--<input type="file" id="" name="store_picture"  class="file-input-ajax" fileTypeSettings="image" multiple="multiple">-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div> 
                            </fieldset>

                        </form>
                    </div>