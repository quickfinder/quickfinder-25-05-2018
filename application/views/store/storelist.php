

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    
                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Stores</a></li>
                            <li class="active">List Store

</li>
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">


                    <!-- Highlighting rows and columns -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
<div class="heading-elements">


                                                    <!-- <a href="<?php echo site_url('adduser');  ?>"> -->

                                <ul class="icons-list">
                                <h5 class="panel-title"><?php if($this->session->userdata('usertype')==3 && $this->session->userdata('usertype')==4){?>
                        <a href="<?php echo site_url('adminaddnewstores');  ?>">
                            <button class="btn ripple btn-gradient btn-primary" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a>
                    <?php }elseif($this->session->userdata('usertype')==2){ ?>
                    
                    <a href="<?php echo site_url('addstore');?>">
                            <button class=" btn btn-gradient btn-primary" >
                                <span>Add Store</span>
                            </button>
                        </a>
                    <?php   
                    }
                    
                    ?>      </h5>
                                    
                                </ul>
                            </div>


                            
                            
                        </div>


                        <table class="table table-bordered  datatable-highlight">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>User UniqueId</th>
                                    <th>Category</th>
                                    <th>Email</th>
                                    <!--<th>Mobile</th>-->
                                    <th>Payment Type</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                                $i = 0;
                                foreach ($storelist as $row) {
                                    $i++;
                                        $userid = $this->session->userdata('userid');

                                          $usertype = $this->session->userdata('usertype');
                                            if ($usertype == 2 && $userid == $row['userid']) {
                                        ?>
                                                                            <tr>

                                        <td>
                                            <img src="<?php echo base_url().$row['firstimage']; ?>" style="width:100px;height:50px">
                                        </td>
                                        <td><?php echo $row['storename']; ?></td>
                                        <td><?php echo 'QFU'.$row['userid']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        
                                        <td><?php echo trim($row['email']); ?></td>
                                        <!--<td><?php echo $row['mobile']; ?></td>-->
                                        <td>
                                        <span class="label label-primary"><?php echo $row['payment_type'];?></span></td>
                                         
										 <td>
                                          
                                            <?php if ($row['IsActive'] == 1) { ?>
                                             <span class="label label-success">Active</span>
                                            <?php } else { ?>
                                                   <a href="<?php echo site_url('pricingp/' . $row['id']) ?>"> <span class="label label-danger">Deactivate</span></a>
											<?php } ?>
										</td>
									
                                         <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                     <li><a href="<?php echo site_url('update-store/'.base64_encode($row['id'])); ?>" ><i class="icon-pencil4"></i> Edit </a></li>
                                                    <li><a href="#"><i id="<?php echo $row['id'];?>" class="storedeleted icon-box"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
										</td>
											</tr>
                                        <?php } elseif ($usertype == 3 || $usertype == 4) {?>	
                                        <tr>
                                         <td>
                                            <img src="<?php echo base_url().$row['firstimage']; ?>" style="width:100px;height:50px">
                                        </td>
                                        <td><?php echo $row['storename']; ?></td>
                                        <td><?php echo 'QFU'.$row['userid']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        
                                        <td><?php echo trim($row['email']); ?></td>
                                        <!--<td><?php echo $row['mobile']; ?></td>-->
                                        <td>
                                        <span class="label label-primary"><?php echo $row['payment_type'];?></span></td>
                                         
                                         <td class="text-center">
                                             <?php if($row['IsActive']==1){?>
                                             <span class="label label-success">Active</span>
                                        <?php }else{?>
                                        <a href="<?php echo site_url('pricingp/' . $row['id']) ?>"> <span class="label label-danger">Deactivate</span></a>
                                        <?php }?>
                                        </td>
                                    <td>
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                                    <li><a href="<?php echo site_url('addstore/'.$row['userid'])?>" data-placement="top" title="Add New Store" value="AddStore"><i class="icon-pencil3"></i>Add Store</a></li>
                                                    
                                                    <li><a href="#"><i id="<?php echo $row['id'];?>" class="storedeleted icon-box"></i> Delete</a></li>
                                                     <li><a href="<?php echo site_url('editstore/'.$row['id'])?>" ><i class="icon-pencil4"></i> Edit </a></li>
                                                </ul>
                                            </li>
                                        </ul>

                                    </td>
 
										
                                    </tr>
								<?php } 
                            }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /highlighting rows and columns -->

                    </div>
                   