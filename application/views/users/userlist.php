
<!-- Page header -->
				<div class="page-header page-header-default">
					<!-- <div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Admin</span></h4>
						</div>

						
					</div> -->

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> User</a></li>
							<li class="active">List Of Admin</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->



<!-- Content area -->
				<div class="content">

				

					<!-- DOM positioning -->
					<div class="panel panel-flat">
						<div class="panel-heading">
						
							<div class="heading-elements">


													<!-- <a href="<?php echo site_url('adduser');  ?>"> -->

								<ul class="icons-list">
								<a href="<?php echo site_url('adduser');  ?>">
                            <button class="btn ripple btn-gradient btn-primary" style="width:150px">
                                <span>Add Admin</span>
                            </button>
                        </a> 
			                		
			                	</ul>
		                	</div>
						</div>

						
						<table class="table table-bordered table-hover datatable-highlight">
							<thead>
								<tr>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
									<th>Stores</th>
									<!-- <th>Action</th> -->
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
							
								<?php $i=1;
								foreach($user as $row){
								
								// print_r($user);
								?>
                                <tr>	
									<td><?php echo $i++;?></td>
									<td><?php echo $row['name'];?></td>
									<td><?php echo $row['email']; ?></td>
									<td><?php echo $row['mobile']; ?></td>
									<td><?php echo $row['numstore']; ?></td>
									
									<td class="text-center">
										<ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="<?php echo site_url('viewstore/'.$row['id']);  ?>"><i class=" icon-pencil4"></i> Search</a></li>
													<li><a href="<?php echo site_url('updateadmin/'.$row['id']);  ?>"><i class="icon-pencil"></i> Edit</a></li>
													<li><a href="#"><i id="<?php echo $row['id'];?>"  class=" icon-box"></i> Delete</a></li>
												</ul>
											</li>
										</ul>
									</td>
                                </tr>
							<?php }?>
							</tbody>
						</table>
					</div>
					</a>
					</div>

					<!-- /DOM positioned -->




			