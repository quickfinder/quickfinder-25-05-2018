<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Quick Finder</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <!-- Custom Theme files -->
        <style>
            //Reusing bootstrap 3 panel CSS

            .panel {
                background-color: #FFFFFF;
                border: 1px solid rgba(0, 0, 0, 0);
                border-radius: 4px 4px 4px 4px;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
                margin-bottom: 20px;
            }   

            .panel-primary {
                border-color: #428BCA;
            }   

            .panel-primary > .panel-heading {
                background-color: #428BCA;
                border-color: #428BCA;
                color: #FFFFFF;
            }   

            .panel-heading {
                border-bottom: 1px solid rgba(0, 0, 0, 0);
                border-top-left-radius: 3px;
                border-top-right-radius: 3px;
                padding: 10px 15px;
            }   

            .cpoint{
                cursor: pointer;
                background-color:;
            }
            .cpoint:hover{

                color: red;
                font-size: 16px;

            }

            .hide-file-img{
    display:none !important;
}
            .v-align
            {
                vertical-align: baseline;
            }
            .panel-title {
                font-size: 16px;
                margin-bottom: 0;
                margin-top: 0;
            }   

            .panel-body:before, .panel-body:after {
                content: " ";
                display: table;
            }   

            .panel-body:before, .panel-body:after {
                content: " ";
                display: table;
            }   

            .panel-body:after {
                clear: both;
            }   

            .panel-body {
                padding: 15px;
            }   

            .panel-footer {
                background-color: #F5F5F5;
                border-bottom-left-radius: 3px;
                border-bottom-right-radius: 3px;
                border-top: 1px solid #DDDDDD;
                padding: 10px 15px;
            }

            //CSS from v3 snipp
            .user-row {
                margin-bottom: 14px;
            }

            .user-row:last-child {
                margin-bottom: 0;
            }

            .dropdown-user {
                margin: 13px 0;
                padding: 5px;
                height: 100%;
            }

            .dropdown-user:hover {
                cursor: pointer;
            }

            .table-user-information > tbody > tr {
                border-top: 1px solid rgb(221, 221, 221);
            }

            .table-user-information > tbody > tr:first-child {
                border-top: 0;
            }


            .table-user-information > tbody > tr > td {
                border-top: 0;
            }

        </style>
        <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
        
        <!--- materialize css --->
        <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css"
              media="all"/>
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- menu style -->
        <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- banner slider -->
        <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
        <!-- carousel slider --><link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- //Custom Theme files -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">


        <link href="<?php echo base_url(); ?>css/circularwaves.css" rel="stylesheet">
        <!-- //font-awesome icons -->
        <!-- js -->
        <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
        <!-- //js -->

        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>

        <!---- materialize js ----
        <script src="<php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
        -->
        <script>

            function readURL(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();
                                            
                                            reader.onload = function (e) {
                                               
                                                $('#propic').attr('src', e.target.result);
                                                 //$("#modal_img").attr("src",e.target.result); 
                                                
                                             };


                                            reader.readAsDataURL(input.files[0]);
                                            
                                        }
                                                                                                               

                                    }//readURL


            $(document).ready(function () {


            });
        </script>
        <!-- start-smooth-scrolling -->
        <script type="text/javascript" src="js/move-top.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- //end-smooth-scrolling -->
        <!-- smooth-scrolling-of-move-up -->
        <script type="text/javascript">
            $(document).ready(function () {

                var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear'
                };

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <!-- //smooth-scrolling-of-move-up -->


        <link href="<?php echo base_url(); ?>asset/css/fade_effect_slider.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">

    </head>
    <body>



        <div class="n-ele-center n-white">
            <!-- header -->

            <div class="header">
                <div class="header-two"><!-- header-two -->
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-lg-1 ">
                                <div class="header-logo">
                                    <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                                 src="<?php echo base_url(); ?>images/logo/1.png"
                                                                                 alt="quickfinder logo"/></a></h1>
                                </div>
                            </div>


                            <div class="col-lg-7 my-acc">
                                <div class="row">
                                    <div class="col-lg-6  acc-heading">
                                        <h4>My Account</h4>
                                    </div>
                                    <!--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <p><?php echo $this->session->userdata('email'); ?></p>-->
                                    <!--</div>-->
                                    <div class="col-lg-6 ">
                                        <?php if ($this->session->userdata('usertype') == 1) { ?>
                                            <a href="<?php echo site_url('freelisting'); ?>"> <button class="btn pull-right  waves-effect waves-light">
                                                    <span>List Your Business</span>
                                                </button>
                                            <?php } ?>
                                        </a>
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-3 " >
                                <div class="social-icons">

                                    <!-- Dropdown Structure -->
                                    <nav class="navbar">
                                        
                                        <?php if ($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE) { ?>
                                            <div class="container-fluid top">
                                                <a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
                                                <a class="bold"  href="<?php echo site_url('signupform'); ?>">Register</a>
                                            </div>
                                        <?php } else { ?>
                                        
                                            <ul class="nav navbar-nav pull-right">
                                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <img src="<?php echo base_url(); ?><?php echo $this->session->userdata('profilepic'); ?>"/>
                                                     <?php echo $this->session->userdata('username'); ?> 
                                                    <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="<?php echo site_url('User'); ?>">My Account</a></li>
                                                        <li><a href="<?php echo site_url('changePassword'); ?>">Change Password</a></li>
                                                        <li><a href="<?php echo site_url('my-profile'); ?>">Profile</a></li>
                                                        <?php if ($this->session->userdata('usertype') == 2) { ?> 
                                                            <li><a href="<?php echo site_url('Dashboard'); ?>">Dashboard</a></li>
                                                        <?php } ?>
                                                        <li><a href="<?php echo site_url('logout'); ?>">Logout</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        <?php } ?>
                                    </nav>

                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                    <!--</div>-->
                </div><!-- //header-two -->
            </div>


            <!-- //header -->

        <form action="<?php echo site_url('profile');?>" method="post" enctype="multipart/form-data">
            <div class="container deals" style="width:80%">
                <div class="panel panel-success">
                    <div class="panel-heading"><b>Edit User Details</b></div>
                    <div class="panel-body">
                        
                            <div class="row">
                                    <div class="col-md-2  "  style="">
                                        <label class="control-label v-align"><b> First Name:</b></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="fname" id="first_name" class=" form-control" value="<?php echo $user[0]['first_name'] ?>">
                                    </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="col-md-2" ">
                                        <label class="control-label v-align"><b> Last Name:</b></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="lname" id="last_name" class=" form-control" value="<?php echo $user[0]['last_name'] ?>">
                                    </div>
                            </div>
                                <br>
                                 <div class="row">
                                    <div class="col-md-2 " ">
                                        <label ><b> Contact:</b></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="contact" id="contact" class=" form-control" value="<?php echo $user[0]['mobile'] ?>">
                                    </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="col-md-2 " ">
                                        <label ><b> Username:</b></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="username" id="user_name" class=" form-control" value="<?php echo $user[0]['username'] ?>" >
                                    </div>

                            </div>
                            
                          <br>
                          

                            <br><br>
                             <div class="row">
                                <div class="col-md-2"><label ><b> Profile Picture:</b></label></div>
                                <div class="col-md-5">
                                    <img class="img-rounded" id="propic" src="<?php echo base_url(); ?><?php echo $this->session->userdata('profilepic'); ?>"  height="100px" width="100px"/>
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class=" btn  btn-primary" 
                                            onclick="document.getElementById('fileInput').click();"> 
                                            <span class="fa fa-file-image-o"></span>&nbsp;    
                                           Browse
                                            </button>
                                            <input type="file"  name="input-file-preview" onchange="readURL(this);"  id="fileInput" class="hide-file-img" 
                                              />
                                </div>
                            </div>



                            

                        
                    </div>
                    <br>
                    

                          
                         <br> <br>
                    
                    <div class="panel-footer">
                        <div class="pull-right">
                           
							<button type="submit" class="btn btn-success"><span class="fa-save fa"></span>&nbsp;&nbsp;Save</button>
                            &nbsp;&nbsp;&nbsp;&nbsp;</a>
							
                            <button class="btn btn-danger"><span class="fa-close fa"></span>&nbsp;&nbsp;Cancel</button>
							</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </form>
            <div class="copy-right">
                <div class="">
                    <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
                </div>
            </div>
        </div>
    </div>
                                    <ul class="icons-list mt-15">
                                        <li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
                                        <li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
                                        <li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /user thumbnail -->


                            <!-- Navigation -->
                           <!--  <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Navigation</h6>
                                    <div class="heading-elements">
                                        <a href="#" class="heading-text">See all &rarr;</a>
                                    </div>
                                </div>

                                <div class="list-group no-border no-padding-top">
                                    <a href="#" class="list-group-item"><i class="icon-user"></i> My profile</a>
                                    <a href="#" class="list-group-item"><i class="icon-cash3"></i> Balance</a>
                                    <a href="#" class="list-group-item"><i class="icon-tree7"></i> Connections <span class="badge bg-danger pull-right">29</span></a>
                                    <a href="#" class="list-group-item"><i class="icon-users"></i> Friends</a>
                                    <div class="list-group-divider"></div>
                                    <a href="#" class="list-group-item"><i class="icon-calendar3"></i> Events <span class="badge bg-teal-400 pull-right">48</span></a>
                                    <a href="#" class="list-group-item"><i class="icon-cog3"></i> Account settings</a>
                                </div>
                            </div> -->
                            <!-- /navigation -->


                         


                            

                         

                        </div>
                    </div>
                    <!-- /user profile -->


                    <!-- Footer -->
                    <!-- <div class="footer text-muted">
                        &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                    </div> -->
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->
