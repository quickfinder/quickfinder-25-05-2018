
	
<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Admin</span> </h4>
						</div>

						
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Admin</a></li>
							<li class="active">Form</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


<!-- Content area -->
				<div class="content">
<!-- 2 columns form -->
					<form class="form-horizontal" method="post" action="<?php echo site_url('adduser'); ?>">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Add Admin Form</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<!-- <li><a data-action="collapse"></a></li> -->
				                		<li><a data-action="reload"></a></li>
				                		<!-- <li><a data-action="close"></a></li> -->
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">
								<div class="row">
								     <?php echo validation_errors();?>

									<div class="col-md-6">
										<fieldset>
<!-- 											<legend class="text-semibold"><i class="icon-reading position-left"></i> Personal details</legend>
 -->
											<div class="form-group">
												<label class="col-lg-3 control-label">Enter First name:</label>
												<div class="col-lg-9">
													<input type="text" class="form-control" placeholder="Enter Name" name="fname" pattern="^[A-Za-z]{1,20}$" required>
												</div>
											</div>

												<div class="form-group">
												<label class="col-lg-3 control-label">Enter Last name:</label>
												<div class="col-lg-9">
													<input type="text" class="form-control" placeholder="Enter Last Name" name="lname" pattern="^[A-Za-z]{1,20}$" required>
												</div>
											</div>	

											<div class="form-group">
												<label class="col-lg-3 control-label">Email:</label>
												<div class="col-lg-9">
													<input type="text" placeholder="eugene@kopyov.com" class="form-control" name="email" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label">Mobile:</label>
												<div class="col-lg-9">
													<input type="text" placeholder="+918000000000" class="form-control" maxlength="10" pattern="[789][0-9]{9}" name="mobile" required>
												</div>
											</div>




	
											
											
										</fieldset>
									</div>

									<div class="col-md-6">
										<fieldset>
						                	<!-- <legend class="text-semibold"><i class="icon-truck position-left"></i> Shipping details</legend> -->

											

											
											<div class="form-group">
												<label class="col-lg-3 control-label">Select your city:</label>
												<div class="col-lg-9">
														<select data-placeholder="Select your state" class="select form-text cities" name="city">
                                        <option value="0">Select city</option>
                                        <option></option>
                                                    <option value="cebu">Cebu</option>
                                                    <option value="manila">Manila</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">Select your Ares:</label>
												<div class="col-lg-9">
                                    <select  name="area" class="select  " data-placeholder="Select Area" >
                                            <option></option>
                                            <option value="Apans">Apans</option>
                                            <option value="Banilad">Banilad</option>
                                            <option value="Mabolo">Mabolo</option>
                                            <option value="Bali">Bali</option>
                                        </select>
												</div>
											</div>
											

											<div class="form-group">
												<label class="col-lg-3 control-label">Address:</label>
												<div class="col-lg-9">
													<input type="text" placeholder="Your address of living" class="form-control" name="address" required>
												</div>
											</div>

											
											<div class="form-group">
												<label class="col-lg-3 control-label">Enter your password:</label>
												<div class="col-lg-9">
												  <input type="hidden" name="password_strength" id="password_strength"/>

													<input type="password" class="form-control" placeholder="Your strong password" id="password" name="password" required>
												</div>
											</div>

											

										</fieldset>
									</div>
								</div>

								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->
