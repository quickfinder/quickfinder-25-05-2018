<style>
img {

border-radius: 4px;
padding: 5px;
width: 150px;
}

img:hover {
box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
</style>
<!-- Page header -->
<div class="page-header page-header-default">


<div class="breadcrumb-line">
<ul class="breadcrumb">
	<li><a href="index.html"><i class="icon-home2 position-left"></i> Vendors</a></li>
	<li class="active">List Vendors</li>
</ul>


</div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">



<!-- DOM positioning -->
<div class="panel panel-flat">
<div class="panel-heading">

	<div class="heading-elements">
		<ul class="icons-list">
    		<!-- <li><a data-action="collapse"></a></li> -->
    		<li><a data-action="reload"></a></li>
    		<!-- <li><a data-action="close"></a></li> -->
    	</ul>
	</div>
</div>


<table class="table datatable-dom-position">
	<thead>
		<tr>
				 <th>Sr.No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
			<th>Stores</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>

	<?php
									$i=1;
								foreach($user as $row){
								
								
								?>
                                <tr>	
									<td><?php echo $i++;?></td>
									<td><?php echo $row['name'];?></td>
									<td><?php echo $row['email']; ?></td>
									<td><?php echo $row['mobile']; ?></td>
									<td><?php echo $row['numstore']; ?></td>
									<!-- <td>
									
									<a href="<?php echo site_url('vendorstore/'.$row['id']);  ?>" class="">
									<button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Search Vendor Store" >
										<span class="fa fa-search"></span>
									</button>
									</a>
									
									<a href="<?php echo site_url('updatevendor/'.$row['id']);  ?>" class="">
									<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Vendor" >
										<span class="fa fa-pencil"></span>
									</button>
									</a>
									<a href="#"id="<?php echo $row['id'];?>" class="deletevendor">
									<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Vendor" >
										<span class="fa fa-bitbucket"></span>
									</button>
									</a>
									</td> -->

<td>
				<ul class="icons-list">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-menu9"></i>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="<?php echo site_url('vendorstore/'.$row['id']);?>"><i class="icon-search4" title="Search Vendor Store"></i> Search</a></li>
							<li><a href="<?php echo site_url('updatevendor/'.$row['id']);  ?>"><i class="icon-pencil"  title="Edit Vendor"></i> Edit </a></li>
							<li><a href="#" id="<?php echo $row['id'];?>" class="deletevendor"><i class="icon-bin2" title="Delete Vendor"></i> Delete</a></li>
						</ul>
					</li>
				</ul>
			</td>

                                </tr>
							<?php }?>
		
	</tbody>
</table>
</div>
<!-- /DOM positioned -->




