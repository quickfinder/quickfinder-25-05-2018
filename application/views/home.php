<?php $usertype=$this->session->userdata('usertype');
	//super admin Dashboard
	if($usertype==4)
	{
		$this->load->view('Dashboard/superadmin');
	}
	//admin Dashboard
	elseif($usertype==3)
	{
		$this->load->view('Dashboard/admin');
	}
	//vendor Dashboard
	elseif($usertype==2)
	{
		$this->load->view('Dashboard/vendor');
	}
	//users Dashboard
	elseif($usertype==1)
	{
		redirect('User');
	}
	
?>
