<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quickfinder</title>
<style type="text/css">
 .error
{color:red;}
.upload {
  width: 100%;
  height: 30px;
}
.previewBox {
  text-align: center;
  position: relative;
   padding: 5px;
  width: 50px;
  height: 50px;
  margin-right: 10px;
  margin-bottom: 20px;
  float: left;
}
.previewBox img {
  height: 50px;
  width: 50px;
  padding: 5px;
  border: 1px solid rgb(232, 222, 189);
}
.delete {
  color: red;
  font-weight: bold;
  position: absolute;
  top: 0;
  cursor: pointer;
  width: 20px;
  height:  20px;
  border-radius: 50%;
  background: red;
}
 .td
 {
	 width:10% !important
 }
.left-group
{
      margin: auto;
    float: none;

}
/*
.top{
	padding-top:20px;
}*/
.bold{
	color:#2d383e;font-weight:bold;
	padding-top:17px;
}
.navbar-nav > li > a {
    padding-top: 21px;
    padding-bottom: 15px;
}
.error{
	color:red !important;
}
.social-icons
{
	margin-top:0px !important;
}
.container{
    margin-top:20px;
}
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}

</style>
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<!-- <link href='//fonts.googleapis.com/css?family=Monda:400,700' rel='stylesheet' type='text/css'> -->
             <link href="<?php echo base_url();?>asset/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/colors.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>assets/css/trcustom.css" rel="stylesheet" type="text/css">
		 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/bootstrap-material-datetimepicker.css"/>

     
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/jquery.tagit.css""/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/tagit.ui-zendesk.css""/>
  <!-- tag in -->
 
<!-- Custom Theme files -->
<!-- for-mobile-apps -->
	<!-- /global stylesheets -->

</head>

<body>
