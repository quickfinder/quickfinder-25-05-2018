<?php
Class Profile_model extends CI_Model
{
	public function getUserData()
	{
		$userid= $this->session->userdata('userid');
		return $this->db->select('*')
				 ->from('user')
				 ->join('userdetails','userdetails.userid=user.id')
				 ->where('user.id',$userid)
				 ->get()->row();
	}
	public function editProfile($name)
	{
		$data=array('first_name'=>$_POST['fname'],
					'last_name'=>$_POST['lname'],
					'email'=>$_POST['email'],
					'profilepic'=>$name,
					'mobile'=>$_POST['mobile']);
		$this->session->set_userdata('profilepic',$name);			
		$this->db->where('userid',$this->session->userdata('userid'))->update('userdetails',$data);
		$data=array('email'=>$_POST['email'],
		             'username'=>$_POST['username'],
		             'mobile'=>$_POST['mobile']);
		return $this->db->where('id',$this->session->userdata('userid'))->update('user',$data);
	}
	public function editProfile1()
	{
		$data=array('first_name'=>$_POST['fname'],
					'last_name'=>$_POST['lname'],
					'email'=>$_POST['email'],
					'mobile'=>$_POST['mobile']);			
		$this->db->where('userid',$this->session->userdata('userid'))->update('userdetails',$data);
		$data=array('email'=>$_POST['email'],
		             'username'=>$_POST['username'],
		             'mobile'=>$_POST['mobile']);
		return $this->db->where('id',$this->session->userdata('userid'))->update('user',$data);
	}
	public function changePassword()
	{
		$data=array('password'=>md5($_POST['cpwd']));
		return $this->db->where('id',$this->session->userdata('userid'))->update('user',$data);
	}
}