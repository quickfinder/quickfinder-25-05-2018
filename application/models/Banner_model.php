<?php

class Banner_model extends CI_Model {

    public function getBannerData() {
       return $this->db->get('banner')->result_array();
    }
	public function getDetails($id)
	{
		return $this->db->where('id',$id)->get('banner')->row(); 
	}
    public function insertBanner($filename) {
    	
            		$startenddate=$_POST['startenddate'];

            		$a=explode('-',$startenddate);
// print_r($a);
// die;
            		$startdate1=$a[0];
            		$enddate1=$a[1];
$startdate = date("Y-m-d", strtotime($startdate1));
$enddate = date("Y-m-d", strtotime($enddate1)); 	
        $data = array(
            'images' => $filename,
          

			'startdate'=>$startdate,
			'enddate'=>$enddate,
        );
        return $this->db->insert('banner', $data);
    }
	public function editBanner($filename,$id)
	{
		// print_r($filename);
		// die;

		$startenddate=$_POST['startenddate'];

            		$a=explode('-',$startenddate);
// print_r($a);
// die;
            		$startdate1=$a[0];
            		$enddate1=$a[1];
$startdate = date("Y-m-d", strtotime($startdate1));
$enddate = date("Y-m-d", strtotime($enddate1));


		$data = array(
            'images' => $filename,
			'startdate'=>$startdate,
			'enddate'=>$enddate,
        );
        return $this->db->where('id',$id)->update('banner', $data);
	}
	public function activeBanner($id)
	{
		$data=array('IsActive'=>1);
		return $this->db->where('id',$id)->update('banner',$data);
	}
	public function deactiveBanner($id)
	{
		$data=array('IsActive'=>0);
		return $this->db->where('id',$id)->update('banner',$data);
	}
	public function bannerdelete($id)
	{
		return $this->db->where('id',$id)->delete('banner');
	}
	public function addPlatinumPlan($month,$year,$store_id)
	{
		$count=$this->db->select('storeid,calender.count as bcount')->where('monthID',$month)->where('year',$year)->get('calender')->row();
		$bcount=$count->bcount;
		$prevstoreid=$count->storeid;
		if(!empty($prevstoreid))
			$prevstoreid.=','.$store_id;
		else
			$prevstoreid=$store_id;
		
		$data=array(
					'storeid'=>$prevstoreid,
					'count'=>$bcount+1,
		);
		return $this->db->where('monthID',$month)->where('year',$year)->update('calender',$data);
	}
}

?>
