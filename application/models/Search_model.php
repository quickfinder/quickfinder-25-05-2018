<?php
class Search_model extends CI_Model {

	public function getStores()
	{
		$this->db->select('stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->get()->result_array();			
	}
	public function getCategories()
	{
		return $this->db->where('IsActive','1')->get('categories')->result_array();
	}
	public function getBanner()
	{
		return $this->db->where('IsActive','1')->get('banner')->result_array();
	}
	public function userCount()
	{
		$row=$this->db->where('date',date('Y-m-d'))->get('sitevisit')->num_rows();
		if($row > 0)
		{
			$count=$this->db->where('date',date('Y-m-d'))->get('sitevisit')->row();
			$data=array('visits'=>$count->visits+1);
			return $this->db->where('date',date('Y-m-d'))->update('sitevisit',$data);
		}
		else{
			$data=array('visits'=>1,'date'=>date('Y-m-d'));
			return $this->db->insert('sitevisit',$data);
		}
	}
	public function top_Result()
	{
	$searchQuery=$this->session->userdata('search_query')!='' ? $this->session->userdata('search_query') : '';
	$city=$this->session->userdata('search_city')!='' ? $this->session->userdata('search_city') : 'Cebu';
	$area=$this->session->userdata('search_area')!='' ? $this->session->userdata('search_area') : 'All';
	
	$searchlen= strlen(trim($searchQuery));
	$query=explode(" ",$searchQuery);
	
	
	$records=array();
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = '$city' and `stores`.`area`='$area' and ( $condition) order by user_clicks DESC" ;
				$premiumRecords=$this->db->query($sql)->result_array();
				foreach($premiumRecords as $row)
				{
					
					$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id'])
					);
				}
				print_r($records);
				die;
	}
	public function getSearch()
	{	
	
		$search=trim($_POST['flexdatalist-search']);
		$city=strtolower($_POST['flexdatalist-city']);
		if($_POST['area']=='All')
		{
			$area='';
		}
		else
		{
			$area=strtolower($_POST['area']);
		}
		$searchlen= strlen($search);
		$query=explode(" ",$search);
		
		$this->session->set_userdata('search_area',$area);
		$this->session->set_userdata('search_city',$city);
		$this->session->set_userdata('search_query',$_POST['flexdatalist-search']);
	
		$records=array();
			if(!empty($city) && !empty($search) && !empty($area))
			{
				
				return $this->ifNotNullSearch($city,$query,$area);
			}
			elseif(!empty($city) && !empty($search) && empty($area))
			{	
			
				return $this->ifAreaNull($city,$query);
			}
			elseif(!empty($city) && empty($search) && !empty($area))
			{		
				return $this->ifSearchNull($city,$area);
			}
			elseif(!empty($city) && empty($search) && empty($area))
			{	
				
				return $this->ifSearchAreaNull($city);
			}
			
	}
	public function ifSearchNull($city,$area)
	{
		$records=array();
		$sql="SELECT  stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = '$city' and `stores`.`area` = '$area'  ";
		$premiumRecords=$this->db->query($sql)->result_array();
		foreach($premiumRecords as $row)
			{
			
				$count=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->num_rows();
				if($count > 0)
				{
					$data=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->result_array();
					$favourite=$data[0]['IsActive'];
				}
				else
				{
					$favourite=0;
				}
				$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id']),
					'favourite'=>$favourite,
					);
			}				
		return $records;
	}
	public function ifAreaNull($city,$query)
	{
		$condition='';
		$records=array();
		for($i=0;count($query)>$i;$i++)
			{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = '$city'  and  ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					
					$count=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->num_rows();
				if($count > 0)
				{
					$data=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->result_array();
					$favourite=$data[0]['IsActive'];
				}
				else
				{
					$favourite=0;
				}
				$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id']),
					'favourite'=>$favourite,
					);
				}				
				return $records;
	}
	public function ifSearchAreaNull($city)
	{
		$records=array();
		$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = '$city' ";
		$premiumRecords=$this->db->query($sql)->result_array();
		foreach($premiumRecords as $row)
			{
				$count=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->num_rows();
				if($count > 0)
				{
					$data=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->result_array();
					$favourite=$data[0]['IsActive'];
				}
				else
				{
					$favourite=0;
				}
				$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id']),
					'favourite'=>$favourite,
					);
			}				
		return $records;
	}
	public function ifNotNullSearch($city,$query,$area)
	{
		$records=array();
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = '$city' and `stores`.`area`='$area' and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				foreach($premiumRecords as $row)
				{
					
					$count=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->num_rows();
				if($count > 0)
				{
					$data=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->result_array();
					$favourite=$data[0]['IsActive'];
				}
				else
				{
					$favourite=0;
				}
				$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id']),
					'favourite'=>$favourite,
					);
				}
				return $records;
	}	
	public function get_mysqli()
	{
		$db = (array)get_instance()->db;
		return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
	}
	public function getRating($storeid)
	{
		$this->db->distinct();
		$this->db->select('sum(rating) as rating,count(*) as row');
		$data=$this->db->where('storeid',$storeid)->get('store_rating')->result_array(); 
		if($data[0]['row']==0)
			return 0;
		else
			return  ceil($data[0]['rating']/$data[0]['row']);
	}


	 public function get_names_for_autofill()
	{
        $query = $this->db->query("SELECT `name` FROM `categories`
                                   UNION
                                   SELECT `name` FROM `subcategories`
                                   UNION
                                   SELECT `name` FROM stores s, store_payment sp
                                          WHERE sp.storeid= s.id and sp.IsActive=1"
		                         );
        return $query->result_array();
    }

	public function   get_all_areas()
	{
        return  $this->db->select('name')
			              ->from('area')
			              ->get()
			              ->result_array();
	}
}
?>
	