<?php 
Class Favourite_model extends CI_Model
{
	public function addStore($id)
	{
		$userid=$this->session->userdata('userid');
		$data=array('userid'=>$userid ,'storeid'=>$id,'IsActive'=>1,'date'=>date('Y-m-d'));
		$row=$this->db->where('userid',$userid)->where('storeid',$id)->get('favourate')->num_rows();
		if($row == 1)
		{
			return $this->db->where('userid',$userid)->where('storeid',$id)->update('favourate',$data);
		}
		else
		{
			return $this->db->insert('favourate',$data);
		}
	}
	public function removeStore($id)
	{
		$userid=$this->session->userdata('userid');
		$data=array('IsActive'=>0,'date'=>date('Y-m-d'));
		return $this->db->where('userid',$userid)->where('storeid',$id)->update('favourate',$data);
	}
	public function myFavorite()
	{
		$records=array();
		$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `favourate` ON `favourate`.`storeid`= `stores`.`id` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `favourate`.`IsActive` = 1";
		$premiumRecords=$this->db->query($sql)->result_array();
				foreach($premiumRecords as $row)
				{
					
					$count=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->num_rows();
				if($count > 0)
				{
					$data=$this->db->where('storeid',$row['id'])->where('userid',$this->session->userdata('userid'))->get('favourate')->result_array();
					$favourite=$data[0]['IsActive'];
				}
				else
				{
					$favourite=0;
				}
				$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id']),
					'favourite'=>$favourite,
					);
				}
				return $records;
				
	}
	public function getRating($storeid)
	{
		$this->db->distinct();
		$this->db->select('sum(rating) as rating,count(*) as row');
		$data=$this->db->where('storeid',$storeid)->get('store_rating')->result_array(); 
		if($data[0]['row']==0)
			return 0;
		else
			return  ceil($data[0]['rating']/$data[0]['row']);
	}
}
?>