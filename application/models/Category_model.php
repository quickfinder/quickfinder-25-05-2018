<?php
class Category_model extends CI_Model
{
	
	public function addNewCategory($filename)
	{
		$string = str_replace(' ', '', $_POST['name']);
		$date = date_create();
		$data=array(
		'name'=>$this->usertype=$_POST['name'],
		'icon_name'=>$string,
		'icons'=>$filename,
		'createdat'=> date_timestamp_get($date),
		'updatedat'=> date_timestamp_get($date),
		);
		return $this->db->insert('categories',$data);
	}
	 public function getListSubCategory($id) {
		if($cid=$this->db->select('id')->where('icon_name',$id)->get('categories')->row())
		{
			return $this->db->where('category_id', $cid->id)->get('subcategories')->result_array();
		}
		else
		{
			return false;
		}
    }
	 public function getProduct($id) {
		$records=array();
        $this->db->select('stores.user_clicks,stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
        $this->db->from('stores');
        $this->db->join('categories', 'categories.id=stores.categoryid');
        $this->db->join('store_payment', 'store_payment.storeid=stores.id');
        $this->db->join('payment', 'payment.id=store_payment.paymentid');
        $premiumRecords = $this->db->where('subcategoryid', $id)->get()->result_array();
        foreach ($premiumRecords as $row) {
            $records[] = array(
                'row' => $row,
                'rating' => $this->getRating($row['id'])
            );
        }
        return $records;
    }
	public function getRating($storeid) {
        $this->db->distinct();
        $this->db->select('sum(rating) as rating,count(*) as row');
        $data = $this->db->where('storeid', $storeid)->get('store_rating')->result_array();
        if ($data[0]['row'] == 0)
            return 0;
        else
            return ceil($data[0]['rating'] / $data[0]['row']);
    }


	public function deactiveCategory($id)
	{
		$data=array('IsActive'=>0);
		return $this->db->where('id',$id)->update('categories',$data);
	}
	public function activeCategory($id)
	{
		$data=array('IsActive'=>1);
		return $this->db->where('id',$id)->update('categories',$data);
		
	}
	public function getSubCategory($id)
	{
			   $this->db->select('categories.name as categoriesname,categories.id as categoriesid,subcategories.name as subcategoriesname');
			   $this->db->from('categories');
			   $this->db->join('subcategories','categories.id=subcategories.category_id');
		$data= $this->db->where('categories.id',$id)->get()->result_array();
		if(!empty($data))
		{
			return $data;
		}
		else{
			
				
			   $row=$this->db->where('id',$id)->get('categories')->result();
			   $data[0]= array('categoriesname'=>$row[0]->name,
							'categoriesid'=>$id,
							'subcategoriesname'=>''
							);
							return $data;
		}
	}
	public function addSubCategory()
	{
		// print_r($_POST);
		// die;	
		$id=$_POST['id'];
		$subcategory=$_POST['subcategory'];
		$keywords=$_POST['keywords'];
		$a=explode(',',$keywords);
		
			$data=array(
			'category_id'=>$id,
			'name'=>$subcategory,
			// 'keywords'=>$a[$i],
			);
			$this->db->insert('subcategories',$data);
			$lsatid=$this->db->insert_id();
			$keywords=$_POST['keywords'];
		$a=explode(',',$keywords);
		for($i=0;count($a)>$i;$i++)
		{
			$data1=array(
			// 'category_id'=>$id,
			'subcategoryid'=>$lsatid,
			'keywords'=>$a[$i],
			);
						$this->db->insert('keywords',$data1);

		}
		return true;
	}
	public function getCategory()
	{
		$categories = $this->db->get('categories')->result_array();
		
		$final_array = array();
		
		foreach($categories as $category)
		{
			$category_id = $category['id'];
			
			$query = $this->db->select('*')
			     ->from('subcategories')
		  	     ->where('category_id', $category_id)
			     ->get();
				 
			//print_r($query->result_array());
			//die();
			
			array_push($final_array,array(
			  'category'=>$category,
			  'subcategories'=>$query->result_array()
			));
		}
		
		return $final_array;
		
	}
	public function categorydelete()
	{
		return $this->db->where('id',$_POST['id'])->delete('categories');
	}
	public function changeSubcategory()
	{
		 $cid=$_POST['id'];
		 $tag=$_POST['tag'];
		 $val=$_POST['val'];
		 $data=array('name'=>$val);
		 return $this->db->where('category_id',$cid)->where('name',$tag)->update('subcategories',$data);
	}
	public function deleteSubcategory()
	{
		 $cid=$_POST['id'];
		 $val=$_POST['val'];
		 return $this->db->where('category_id',$cid)->where('name',$val)->delete('subcategories');
	}
	public function addNewSubcategory()
	{
		$data=$this->db->where('category_id',$_POST['id'])->where('name',strtolower($_POST['val']))->get('subcategories')->num_rows();
		if($data == 0)
		{
			$data=array(
			'category_id'=>$_POST['id'],
			'name'=>strtolower($_POST['val']),
			);
			return $this->db->insert('subcategories',$data);
		}
		else
		{
			return false;
		}
	}
	public function getCategoryDetails($id)
	{
		return $this->db->where('id',$id)->get('categories')->result();
	}
	public function editCategories($filename,$id)
	{
		$string = str_replace(' ', '', $_POST['name']);
		$date = date_create();
		$data=array(
		'name'=>$this->usertype=$_POST['name'],
		'icon_name'=>$string,
		'icons'=>$filename,
		'createdat'=> date_timestamp_get($date),
		'updatedat'=> date_timestamp_get($date),
		);
		return $this->db->where('id',$id)->update('categories',$data);
	}
	
	public function userVisit($id) {
        $clicks = $this->db->where('id', $id)->get('stores')->row();
        $data = array('user_clicks' => $clicks->user_clicks + 1);
        return $this->db->where('id', $id)->update('stores', $data);
	}
	
	
	public function getStoreProfile($storeid) 
	{
        $this->db->select('*');
        $this->db->from('stroe_images');
        $this->db->join('stores', 'stroe_images.storeid=stores.id');
        $data = $this->db->where('storeid', $storeid)->get()->row_array();
       if($data['subcategoryid']=='other')
	   {

				$this->db->select('*,categories.name as categoriesname,stores.name as storesname');
				$this->db->from('stores');
				$this->db->join('categories','categories.id=stores.categoryid');
				$data=$this->db->where('stores.id',$storeid)->get()->row_array();
				$othercat=$this->db->where('storeid',$storeid)->get('othercategory')->row();
				
				if(!empty($othercat->name))
				{
					$data['othercat']=$othercat->name;
					return $data;
				}
				else
				{
					$data['othercat']='';
					return $data;
				}
	   }
	   else
	   {
			if (!empty($data))
            	return $data;
            else 
            	return $this->db->where('id', $id)->get('stores')->row_array();
	   }
		
    }
	 public function getStoreRating($id) {
		$records=array();
        $poor =0;
        $average =0;
        $good =0;
        $verygood =0;
        $excellence =0;
        $ratevalue =0;
        $this->db->select('*');
        $this->db->from('store_rating');
        $result_array = $this->db->where('storeid', $id)->get()->result_array();
        foreach ($result_array as $row) {
            $ratevalue = $ratevalue + (int)$row['rating'];
            if ($row['rating'] == 1) {
               $poor++;
            } elseif ($row['rating'] == 2) {
               $average++;
            } elseif ($row['rating'] == 3) {
                $good++;
            } elseif ($row['rating'] == 4) {
               $verygood++;
            } elseif ($row['rating'] == 5) {
                $excellence++;
            }

	    /**
		 *  actually userid  is userdetailsid here
		 *  -- 
		 *  wrong name for key
		 *  userdetailsid stored in userid field of store_rating
		 */
        $records[] = array(
            'rating' => $row['rating'],
            'value' => $ratevalue,
            'review' => $row['review'],
            'userid' => $row['userid'],
            'poor' => $poor,
            'average' => $average,
            'good' => $good,
            'verygood' => $verygood,
            'excellence' => $excellence,
            'user_details' => $this->userDetails($row['userid']),
        );
        }
        return $records;
    }
	public function userDetails($param) {
//        echo $param;
        $this->db->select('*');
        $this->db->from('userdetails');
        //return $this->db->where('userid', $param) ->get()->result_array();
        return $this->db->where('id', $param) ->get()->result_array();
    }
}
?>