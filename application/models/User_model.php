<?php
Class User_model extends CI_Model
{
	public function addUser()
	{
		$data=array(
					'password'=>md5($_POST['password']),
					'usertype'=>3,
					'mobile'=>$_POST['mobile'],
					'email'=>$_POST['email'],
					'isactive'=>1,
					);
		$this->db->insert('user',$data);
		$userid=$this->db->insert_id();
		$data=array(
					'userid'=>$userid,
					'first_name'=>$_POST['fname'],
					'last_name'=>$_POST['lname'],
					'mobile'=>$_POST['fname'],
					'email'=>$_POST['email'],
					'country'=>'philippines',
					'state'=>$_POST['state'],
					'city'=>$_POST['city'],
					'mobile'=>$_POST['mobile'],
					'address'=>$_POST['address'],
					);
		return $this->db->insert('userdetails',$data);
	}
	public function getUser()
	{
		$this->db->select('*,user.id as userid');
		$this->db->from('user');
		$this->db->join('userdetails','userdetails.userid=user.id');
		$data=$this->db->where('user.usertype','3')->get()->result_array();
		foreach($data as $row)
		{
			$datalist[]=array('numstore'=>$this->db->select('*')->where('adminid',$row['id'])->get('stores')->num_rows(),
							'name'=>$row['first_name'].' '.$row['last_name'],
							'email'=>$row['email'],
							'mobile'=>$row['mobile'],
							'id'=>$row['userid']
							);
		}
		return $datalist;
	}
	public function getAdminStores($id)
	{
		$this->db->select('stores.userid,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->where('stores.adminid',$id)->get()->result_array();	
	}
	public function getVendorStore($id)
	{
		$this->db->select('stores.userid,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->where('stores.userid',$id)->get()->result_array();	
	}
	public function getVendors()
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('userdetails','userdetails.userid=user.id');
		$data=$this->db->where('user.usertype','2')->get()->result_array();
		
		foreach($data as $row)
		{
			$datalist[]=array('numstore'=>$this->db->select('*')->where('userid',$row['userid'])->get('stores')->num_rows(),
							'name'=>$row['first_name'].' '.$row['last_name'],
							'email'=>$row['email'],
							'mobile'=>$row['mobile'],
							'id'=>$row['userid']
							);
		}
		return $datalist;
	}
	public function getUsers()
	{
		
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('userdetails','userdetails.userid=user.id');
		$data=$this->db->where('user.usertype','1')->get()->result_array();
		$datalist=array();
		foreach($data as $row)
		{
			$datalist[]=array('numstore'=>0,
							'name'=>$row['first_name'].' '.$row['last_name'],
							'email'=>$row['email'],
							'mobile'=>$row['mobile'],
							'id'=>$row['userid']
							);
		}
		return $datalist;
	}
	public function getAdminDetails($id)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('userdetails','userdetails.userid=user.id');
		return $this->db->where('user.id',$id)->get()->result_array();
	}
	public function updateAdminDetails($id)
	{
		$data=array(
					'mobile'=>$_POST['mobile'],
					'email'=>$_POST['email'],
					);
		$this->db->where('id',$id)->update('user',$data);
		$data=array(
					'first_name'=>$_POST['fname'],
					'last_name'=>$_POST['lname'],
					'mobile'=>$_POST['fname'],
					'email'=>$_POST['email'],
					'country'=>'philippines',
					'state'=>$_POST['state'],
					'city'=>$_POST['city'],
					'mobile'=>$_POST['mobile'],
					'address'=>$_POST['address'],
					);
		return $this->db->where('userid',$id)->update('userdetails',$data);
	}
	public function admindelete()
	{
		  $this->db->where('id',$_POST['id']);
   return $this->db->delete('user');
	}
}
?>